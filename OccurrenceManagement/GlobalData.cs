﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OccurrenceManagement
{
    static class GlobalData
    {
        private static string _UserID = "";
        private static string _UserName = "";
        private static string _UserLogin = "";
        private static string _Locations = "";
        private static int _Level = 7;


        public static string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public static string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

         public static string UserLogin
        {
            get { return _UserLogin; }
            set { _UserLogin = value; }
        }

        public static string Locations
        {
            get { return _Locations; }
            set { _Locations = value; }
        }

        public static int Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
    }
}
