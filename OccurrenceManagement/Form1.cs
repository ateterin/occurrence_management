﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Microsoft.Reporting.WinForms;
using System.IO;
using AxAcroPDFLib;
using System.Threading;
using System.Globalization;
using System.Net.Mail;




namespace OccurrenceManagement
{
    public partial class frmMain : Form
    {

        dsOccurrenceTypes.dtOccurrenceTypesDataTable dtOTypes = new dsOccurrenceTypes.dtOccurrenceTypesDataTable();
        dsOccurrenceTypesTableAdapters.dtOccurrenceTypesTableAdapter daOTypes = new dsOccurrenceTypesTableAdapters.dtOccurrenceTypesTableAdapter();
        dsOccurrenceSubtypes.dtOccurrenceSubtypesDataTable dtOSubtypes = new dsOccurrenceSubtypes.dtOccurrenceSubtypesDataTable();
        dsOccurrenceSubtypesTableAdapters.dtOccurrenceSubtypesTableAdapter daOSubtypes = new dsOccurrenceSubtypesTableAdapters.dtOccurrenceSubtypesTableAdapter();
        dsLocation.dtLocationsDataTable dtOLocations = new dsLocation.dtLocationsDataTable();
        dsLocationTableAdapters.dtLocationsTableAdapter daOLocations = new dsLocationTableAdapters.dtLocationsTableAdapter();
        dsWeather.dtWeatherDataTable dtOWeather = new dsWeather.dtWeatherDataTable();
        dsWeatherTableAdapters.dtWeatherTableAdapter daOWeather = new dsWeatherTableAdapters.dtWeatherTableAdapter();
        dsPNFVehicleTypes.dtPNFVehicleTypesDataTable dtOPNFVTypes = new dsPNFVehicleTypes.dtPNFVehicleTypesDataTable();
        dsPNFVehicleTypesTableAdapters.dtPNFVehicleTypesTableAdapter daPNFVTypes = new dsPNFVehicleTypesTableAdapters.dtPNFVehicleTypesTableAdapter();

        dsGetData.dtGetDataDataTable dtGData = new dsGetData.dtGetDataDataTable();
        dsGetDataTableAdapters.dtGetDataTableAdapter daGData = new dsGetDataTableAdapters.dtGetDataTableAdapter();
        dsParks.dtParksDataTable dtVParks = new dsParks.dtParksDataTable();
        dsParksTableAdapters.dtParksTableAdapter daVParks = new dsParksTableAdapters.dtParksTableAdapter();
        dsServices.dtServicesDataTable dtGServices = new dsServices.dtServicesDataTable();
        dsServicesTableAdapters.dtServicesTableAdapter daGServices = new dsServicesTableAdapters.dtServicesTableAdapter();

        dsHistory.dtHistoryDataTable dtGHistory = new dsHistory.dtHistoryDataTable();
        dsHistory.dtHistoryDataTable dtGHistoryM = new dsHistory.dtHistoryDataTable();
        dsHistoryTableAdapters.dtHistoryTableAdapter daGHistory = new dsHistoryTableAdapters.dtHistoryTableAdapter();

        dsReportOccurrenceData.dtReportOccurrenceDataDataTable dtRepOccurrenceData = new dsReportOccurrenceData.dtReportOccurrenceDataDataTable();
        dsReportOccurrenceDataTableAdapters.dtReportOccurrenceDataTableAdapter daRepOccurrenceData = new dsReportOccurrenceDataTableAdapters.dtReportOccurrenceDataTableAdapter();
        dsOccurrencesOverdue.dtOccurrencesOverdueDataTable dtOccurOverdue = new dsOccurrencesOverdue.dtOccurrencesOverdueDataTable();
        dsOccurrencesOverdueTableAdapters.dtOccurrencesOverdueTableAdapter daOccurOverdue = new dsOccurrencesOverdueTableAdapters.dtOccurrencesOverdueTableAdapter();

        dsReportOccurrenceDataUser.dtReportOccurrenceDataUserDataTable dtRepOccurrenceDataUser = new dsReportOccurrenceDataUser.dtReportOccurrenceDataUserDataTable();
        dsReportOccurrenceDataUserTableAdapters.dtReportOccurrenceDataUserTableAdapter daRepOccurrenceDataUser = new dsReportOccurrenceDataUserTableAdapters.dtReportOccurrenceDataUserTableAdapter();

        dsLocationCheque.dtGetLocationsChequeDataTable dtLocCheques = new dsLocationCheque.dtGetLocationsChequeDataTable();
        dsLocationChequeTableAdapters.dtGetLocationsChequeTableAdapter daLocCheque = new dsLocationChequeTableAdapters.dtGetLocationsChequeTableAdapter();

        string strTicketId = "0";
        string strLocationId = "";
        //string strOccurrenceNumber = "";
        //Int64 OccurrenceNumber = 0;
        string strStatus = "";
        string strEmployeeClosed = "";
        DateTime dtDateClosed;
        //string strPath = "";
        string strOpenCheck = "No";
        string strAssignedToRMM = "";
        private String strFileLock_ = String.Empty;

        public frmMain()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            if (GlobalData.Level > 1)
            {
                tbcOccurrence.TabPages.Remove(tabRisk);
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                string userLogin = GlobalData.UserLogin;
                string gplocations = GlobalData.Locations;



                daOTypes.Fill(dtOTypes);
                cmbGType.DataSource = dtOTypes;
                cmbGType.DisplayMember = "occurrence_type";
                cmbGType.ValueMember = "occurrence_type_id";

                daOSubtypes.Fill(dtOSubtypes, "1");
                cmbGSubtype.DataSource = dtOSubtypes;
                cmbGSubtype.DisplayMember = "occurrence_subtype";
                cmbGSubtype.ValueMember = "occurrence_subtype_id";

                daOLocations.Fill(dtOLocations, gplocations);
                cmbGLocation.DataSource = dtOLocations;
                cmbGLocation.DisplayMember = "location";
                cmbGLocation.ValueMember = "location_id";

                daOWeather.Fill(dtOWeather);
                cmbGWeather.DataSource = dtOWeather;
                cmbGWeather.DisplayMember = "weather";
                cmbGWeather.ValueMember = "weather_id";

                daPNFVTypes.Fill(dtOPNFVTypes);
                cmbVPNFVehicleType.DataSource = dtOPNFVTypes;
                cmbVPNFVehicleType.DisplayMember = "pnf_vehicle_type";
                cmbVPNFVehicleType.ValueMember = "pnf_vehicle_type_id";

                dgvVServices.ColumnHeadersDefaultCellStyle.ForeColor = Color.DarkGreen;
                dgvVServices.ColumnHeadersDefaultCellStyle.BackColor = Color.PaleGreen;
                dgvVServices.Refresh();

                //strLocationId = ConfigurationManager.AppSettings["DefaultLocation"];
                strLocationId = cmbGLocation.SelectedValue.ToString();
                //int index =  cmbGLocation.FindString("Toronto Valet"); //get index
                //cmbGLocation.SelectedValue = strLocationId;
                //cmbGLocation.SelectedIndex = index;

                cmbVFault.SelectedIndex = 0;
                cmbVAccident.SelectedIndex = 0;

                //tbcOccurrence.TabPages.Remove(tabRisk);
                //tbcOccurrence.TabPages.Add(tabRisk);

                this.LogMessage(string.Format("Form is loaded"));

            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            this.reportViewer1.RefreshReport();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
    
            try
            {
                chkAttachments.Visible = false;
                daGHistory.Fill(dtGHistory, 0);
                //dgvGHistory.DataSource = dtGHistory;
                strStatus = "O";

                btnCustomerCopy.Visible = false;
                btnInternalCopy.Visible = false;
                btnFinalRelease.Visible = false;
                btnFinalReleaseVoucher.Visible = false;
                btnFinalReleaseRepair.Visible = false;

                btnCustomerCopyFrench.Visible = false;
                btnFinalReleaseFrench.Visible = false;
                btnFinalReleaseVoucherFrench.Visible = false;
                btnFinalReleaseRepairFrench.Visible = false;
                btnOccurrencesOverdue.Visible = false;


                reportViewer1.Visible = false;
                lstPictures.Visible = false;

                ShowGeneralTab();

                btnAddPicture.Visible = false;
                btnDeletePicture.Visible = false;
                btnSaveAs.Visible = false;
            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void cmbGType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtOSubtypes.Clear();
            if (cmbGType.SelectedItem != null)
            {
                daOSubtypes.Fill(dtOSubtypes, cmbGType.SelectedValue.ToString());
                cmbGSubtype.DataSource = dtOSubtypes;
                cmbGSubtype.DisplayMember = "occurrence_subtype";
                cmbGSubtype.ValueMember = "occurrence_subtype_id";              
            }
        }

        private void cmbGSubtype_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbGSubtype.SelectedItem != null && cmbGSubtype.SelectedValue.ToString() == "8")
            {
                chkLTInjury.Visible = true;
            }
            else
            {
                chkLTInjury.Visible = false;
                lblGLostDays.Visible = false;
                txtGLostDays.Visible = false;
            }
        }

        private void chkLTInjury_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLTInjury.Checked)
            {
                lblGLostDays.Visible = true;
                txtGLostDays.Visible = true;
            }
            else
            {
                lblGLostDays.Visible = false;
                txtGLostDays.Visible = false;
                txtGLostDays.Text = "";
            }

        }

        private void chkGPolice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGPolice.Checked)
            {
                lblGDatePolice.Visible = true;
                lblGBadge.Visible = true;
                dtpGDatePolice.Visible = true;
                txtGBadge.Visible = true;
            }
            else
            {
                lblGDatePolice.Visible = false;
                lblGBadge.Visible = false;
                dtpGDatePolice.Visible = false;
                txtGBadge.Visible = false;

            }
        }

        private void cmbGLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbGLocation.SelectedValue != null)
            {
                strLocationId = cmbGLocation.SelectedValue.ToString();               
            }
        }

        private void btnGGetData_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strFTicketId = new StringBuilder(String.Empty);

                using (frmGetData frmGD = new frmGetData(ref strFTicketId, strLocationId))
                {
                    if (frmGD.ShowDialog() == DialogResult.OK)
                    {
                        strTicketId = strFTicketId.ToString();

                        string sTicketId = strTicketId.ToString();

                        if (sTicketId=="")
                        {
                            sTicketId = "0";
                        }

                        daGData.Fill(dtGData, Convert.ToInt64(sTicketId),GlobalData.Locations);

                        if (dtGData.Rows.Count == 0)
                        {
                            txtVTicketID.Text = "0";
                            txtVTicketNumber.Text = "";
                            txtVIssueNumber.Text = "";
                            txtVPinNumber.Text = "";
                            txtVBarCode.Text = "";
                            txtVOpenDate.Text = "";
                            txtVCloseDate.Text = "";
                            txtVVehicleID.Text = "0";
                            txtVCustomerID.Text = "0";
                            txtVLicensePlate.Text = "Unknown";
                            txtVMake.Text = "";
                            txtVColour.Text = "";
                            txtVFirstName.Text = "Unknown";
                            txtVLastName.Text = "Unknown";
                            txtVNumberOfParks.Text = "0";
                            txtVPostalCode.Text = "";
                            txtVCity.Text = "";
                            txtVProvince.Text = "";
                            txtVAddress1.Text = "";
                            txtVAddress2.Text = "";
                            txtVEmail.Text = "";
                            txtVHomePhone.Text = "";
                            txtVWorkPhone.Text = "";
                            txtVCellPhone.Text = "";
                            txtVFax.Text = "";                  
                        }
                        else
                        {
                    
                            txtVTicketID.Text = sTicketId;
                            txtVTicketNumber.Text = dtGData.Rows[0].ItemArray[1].ToString();
                            txtVIssueNumber.Text =  dtGData.Rows[0].ItemArray[2].ToString();
                            txtVPinNumber.Text = dtGData.Rows[0].ItemArray[3].ToString();
                            txtVBarCode.Text = dtGData.Rows[0].ItemArray[4].ToString();
                            txtVOpenDate.Text = dtGData.Rows[0].ItemArray[5].ToString();
                            txtVCloseDate.Text = dtGData.Rows[0].ItemArray[6].ToString();
                            txtVVehicleID.Text =  dtGData.Rows[0].ItemArray[7].ToString();
                            txtVCustomerID.Text =  dtGData.Rows[0].ItemArray[8].ToString();
                            txtVLicensePlate.Text =  dtGData.Rows[0].ItemArray[9].ToString();
                            txtVMake.Text =  dtGData.Rows[0].ItemArray[10].ToString();
                            txtVColour.Text = dtGData.Rows[0].ItemArray[11].ToString();
                            txtVFirstName.Text = dtGData.Rows[0].ItemArray[12].ToString();
                            txtVLastName.Text = dtGData.Rows[0].ItemArray[13].ToString();
                            txtVNumberOfParks.Text = dtGData.Rows[0].ItemArray[14].ToString();
                            txtVPostalCode.Text =  dtGData.Rows[0].ItemArray[15].ToString();
                            txtVCity.Text = dtGData.Rows[0].ItemArray[16].ToString();
                            txtVProvince.Text = dtGData.Rows[0].ItemArray[17].ToString();
                            txtVAddress1.Text = dtGData.Rows[0].ItemArray[18].ToString();
                            txtVAddress2.Text = dtGData.Rows[0].ItemArray[19].ToString();
                            txtVEmail.Text = dtGData.Rows[0].ItemArray[20].ToString();
                            txtVHomePhone.Text = dtGData.Rows[0].ItemArray[21].ToString();
                            txtVWorkPhone.Text = dtGData.Rows[0].ItemArray[22].ToString();
                            txtVCellPhone.Text = dtGData.Rows[0].ItemArray[23].ToString();
                            txtVFax.Text = dtGData.Rows[0].ItemArray[24].ToString();

                        }

                        daVParks.Fill(dtVParks,Convert.ToInt64(sTicketId));
                        dgvVCarMovements.DataSource = dtVParks;
                        dgvVCarMovements.Refresh();

                        daGServices.Fill(dtGServices, Convert.ToInt64(sTicketId));
                        dgvVServices.DataSource = dtGServices;
                        dgvVServices.Refresh();

                        tbcOccurrence.SelectedTab = tabVehicle;

                        ShowDataTab();

                        btnSave.Visible = true;

                        cmbVFault.SelectedIndex = 0;
                        cmbVAccident.SelectedIndex = 0;

                    }
                }

            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
  
        }

        private void btnGAddRecord_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strNotes1 = new StringBuilder(String.Empty);
                string strNotes = "";
                DateTime actionDate = Convert.ToDateTime("2000-01-01");
                int action_id = 0;
                string strAction = "";

                using (frmAddRecord frmAR = new frmAddRecord(ref strNotes1))
                {
                    if (frmAR.ShowDialog() == DialogResult.OK)
                    {
                        strNotes = strNotes1.ToString();
                        actionDate = frmAR.actionDate;
                        action_id = frmAR.action_id;
                        strAction = frmAR.strAction;

                        Int64 maxID = 1;

                        if (dtGHistory.Rows.Count > 0)
                        {
                            //maxID = (Int32)dtGHistory.Compute("Max(act_id)", "");
                            maxID = (Int32)dtGHistory.Rows.Count;
                            maxID = maxID + 1;
                        }

                        dtGHistory.Rows.Add(maxID, 0, GlobalData.UserLogin, GlobalData.UserName, DateTime.Now, action_id, strAction, strNotes, actionDate );
                        dgvGHistory.Refresh();

                    }
                }



            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            string strWarning = "";

            string strPoliceCalled = "N";
            string strReportedBy = "O";
            string strAssignToRM = "N";
            string strLostTimeInjury = "N";

            if (rdbGCheckOut.Checked)
            {
                strReportedBy = "C";
            }

            if (rdbGTelephone.Checked)
            {
                strReportedBy = "T";
            }

            if (rdbGEmail.Checked)
            {
                strReportedBy = "E";
            }

            if (chkGPolice.Checked)
            {
                strPoliceCalled = "Y";
            }

            if (chkAssignToRM.Checked)
            {
                strAssignToRM = "Y";
            }

            if (chkLTInjury.Checked)
            {
                strLostTimeInjury = "Y";
            }


            //Int64 x = Convert.ToInt64(txtOccurrenceNumber.Text);
            if (txtOccurrenceNumber.Text.Length == 0)
            {
                try
                {
                    chkClosed.Visible = true;
                    lblGStatus.Visible = true;

                    InsertOccurrenceData();

                    Open();

                    if (strAssignToRM == "Y" || cmbGType.SelectedValue.ToString() =="3")
                    {
                        SendMail(txtOccurrenceNumber.Text);
                    }

                    btnCustomerCopy.Visible = true;
                    btnInternalCopy.Visible = true;
                    btnFinalRelease.Visible = true;
                    btnFinalReleaseVoucher.Visible = true;
                    btnFinalReleaseRepair.Visible = true;

                    btnCustomerCopyFrench.Visible = true;
                    btnFinalReleaseFrench.Visible = true;
                    btnFinalReleaseVoucherFrench.Visible = true;
                    btnFinalReleaseRepairFrench.Visible = true;
                    btnOccurrencesOverdue.Visible = true;


                    btnAddPicture.Visible = true;
                    btnDeletePicture.Visible = true;
                    btnSaveAs.Visible = true;

                    reportViewer1.Visible = true;
                    lstPictures.Visible = true;
                }
                catch (Exception ex)
                {
                    this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                    MessageBox.Show("Data was NOT saved. ERROR:" + ex.Message.ToString(),"ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1);
                }                

            }
            else
            {
                try
                {
                    UpdateOccurrenceData();

                    if (strAssignedToRMM == "N" && strAssignToRM == "Y")
                    {
                        SendMail(txtOccurrenceNumber.Text);
                    }

                    Open();

                    btnCustomerCopy.Visible = true;
                    btnInternalCopy.Visible = true;
                    btnFinalRelease.Visible = true;
                    btnFinalReleaseVoucher.Visible = true;
                    btnFinalReleaseRepair.Visible = true;

                    btnCustomerCopyFrench.Visible = true;
                    btnFinalReleaseFrench.Visible = true;
                    btnFinalReleaseVoucherFrench.Visible = true;
                    btnFinalReleaseRepairFrench.Visible = true;
                    btnOccurrencesOverdue.Visible = true;
                    


                }
                catch (Exception ex)
                {
                    this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                    MessageBox.Show("Data was NOT saved. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtOccurrenceNumber.Text.Length > 0)
                {


                    DataSet dsOccurrence = new DataSet();

                    using (SqlConnection connOS = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.DATAConnectionString"].ConnectionString))
                    {
                        SqlDataAdapter daOS = new SqlDataAdapter();
                        SqlCommand cmdOS = new SqlCommand();
                        cmdOS.CommandText = "sp_GetOccurrenceData_v2";
                        cmdOS.CommandType = CommandType.StoredProcedure;
                        cmdOS.Connection = connOS;
                        cmdOS.Parameters.AddWithValue("@occurrence_id", txtOccurrenceNumber.Text);
                        cmdOS.Parameters.AddWithValue("@locations", GlobalData.Locations);
                        daOS.SelectCommand = cmdOS;
                        connOS.Open();
                        daOS.Fill(dsOccurrence);
                        connOS.Close();
                    }

                    if (dsOccurrence.Tables[0].Rows.Count == 0)
                    {
                        MessageBox.Show("There is no such occurrence number for your locations");
                    }
                    else
                    {
                        Open();
                    }

                    if (chkAttachments.Checked == false)
                    {
                        lstPictures.Visible = false;
                        btnAddPicture.Visible = false;
                        btnDeletePicture.Visible = false;
                        btnSaveAs.Visible = false;
                    }

                    chkAttachments.Enabled = false;
                }
                else
                {
                    MessageBox.Show("Please enter occurrence number");
                }
            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        #region Documents
        private void btnCustomerCopy_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptCustCopy.rdlc";
                localReport.ReportPath = @"rptCustCopy.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }


        }

        private void btnCustomerCopyFrench_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptCustCopy.rdlc";
                localReport.ReportPath = @"rptCustCopyFr.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }


        private void btnInternalCopy_Click(object sender, EventArgs e)
        {

            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptIntCopy.rdlc";
                localReport.ReportPath = @"rptIntCopy.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

               reportViewer1.LocalReport.SubreportProcessing +=
               new SubreportProcessingEventHandler(DemoSubreportProcessingEventHandler);



                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        void DemoSubreportProcessingEventHandler(object sender, SubreportProcessingEventArgs e)
        {
            //if (orderDetailsData == null)
            //    orderDetailsData = LoadOrderDetailsData();
            e.DataSources.Add(new ReportDataSource("dtRepHistory", (DataTable)dtGHistory));

            DataTable dt = new DataTable();
            dt = (DataTable)(dgvVCarMovements.DataSource);
            dt.Columns[11].ColumnName = "timestamp";
            dt.Columns[14].ColumnName = "first_name";
            dt.Columns[15].ColumnName = "last_name";
            dt.Columns[8].ColumnName = "lot_code";
            dt.Columns[6].ColumnName = "row_code";
            e.DataSources.Add(new ReportDataSource("dtCarMovements", (DataTable)dt));

        }
       
        private void btnFinalRelease_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalRelease.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                foreach (DataRow dr in dtRepOccurrenceData.Rows)
                {
                    //need to set value to MyRow column
                    dr["FinalCostString"] = FinalCostString(dr["final_cost"].ToString());   // or set it to some other value
                }


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnFinalReleaseFrench_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalReleaseFr.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                foreach (DataRow dr in dtRepOccurrenceData.Rows)
                {
                    //need to set value to MyRow column
                    dr["FinalCostString"] = FinalCostStringFrench(dr["final_cost"].ToString());   // or set it to some other value
                }


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnFinalReleaseVoucher_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalReleaseVoucher.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                foreach (DataRow dr in dtRepOccurrenceData.Rows)
                {
                    //need to set value to MyRow column
                    dr["VoucherString"] = VoucherString(dr["vouchers"].ToString());   // or set it to some other value
                }


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnFinalReleaseVoucherFrench_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalReleaseVoucherFr.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                foreach (DataRow dr in dtRepOccurrenceData.Rows)
                {
                    //need to set value to MyRow column
                    dr["VoucherString"] = VoucherStringFrench(dr["vouchers"].ToString());   // or set it to some other value
                }


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
        

        private void btnFinalReleaseRepair_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalReleaseRepair.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                //foreach (DataRow dr in dtRepOccurrenceData.Rows)
                //{
                //    //need to set value to MyRow column
                //    dr["VoucherString"] = VoucherString(dr["vouchers"].ToString());   // or set it to some other value
                //}


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
           
        }

        private void btnFinalReleaseRepairFrench_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptFinalReleaseRepairFr.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                //foreach (DataRow dr in dtRepOccurrenceData.Rows)
                //{
                //    //need to set value to MyRow column
                //    dr["VoucherString"] = VoucherString(dr["vouchers"].ToString());   // or set it to some other value
                //}


                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnOccurrencesOverdue_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptOverdueCases.rdlc";

                dtOccurOverdue.Clear();
                daOccurOverdue.Fill(dtOccurOverdue, GlobalData.Locations.ToString());
               // daOccurOverdue.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                //foreach (DataRow dr in dtRepOccurrenceData.Rows)
                //{
                //    //need to set value to MyRow column
                //    dr["VoucherString"] = VoucherString(dr["vouchers"].ToString());   // or set it to some other value
                //}


                ReportDataSource rdsOccurOverdue = new ReportDataSource();
                rdsOccurOverdue.Value = dtOccurOverdue;
                rdsOccurOverdue.Name = "dsOccurrencesOverdue";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurOverdue);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
        private void btnChequeRequisition_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;

                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptFinalRelease.rdlc";
                localReport.ReportPath = @"rptChequeRequisition.rdlc";

                dtRepOccurrenceDataUser.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                daLocCheque.Fill(dtLocCheques);

                foreach (DataRow dr in dtRepOccurrenceData)
                {
                    foreach (DataRow dr1 in dtLocCheques)
                    {
                        if (dr1["location"].ToString() == dr["location"].ToString())
                        {
                            dr["location"] = dr1["cheque_report"];
                        }
                    }
                    dr["email"] = GlobalData.UserName;
                }

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShowGeneralTab()
        {
            tbcOccurrence.Visible = true;
            lblGDate.Visible = true;
            lblGDateR.Visible = true;
            lblGDescription.Visible = true;
            lblGHistory.Visible = true;
            lblGLocation.Visible = true;
            lblGOpened.Visible = true;
            lblGSubtype.Visible = true;
            lblGType.Visible = true;
            lblGWeather.Visible = true;
            txtGDescription.Visible = true;
            dtpGDate.Visible = true;
            dtpGDateR.Visible = true;
            cmbGSubtype.Visible = true;
            cmbGType.Visible = true;
            cmbGLocation.Visible = true;
            cmbGWeather.Visible = true;
            grpGPolice.Visible = true;
            chkGPolice.Visible = true;
            btnCreate.Visible = false;
            btnFind.Visible = false;
            btnOpen.Visible = false;
            btnGGetData.Visible = true;
            btnGAddRecord.Visible = true;
            dgvGHistory.Visible = true;
            txtOccurrenceNumber.Visible = false;
            lblOccurrenceNumber.Visible = false;
            grpGCost.Visible = true;
            lblGEstimatedCost.Visible = true;
            lblGFinalCost.Visible = true;
            lblGVouchers.Visible = true;
            txtGEstimatedCost.Visible = true;
            txtGFinalCost.Visible = true;
            txtGVouchers.Visible = true;
            lblGReported.Visible = true;
            rdbGCheckOut.Visible = true;
            rdbGOther.Visible = true;
            rdbGTelephone.Visible = true;
            rdbGEmail.Visible = true;
            chkAssignToRM.Visible = true;


            lblGOpened.Text = "Created by " + GlobalData.UserName + " " + DateTime.Now.ToString("MMMM dd, yyyy");

            //daGHistory.Fill(dtGHistory, 0);
            dgvGHistory.DataSource = dtGHistory;
            dgvGHistory.Refresh();
            dgvGHistory.Columns[0].Visible = false;
            dgvGHistory.Columns[1].Visible = false;
            dgvGHistory.Columns[2].Visible = false;
            dgvGHistory.Columns[5].Visible = false;
            //dgvGHistory.Columns[4].Width = 150;
            dgvGHistory.Columns[4].DisplayIndex = 0;
            dgvGHistory.Columns[3].DisplayIndex = 1;
            dgvGHistory.Columns[3].Width = 170;
            dgvGHistory.Columns[4].Width = 110;
            dgvGHistory.Columns[8].DisplayIndex = 4;
            //dgvGHistory.Columns[8].Width = 110; 
            dgvGHistory.Columns[6].DisplayIndex = 2;
            dgvGHistory.Columns[6].Width = 220;
            dgvGHistory.Columns[7].DisplayIndex = 3;
            dgvGHistory.Columns[7].Width = 410;
            dgvGHistory.Columns[4].DefaultCellStyle.Format = "MMM dd, yyyy";
            dgvGHistory.Columns[8].DefaultCellStyle.Format = "MMM dd, yyyy";
            dgvGHistory.Columns[4].HeaderText = "Record Date";
            dgvGHistory.Columns[7].HeaderText = "Action Notes";
            dgvGHistory.Columns[8].HeaderText = "Follow Up";
            dgvGHistory.Columns[6].HeaderText = "Action Type";
            dgvGHistory.Columns[3].HeaderText = "Employee";

            btnCancel.Visible = true;
            chkNoTicket.Visible = true;
        }

        private void ShowDataTab()
        {

            dgvVCarMovements.Columns[0].Visible = false;
            dgvVCarMovements.Columns[1].Visible = false;
            dgvVCarMovements.Columns[2].Visible = false;
            dgvVCarMovements.Columns[3].Visible = false;
            dgvVCarMovements.Columns[9].Visible = false;
            dgvVCarMovements.Columns[10].Visible = false;
            dgvVCarMovements.Columns[13].Visible = false;
            dgvVCarMovements.Columns[14].DisplayIndex = 0;
            dgvVCarMovements.Columns[15].DisplayIndex = 1;
            dgvVCarMovements.Columns[11].DisplayIndex = 2;
            dgvVCarMovements.Columns[8].DisplayIndex = 3;
            dgvVCarMovements.Columns[7].DisplayIndex = 4;
            dgvVCarMovements.Columns[6].DisplayIndex = 5;
            dgvVCarMovements.Columns[5].DisplayIndex = 6;
            dgvVCarMovements.Columns[4].DisplayIndex = 7;
            dgvVCarMovements.Columns[12].DisplayIndex = 8;
            dgvVCarMovements.Columns[11].Width = 200;
            dgvVCarMovements.Columns[5].Width = 120;
            dgvVCarMovements.Columns[15].Width = 200;
            dgvVCarMovements.Columns[14].Width = 140;
            dgvVCarMovements.Columns[8].Width = 50;
            dgvVCarMovements.Columns[6].Width = 60;
            dgvVCarMovements.Columns[4].Width = 50;

            dgvVServices.Columns[0].Visible = false;
            dgvVServices.Columns[2].Visible = false;
            dgvVServices.Columns[3].Visible = false;
            dgvVServices.Columns[4].Visible = false;
            dgvVServices.Columns[6].Visible = false;
            dgvVServices.Columns[7].Visible = false;
            dgvVServices.Columns[8].Visible = false;
            dgvVServices.Columns[9].Visible = false;
            dgvVServices.Columns[11].Visible = false;
            dgvVServices.Columns[10].DisplayIndex = 0;
            dgvVServices.Columns[12].DisplayIndex = 1;
            dgvVServices.Columns[1].DisplayIndex = 2;
            dgvVServices.Columns[5].DisplayIndex = 3;
            dgvVServices.Columns[1].Width = 90;
            dgvVServices.Columns[5].Width = 70;
            dgvVServices.Columns[12].Width = 55;
       
            txtVAddress1.Visible = true;
            txtVAddress2.Visible = true;
            txtVBarCode.Visible = true;
            txtVCellPhone.Visible = true;
            txtVCity.Visible = true;
            txtVCloseDate.Visible = true;
            txtVColour.Visible = true;
            txtVCustomerID.Visible = true;
            txtVEmail.Visible = true;
            txtVFax.Visible = true;
            txtVFirstName.Visible = true;
            txtVHomePhone.Visible = true;
            txtVIssueNumber.Visible = true;
            txtVLastName.Visible = true;
            txtVLicensePlate.Visible = true;
            txtVMake.Visible = true;
            txtVModel.Visible = true;
            txtVModelYear.Visible = true;
            txtVOdometer.Visible = true;
            txtVOpenDate.Visible = true;
            txtVPinNumber.Visible = true;
            txtVPNFUnitNumber.Visible = true;
            cmbVPNFVehicleType.Visible = true;
            txtVPostalCode.Visible = true;
            txtVProvince.Visible = true;
            txtVTicketID.Visible = true;
            txtVTicketNumber.Visible = true;
            txtVVehicleID.Visible = true;
            txtVWorkPhone.Visible = true;
            lblVAccident.Visible = true;
            lblVAddress1.Visible = true;
            lblVAddress2.Visible = true;
            lblVBarCode.Visible = true;
            lblVCarMovements.Visible = true;
            lblVCellPhone.Visible = true;
            lblVCity.Visible = true;
            lblVColour.Visible = true;
            lblVCustomerID.Visible = true;
            lblVDateClose.Visible = true;
            lblVDateOpen.Visible = true;
            lblVEmail.Visible = true;
            lblVFault.Visible = true;
            lblVFax.Visible = true;
            lblVFirstName.Visible = true;
            lblVHomePhone.Visible = true;
            lblVIssueNumber.Visible = true;
            lblVLastName.Visible = true;
            lblVLicensePlate.Visible = true;
            lblVMake.Visible = true;
            lblVModel.Visible = true;
            lblVModelYear.Visible = true;
            lblVNumberOfParks.Visible = true;
            lblVOdometer.Visible = true;
            lblVPinNumber.Visible = true;
            lblVPNFUnitNumber.Visible = true;
            lblVPNFVehicleType.Visible = true;
            lblVPostalCode.Visible = true;
            lblVProvince.Visible = true;
            lblVServices.Visible = true;
            lblVTicketID.Visible = true;
            lblVTicketNumber.Visible = true;
            lblVVehicleID.Visible = true;
            lblVWorkPhone.Visible = true;
            grpVCustomer.Visible = true;
            grpVPNF.Visible = true;
            grpVTicket.Visible = true;
            grpVVehicle.Visible = true;
            dgvVCarMovements.Visible = true;
            dgvVServices.Visible = true;
            cmbVFault.Visible = true;
            cmbVAccident.Visible = true;
            txtVNumberOfParks.Visible = true;


        }

        private void InsertOccurrenceData()
        {
            string strPoliceCalled = "N";
            string strReportedBy = "O";
            string strAssignToRM = "N";
            string strLostTimeInjury = "N";

            if (rdbGCheckOut.Checked)
            {
                strReportedBy = "C";
            }

            if (rdbGTelephone.Checked)
            {
                strReportedBy = "T";
            }

            if (rdbGEmail.Checked)
            {
                strReportedBy = "E";
            }

            if (chkGPolice.Checked)
            {
                strPoliceCalled = "Y";
            }

            if (chkAssignToRM.Checked)
            {
                strAssignToRM = "Y";
            }

            if (chkLTInjury.Checked)
            {
                strLostTimeInjury = "Y";
            }


            DataTable _dtTicketServices;
            _dtTicketServices = new DataTable("TicketServices");
            _dtTicketServices.Columns.Add("occurrence_id", typeof(Int64));
            _dtTicketServices.Columns.Add("ticket_id", typeof(Int64));
            _dtTicketServices.Columns.Add("service_id", typeof(string));
            _dtTicketServices.Columns.Add("service_description", typeof(string));
            _dtTicketServices.Columns.Add("quantity", typeof(int));
            _dtTicketServices.Columns.Add("rate", typeof(decimal));
            _dtTicketServices.Columns.Add("status", typeof(string));

            int icount = 1;
            foreach (DataRow dr in dtGServices.Rows)
            {
                _dtTicketServices.Rows.Add(0, 0, icount, dr.ItemArray[10].ToString(), Convert.ToInt32(dr.ItemArray[1].ToString()), Convert.ToDecimal(dr.ItemArray[12].ToString()),dr.ItemArray[5].ToString());
                icount = icount + 1;
            }

            DataTable _dtCarMovements;
            _dtCarMovements = new DataTable("CarMovements");
            _dtCarMovements.Columns.Add("occurrence_id", typeof(Int64));
            _dtCarMovements.Columns.Add("ticket_id", typeof(Int64));
            _dtCarMovements.Columns.Add("employee_login", typeof(string));
            _dtCarMovements.Columns.Add("date_time", typeof(DateTime));
            _dtCarMovements.Columns.Add("park_type", typeof(string));
            _dtCarMovements.Columns.Add("lot", typeof(string));
            _dtCarMovements.Columns.Add("row", typeof(string));
            _dtCarMovements.Columns.Add("section", typeof(string));
            _dtCarMovements.Columns.Add("spot", typeof(string));
            _dtCarMovements.Columns.Add("row_type", typeof(string));
            _dtCarMovements.Columns.Add("employee_first_name", typeof(string));
            _dtCarMovements.Columns.Add("employee_last_name", typeof(string));
            _dtCarMovements.Columns.Add("status", typeof(string));
            _dtCarMovements.Columns.Add("ticket_number", typeof(string));
            _dtCarMovements.Columns.Add("spot_id", typeof(int));
            _dtCarMovements.Columns.Add("vehicle_park_id", typeof(Int64));
            _dtCarMovements.Columns.Add("employee_id", typeof(int));

            foreach (DataRow dr in dtVParks.Rows)
            {
                if (dr.ItemArray[9].ToString().Length > 0)
                {
                    _dtCarMovements.Rows.Add(0, 0, dr.ItemArray[13].ToString(), Convert.ToDateTime(dr.ItemArray[11].ToString()), dr.ItemArray[12].ToString(),
                         dr.ItemArray[8].ToString(), dr.ItemArray[6].ToString(), dr.ItemArray[5].ToString(), dr.ItemArray[4].ToString(), dr.ItemArray[7].ToString(),
                         dr.ItemArray[14].ToString(), dr.ItemArray[15].ToString(), dr.ItemArray[10].ToString(), dr.ItemArray[2].ToString(), 
                         Convert.ToInt32(dr.ItemArray[3].ToString()), Convert.ToInt64(dr.ItemArray[0].ToString()),Convert.ToInt32(dr.ItemArray[9].ToString()));
                }
                else
                {
                    _dtCarMovements.Rows.Add(0, 0, dr.ItemArray[13].ToString(), Convert.ToDateTime(dr.ItemArray[11].ToString()), dr.ItemArray[12].ToString(),
                         dr.ItemArray[8].ToString(), dr.ItemArray[6].ToString(), dr.ItemArray[5].ToString(), dr.ItemArray[4].ToString(), dr.ItemArray[7].ToString(),
                         dr.ItemArray[14].ToString(), dr.ItemArray[15].ToString(), dr.ItemArray[10].ToString(), dr.ItemArray[2].ToString(),
                         Convert.ToInt32(dr.ItemArray[3].ToString()), Convert.ToInt64(dr.ItemArray[0].ToString()), DBNull.Value);
                }
            }

            DataTable _dtHistory;
            _dtHistory = new DataTable("History");
            _dtHistory.Columns.Add("act_id", typeof(int));
            _dtHistory.Columns.Add("occurrence_id", typeof(Int64));
            _dtHistory.Columns.Add("employee_login", typeof(string));
            _dtHistory.Columns.Add("employee_name", typeof(string));
            _dtHistory.Columns.Add("record_date", typeof(DateTime));
            _dtHistory.Columns.Add("action_id", typeof(int));
            _dtHistory.Columns.Add("notes", typeof(string));
            _dtHistory.Columns.Add("action_date", typeof(DateTime));

            foreach (DataRow dr in dtGHistory.Rows)
            {
                _dtHistory.Rows.Add(Convert.ToInt32(dr.ItemArray[0].ToString()), 0, dr.ItemArray[2].ToString(), dr.ItemArray[3].ToString(),
                    Convert.ToDateTime(dr.ItemArray[4].ToString()), Convert.ToInt32(dr.ItemArray[5].ToString()), dr.ItemArray[7].ToString(),
                    Convert.ToDateTime(dr.ItemArray[8].ToString())); 
            }

            string sqlOccurrenceUpdate = "dbo.sp_InsertOccurrenceData_v3";

            using (SqlConnection connOU = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.DATAConnectionString"].ConnectionString))
            {
                connOU.Open();
                SqlCommand cmdOU = connOU.CreateCommand();
                cmdOU.CommandText = sqlOccurrenceUpdate;
                cmdOU.CommandType = CommandType.StoredProcedure;

                cmdOU.Parameters.AddWithValue("@type_id", Convert.ToInt32(cmbGType.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@subtype_id", Convert.ToInt32(cmbGSubtype.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@location_id", Convert.ToInt32(cmbGLocation.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@weather_id", Convert.ToInt32(cmbGWeather.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@description", txtGDescription.Text);
                cmdOU.Parameters.AddWithValue("@employee_name_opened", GlobalData.UserName);
                cmdOU.Parameters.AddWithValue("@created_date", DateTime.Now);
                if (strStatus == "O" && chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", GlobalData.UserName);
                    cmdOU.Parameters.AddWithValue("@close_date", DateTime.Now);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                if (strStatus == "C" && chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", strEmployeeClosed);
                    cmdOU.Parameters.AddWithValue("@close_date", dtDateClosed);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                if (strStatus == "O" && !chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@close_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@status", "O");
                }
                if (strStatus == "C" && !chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", strEmployeeClosed);
                    cmdOU.Parameters.AddWithValue("@close_date", dtDateClosed);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                cmdOU.Parameters.AddWithValue("@date_of_occurrence", dtpGDate.Value);
                cmdOU.Parameters.AddWithValue("@date_of_occurrence_reported", dtpGDateR.Value);
                if (txtGEstimatedCost.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@estimated_cost", Convert.ToDecimal(txtGEstimatedCost.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@estimated_cost", DBNull.Value);
                }
                if (txtGFinalCost.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@final_cost", Convert.ToDecimal(txtGFinalCost.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@final_cost", DBNull.Value);
                }
                if (txtGVouchers.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@vouchers", Convert.ToInt32(txtGVouchers.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@vouchers", DBNull.Value);
                }

                cmdOU.Parameters.AddWithValue("@police_called", strPoliceCalled);
                if (chkGPolice.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@date_police_called", dtpGDatePolice.Value);
                    cmdOU.Parameters.AddWithValue("@police_report_badge_number", txtGBadge.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@date_police_called", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@police_report_badge_number", DBNull.Value);
                }
                cmdOU.Parameters.AddWithValue("@pnf_driver_at_fault", cmbVFault.Text);
                cmdOU.Parameters.AddWithValue("@accident_preventable", cmbVAccident.Text);
                cmdOU.Parameters.AddWithValue("@reported_by", strReportedBy);
                cmdOU.Parameters.AddWithValue("@assign_to_rm", strAssignToRM);

                cmdOU.Parameters.AddWithValue("@lost_time_injury", strLostTimeInjury);
                if (chkLTInjury.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@days_lost", Convert.ToInt32(txtGLostDays.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@days_lost", DBNull.Value);
                }

                //********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@ticket_id", Convert.ToInt64(txtVTicketID.Text));
                cmdOU.Parameters.AddWithValue("@ticket_number", txtVTicketNumber.Text);
                cmdOU.Parameters.AddWithValue("@issue_number", txtVIssueNumber.Text);
                cmdOU.Parameters.AddWithValue("@pin_number", txtVPinNumber.Text);
                cmdOU.Parameters.AddWithValue("@bar_code", txtVBarCode.Text);
                DateTime dtt;
                if (DateTime.TryParse(txtVOpenDate.Text, out dtt))
                {
                    cmdOU.Parameters.AddWithValue("@ticket_open_date", txtVOpenDate.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@ticket_open_date", DBNull.Value);
                }
                if (DateTime.TryParse(txtVCloseDate.Text, out dtt))
                {
                    cmdOU.Parameters.AddWithValue("@ticket_close_date", txtVCloseDate.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@ticket_close_date", DBNull.Value);
                }
                //*********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@vehicle_id", Convert.ToInt64(txtVCustomerID.Text));
                cmdOU.Parameters.AddWithValue("@license_plate", txtVLicensePlate.Text);
                cmdOU.Parameters.AddWithValue("@v_province", txtVProvince.Text);
                cmdOU.Parameters.AddWithValue("@make", txtVMake.Text);
                cmdOU.Parameters.AddWithValue("@model", txtVModel.Text);
                cmdOU.Parameters.AddWithValue("@model_year", txtVModelYear.Text);
                cmdOU.Parameters.AddWithValue("@colour", txtVColour.Text);
                cmdOU.Parameters.AddWithValue("@odometer", txtVOdometer.Text);
                cmdOU.Parameters.AddWithValue("@unit_number", txtVPNFUnitNumber.Text);
                cmdOU.Parameters.AddWithValue("@pnf_vehicle_type_id", cmbVPNFVehicleType.SelectedValue);
                cmdOU.Parameters.AddWithValue("@number_of_parks", Convert.ToInt32(txtVNumberOfParks.Text));
                //*********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@cust_id", Convert.ToInt64(txtVCustomerID.Text));
                cmdOU.Parameters.AddWithValue("@last_name", txtVLastName.Text);
                cmdOU.Parameters.AddWithValue("@first_name", txtVFirstName.Text);
                cmdOU.Parameters.AddWithValue("@address_1", txtVAddress1.Text);
                cmdOU.Parameters.AddWithValue("@address_2", txtVAddress2.Text);
                cmdOU.Parameters.AddWithValue("@city", txtVCity.Text);
                cmdOU.Parameters.AddWithValue("@c_province", txtVProvince.Text);
                cmdOU.Parameters.AddWithValue("@postal_code", txtVPostalCode.Text);
                cmdOU.Parameters.AddWithValue("@email", txtVEmail.Text);
                cmdOU.Parameters.AddWithValue("@home_phone", txtVHomePhone.Text);
                cmdOU.Parameters.AddWithValue("@work_phone", txtVWorkPhone.Text);
                cmdOU.Parameters.AddWithValue("@cell_phone", txtVCellPhone.Text);
                cmdOU.Parameters.AddWithValue("@fax", txtVFax.Text);

                SqlParameter tvpParam1 = cmdOU.Parameters.AddWithValue("@ticket_services_TVP", _dtTicketServices); //Needed TVP
                tvpParam1.SqlDbType = SqlDbType.Structured;

                SqlParameter tvpParam2 = cmdOU.Parameters.AddWithValue("@car_movements_TVP", _dtCarMovements); //Needed TVP
                tvpParam2.SqlDbType = SqlDbType.Structured;

                SqlParameter tvpParam3 = cmdOU.Parameters.AddWithValue("@occurrence_history_TVP", _dtHistory); //Needed TVP
                tvpParam3.SqlDbType = SqlDbType.Structured;

                //*****Risk Assessment**********************************************************************************************

                string strRReported = "N";
                string strRAssigned = "N";
                string strRResolvedIC = "N";
                string strRResolvedC = "N";
                string strRLetter = "N";
                string strRSOC = "N";
                string strRDefence = "N";
                string strRPretrial = "N";
                string strRTrial = "N";
                string strRJudgement = "N";

                if (chkRReported.Checked)
                {
                    strRReported = "Y";
                }
                cmdOU.Parameters.AddWithValue("@report_to_iaa", strRReported);
                if (chkRReported.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@report_to_iaa_date", dtpRReportedDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@report_to_iaa_date", DBNull.Value);
                }

                if (chkRAssigned.Checked)
                {
                    strRAssigned = "Y";
                }
                cmdOU.Parameters.AddWithValue("@claim_assigned", strRAssigned);
                if (chkRAssigned.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@claim_number", txtRClaim.Text);
                    cmdOU.Parameters.AddWithValue("@claim_date", dtpRAssignedDate.Value);
                    cmdOU.Parameters.AddWithValue("@adjuster", txtRAdjuster.Text);
                    if (chkRResolvedIC.Checked)
                    {
                        strRResolvedIC = "Y";
                    }
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic", strRResolvedIC);
                    if (chkRResolvedIC.Checked)
                    {
                        cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", dtpRResolvedICDate.Value);
                    }
                    else
                    {
                        cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", DBNull.Value);
                    }
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@claim_number", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@claim_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@adjuster", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", DBNull.Value);
                }

                if (chkRResolvedC.Checked)
                {
                    strRResolvedC = "Y";
                }
                cmdOU.Parameters.AddWithValue("@resolved_to_customer", strRResolvedC);
                if (chkRResolvedC.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@resolved_to_customer_date", dtpRResolvedCDate.Value);
                    if (chkRLetter.Checked)
                    {
                        strRLetter = "Y";
                    }
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent", strRLetter);
                    if (chkRLetter.Checked)
                    {
                        cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", dtpRLetterDate.Value);
                    }
                    else
                    {
                        cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", DBNull.Value);
                    }
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@resolved_to_customer_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", DBNull.Value);
                }

                if (chkRSOC.Checked)
                {
                    strRSOC = "Y";
                }
                cmdOU.Parameters.AddWithValue("@soc_received", strRSOC);
                if (chkRSOC.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@soc_received_date", dtpRSOCDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@soc_received_date", DBNull.Value);
                }

                if (chkRDefence.Checked)
                {
                    strRDefence = "Y";
                }
                cmdOU.Parameters.AddWithValue("@defence_filed", strRDefence);
                if (chkRDefence.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@defence_filed_date", dtpRDefenceDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@defence_filed_date", DBNull.Value);
                }

                if (chkRPreTrial.Checked)
                {
                    strRPretrial = "Y";
                }
                cmdOU.Parameters.AddWithValue("@pretrial", strRPretrial);
                if (chkRPreTrial.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@pretrial_date", dtpRPreTrialDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@pretrial_date", DBNull.Value);
                }

                if (chkRTrial.Checked)
                {
                    strRTrial = "Y";
                }
                cmdOU.Parameters.AddWithValue("@trial", strRTrial);
                if (chkRTrial.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@trial_date", dtpRTrialDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@trial_date", DBNull.Value);
                }

                if (chkRJudgement.Checked)
                {
                    strRJudgement = "Y";
                }
                cmdOU.Parameters.AddWithValue("@judgement", strRJudgement);
                if (chkRJudgement.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@judgement_date", dtpRJudgementDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@judgement_date", DBNull.Value);
                }

                if (strAssignToRM == "Y")
                {
                    cmdOU.Parameters.AddWithValue("@notes", txtRNotes.Text);

                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@notes", DBNull.Value);

                }


                //************************************************************************************

                SqlParameter occurID = new SqlParameter("@OccurrenceIDout", SqlDbType.BigInt);
                occurID.Direction = ParameterDirection.Output;
                cmdOU.Parameters.Add(occurID);

                cmdOU.ExecuteNonQuery();

                connOU.Close();

                txtOccurrenceNumber.Text = occurID.Value.ToString();

                string folderName = ConfigurationManager.AppSettings["AttachmentsPath"];
                string pathString = System.IO.Path.Combine(folderName, txtOccurrenceNumber.Text);
                System.IO.Directory.CreateDirectory(pathString);

            }
        }

        private void UpdateOccurrenceData()
        {
            string strPoliceCalled = "N";
            string strReportedBy = "O";
            string strAssignToRM = "N";
            string strLostTimeInjury = "N";

            if (rdbGCheckOut.Checked)
            {
                strReportedBy = "C";
            }

            if (rdbGTelephone.Checked)
            {
                strReportedBy = "T";
            }

            if (rdbGEmail.Checked)
            {
                strReportedBy = "E";
            }

            if (chkGPolice.Checked)
            {
                strPoliceCalled = "Y";
            }

            if (chkAssignToRM.Checked)
            {
                strAssignToRM = "Y";
            }

            if (chkLTInjury.Checked)
            {
                strLostTimeInjury = "Y";
            }

            DataTable _dtHistory;
            _dtHistory = new DataTable("History");
            _dtHistory.Columns.Add("act_id", typeof(int));
            _dtHistory.Columns.Add("occurrence_id", typeof(Int64));
            _dtHistory.Columns.Add("employee_login", typeof(string));
            _dtHistory.Columns.Add("employee_name", typeof(string));
            _dtHistory.Columns.Add("record_date", typeof(DateTime));
            _dtHistory.Columns.Add("action_id", typeof(int));
            _dtHistory.Columns.Add("notes", typeof(string));
            _dtHistory.Columns.Add("action_date", typeof(DateTime));

            foreach (DataRow dr in dtGHistory.Rows)
            {
                if (Convert.ToInt32(dr.ItemArray[0].ToString()) > dtGHistoryM.Rows.Count)
                {
                    _dtHistory.Rows.Add(Convert.ToInt32(dr.ItemArray[0].ToString()), 0, dr.ItemArray[2].ToString(), dr.ItemArray[3].ToString(),
                        Convert.ToDateTime(dr.ItemArray[4].ToString()), Convert.ToInt32(dr.ItemArray[5].ToString()), dr.ItemArray[7].ToString(),
                        Convert.ToDateTime(dr.ItemArray[8].ToString()));
                }
            }

            string sqlOccurrenceUpdate = "dbo.sp_SaveOccurrenceData_v3";

            using (SqlConnection connOU = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.DATAConnectionString"].ConnectionString))
            {
                connOU.Open();
                SqlCommand cmdOU = connOU.CreateCommand();
                cmdOU.CommandText = sqlOccurrenceUpdate;
                cmdOU.CommandType = CommandType.StoredProcedure;

                cmdOU.Parameters.AddWithValue("@occurrence_id", Convert.ToInt64(txtOccurrenceNumber.Text));
                cmdOU.Parameters.AddWithValue("@type_id", Convert.ToInt32(cmbGType.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@subtype_id", Convert.ToInt32(cmbGSubtype.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@location_id", Convert.ToInt32(cmbGLocation.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@weather_id", Convert.ToInt32(cmbGWeather.SelectedValue.ToString()));
                cmdOU.Parameters.AddWithValue("@description", txtGDescription.Text);
                //cmdOU.Parameters.AddWithValue("@employee_name_opened", GlobalData.UserName);
                //cmdOU.Parameters.AddWithValue("@created_date", DateTime.Now);
                if (strStatus == "O" && chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", GlobalData.UserName);
                    cmdOU.Parameters.AddWithValue("@close_date", DateTime.Now);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                if (strStatus == "C" && chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", strEmployeeClosed);
                    cmdOU.Parameters.AddWithValue("@close_date", dtDateClosed);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                if (strStatus == "O" && !chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@close_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@status", "O");
                }
                if (strStatus == "C" && !chkClosed.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@employee_name_closed", strEmployeeClosed);
                    cmdOU.Parameters.AddWithValue("@close_date", dtDateClosed);
                    cmdOU.Parameters.AddWithValue("@status", "C");
                }
                cmdOU.Parameters.AddWithValue("@date_of_occurrence", dtpGDate.Value);
                cmdOU.Parameters.AddWithValue("@date_of_occurrence_reported", dtpGDateR.Value);
                if (txtGEstimatedCost.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@estimated_cost", Convert.ToDecimal(txtGEstimatedCost.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@estimated_cost", DBNull.Value);
                }
                if (txtGFinalCost.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@final_cost", Convert.ToDecimal(txtGFinalCost.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@final_cost", DBNull.Value);
                }
                if (txtGVouchers.Text.Length > 0)
                {
                    cmdOU.Parameters.AddWithValue("@vouchers", Convert.ToInt32(txtGVouchers.Text));
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@vouchers", DBNull.Value);
                }

                cmdOU.Parameters.AddWithValue("@police_called", strPoliceCalled);
                if (chkGPolice.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@date_police_called", dtpGDatePolice.Value);
                    cmdOU.Parameters.AddWithValue("@police_report_badge_number", txtGBadge.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@date_police_called", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@police_report_badge_number", DBNull.Value);
                }
                cmdOU.Parameters.AddWithValue("@pnf_driver_at_fault", cmbVFault.Text);
                cmdOU.Parameters.AddWithValue("@accident_preventable", cmbVAccident.Text);
                cmdOU.Parameters.AddWithValue("@reported_by", strReportedBy);
                cmdOU.Parameters.AddWithValue("@assign_to_rm", strAssignToRM);

                cmdOU.Parameters.AddWithValue("@lost_time_injury", strLostTimeInjury);
                if (chkLTInjury.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@days_lost", txtGLostDays.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@days_lost", DBNull.Value);
                }
                //********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@ticket_id", Convert.ToInt64(txtVTicketID.Text));
                cmdOU.Parameters.AddWithValue("@ticket_number", txtVTicketNumber.Text);
                cmdOU.Parameters.AddWithValue("@issue_number", txtVIssueNumber.Text);
                cmdOU.Parameters.AddWithValue("@pin_number", txtVPinNumber.Text);
                cmdOU.Parameters.AddWithValue("@bar_code", txtVBarCode.Text);
                DateTime dtt;
                if (DateTime.TryParse(txtVOpenDate.Text, out dtt))
                {
                    cmdOU.Parameters.AddWithValue("@ticket_open_date", txtVOpenDate.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@ticket_open_date", DBNull.Value);
                }
                if (DateTime.TryParse(txtVCloseDate.Text, out dtt))
                {
                    cmdOU.Parameters.AddWithValue("@ticket_close_date", txtVCloseDate.Text);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@ticket_close_date", DBNull.Value);
                }
                //*********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@vehicle_id", Convert.ToInt64(txtVCustomerID.Text));
                cmdOU.Parameters.AddWithValue("@license_plate", txtVLicensePlate.Text);
                cmdOU.Parameters.AddWithValue("@v_province", txtVProvince.Text);
                cmdOU.Parameters.AddWithValue("@make", txtVMake.Text);
                cmdOU.Parameters.AddWithValue("@model", txtVModel.Text);
                cmdOU.Parameters.AddWithValue("@model_year", txtVModelYear.Text);
                cmdOU.Parameters.AddWithValue("@colour", txtVColour.Text);
                cmdOU.Parameters.AddWithValue("@odometer", txtVOdometer.Text);
                cmdOU.Parameters.AddWithValue("@unit_number", txtVPNFUnitNumber.Text);
                cmdOU.Parameters.AddWithValue("@pnf_vehicle_type_id", cmbVPNFVehicleType.SelectedValue);
                cmdOU.Parameters.AddWithValue("@number_of_parks", Convert.ToInt32(txtVNumberOfParks.Text));
                //*********************************************************************************************************************
                cmdOU.Parameters.AddWithValue("@cust_id", Convert.ToInt64(txtVCustomerID.Text));
                cmdOU.Parameters.AddWithValue("@last_name", txtVLastName.Text);
                cmdOU.Parameters.AddWithValue("@first_name", txtVFirstName.Text);
                cmdOU.Parameters.AddWithValue("@address_1", txtVAddress1.Text);
                cmdOU.Parameters.AddWithValue("@address_2", txtVAddress2.Text);
                cmdOU.Parameters.AddWithValue("@city", txtVCity.Text);
                cmdOU.Parameters.AddWithValue("@c_province", txtVProvince.Text);
                cmdOU.Parameters.AddWithValue("@postal_code", txtVPostalCode.Text);
                cmdOU.Parameters.AddWithValue("@email", txtVEmail.Text);
                cmdOU.Parameters.AddWithValue("@home_phone", txtVHomePhone.Text);
                cmdOU.Parameters.AddWithValue("@work_phone", txtVWorkPhone.Text);
                cmdOU.Parameters.AddWithValue("@cell_phone", txtVCellPhone.Text);
                cmdOU.Parameters.AddWithValue("@fax", txtVFax.Text);

                SqlParameter tvpParam3 = cmdOU.Parameters.AddWithValue("@occurrence_history_TVP", _dtHistory); //Needed TVP
                tvpParam3.SqlDbType = SqlDbType.Structured;

                //--------------------------------------------------------------------------------------------------------------------------
                string strRReported = "N";
                string strRAssigned = "N";
                string strRResolvedIC = "N";
                string strRResolvedC = "N";
                string strRLetter = "N";
                string strRSOC = "N";
                string strRDefence = "N";
                string strRPretrial = "N";
                string strRTrial = "N";
                string strRJudgement = "N";

                if (chkRReported.Checked)
                {
                    strRReported = "Y";
                }
                cmdOU.Parameters.AddWithValue("@report_to_iaa", strRReported);
                if (chkRReported.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@report_to_iaa_date", dtpRReportedDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@report_to_iaa_date", DBNull.Value);
                }

                if (chkRAssigned.Checked)
                {
                    strRAssigned = "Y";
                }
                cmdOU.Parameters.AddWithValue("@claim_assigned", strRAssigned);
                if (chkRAssigned.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@claim_number", txtRClaim.Text);
                    cmdOU.Parameters.AddWithValue("@claim_date", dtpRAssignedDate.Value);
                    cmdOU.Parameters.AddWithValue("@adjuster", txtRAdjuster.Text);
                    if (chkRResolvedIC.Checked)
                    {
                        strRResolvedIC = "Y";
                    }
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic", strRResolvedIC);
                    if (chkRResolvedIC.Checked)
                    {
                        cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", dtpRResolvedICDate.Value);
                    }
                    else
                    {
                        cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", DBNull.Value);
                    }
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@claim_number", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@claim_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@adjuster", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@resolved_by_ic_date", DBNull.Value);
                }

                if (chkRResolvedC.Checked)
                {
                    strRResolvedC = "Y";
                }
                cmdOU.Parameters.AddWithValue("@resolved_to_customer", strRResolvedC);
                if (chkRResolvedC.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@resolved_to_customer_date", dtpRResolvedCDate.Value);
                    if (chkRLetter.Checked)
                    {
                        strRLetter = "Y";
                    }
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent", strRLetter);
                    if (chkRLetter.Checked)
                    {
                        cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", dtpRLetterDate.Value);
                    }
                    else
                    {
                        cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", DBNull.Value);
                    }
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@resolved_to_customer_date", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent", DBNull.Value);
                    cmdOU.Parameters.AddWithValue("@denial_letter_sent_date", DBNull.Value);
                }

                if (chkRSOC.Checked)
                {
                    strRSOC = "Y";
                }
                cmdOU.Parameters.AddWithValue("@soc_received", strRSOC);
                if (chkRSOC.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@soc_received_date", dtpRSOCDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@soc_received_date", DBNull.Value);
                }

                if (chkRDefence.Checked)
                {
                    strRDefence = "Y";
                }
                cmdOU.Parameters.AddWithValue("@defence_filed", strRDefence);
                if (chkRDefence.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@defence_filed_date", dtpRDefenceDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@defence_filed_date", DBNull.Value);
                }

                if (chkRPreTrial.Checked)
                {
                    strRPretrial = "Y";
                }
                cmdOU.Parameters.AddWithValue("@pretrial", strRPretrial);
                if (chkRPreTrial.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@pretrial_date", dtpRPreTrialDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@pretrial_date", DBNull.Value);
                }

                if (chkRTrial.Checked)
                {
                    strRTrial = "Y";
                }
                cmdOU.Parameters.AddWithValue("@trial", strRTrial);
                if (chkRTrial.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@trial_date", dtpRTrialDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@trial_date", DBNull.Value);
                }

                if (chkRJudgement.Checked)
                {
                    strRJudgement = "Y";
                }
                cmdOU.Parameters.AddWithValue("@judgement", strRJudgement);
                if (chkRJudgement.Checked)
                {
                    cmdOU.Parameters.AddWithValue("@judgement_date", dtpRJudgementDate.Value);
                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@judgement_date", DBNull.Value);
                }

                if (strAssignToRM == "Y")
                {
                    cmdOU.Parameters.AddWithValue("@notes", txtRNotes.Text);

                }
                else
                {
                    cmdOU.Parameters.AddWithValue("@notes", DBNull.Value);

                }

                //--------------------------------------------------------------------------------
                //cmdOU.ExecuteScalar();
                cmdOU.ExecuteNonQuery();

                connOU.Close();
            }
        }

        private void Open()
        {

            #region Get Occurrence Data
                
            DataSet dsOccurrence = new DataSet();

            using (SqlConnection connOS = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.DATAConnectionString"].ConnectionString))
            {
                SqlDataAdapter daOS = new SqlDataAdapter();
                SqlCommand cmdOS = new SqlCommand();
                cmdOS.CommandText = "sp_GetOccurrenceData_v2";
                cmdOS.CommandType = CommandType.StoredProcedure;
                cmdOS.Connection = connOS;
                cmdOS.Parameters.AddWithValue("@occurrence_id", txtOccurrenceNumber.Text );
                cmdOS.Parameters.AddWithValue("@locations", GlobalData.Locations);

                daOS.SelectCommand = cmdOS;
                // add table mappings to the SqlDataAdapter
                daOS.TableMappings.Add("Table", "Occurrences");
                daOS.TableMappings.Add("Table1", "Tickets");
                daOS.TableMappings.Add("Table2", "Vehicles");
                daOS.TableMappings.Add("Table3", "Customers");
                daOS.TableMappings.Add("Table4", "Services");
                daOS.TableMappings.Add("Table5", "Movements");
                daOS.TableMappings.Add("Table6", "History");
                daOS.TableMappings.Add("Table7", "Risk");

                connOS.Open();
                daOS.Fill(dsOccurrence);
                connOS.Close();
            }

            #endregion

            ShowGeneralTab();
            btnSave.Visible = true;

            txtOccurrenceNumber.Visible = true;
            txtOccurrenceNumber.ReadOnly = true;
            lblOccurrenceNumber.Visible = true;

            #region Fill out General fields

            txtOccurrenceNumber.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[0].ToString();

            //OccurrenceNumber = Convert.ToInt64(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[0].ToString());

            cmbGType.SelectedIndex = Convert.ToInt32(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[1].ToString()) - 1;
            cmbGSubtype.SelectedValue = Convert.ToInt32(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[2].ToString());
            //cmbGSubtype.SelectedIndex = Convert.ToInt32(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[2].ToString()) - 1;
            cmbGLocation.SelectedValue = Convert.ToInt32(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[3].ToString());
            cmbGWeather.SelectedIndex = Convert.ToInt32(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[4].ToString()) - 1;
            txtGDescription.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[5].ToString();
            lblGOpened.Text = "Created by " + dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[6].ToString() + " " + 
                Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[7].ToString()).ToString("MMMM dd, yyyy");
            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[8].ToString().Length > 0)
            {
                lblGClosed.Text = "Closed by " + dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[8].ToString() + " " +
                Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[9].ToString()).ToString("MMMM dd, yyyy");
                strStatus = "C";
                strEmployeeClosed = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[8].ToString();
                dtDateClosed = Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[9].ToString());
                lblGClosed.Visible = true;
            }
            else
            {
                lblGClosed.Text = "";
                strStatus = "O";
                strEmployeeClosed = "";
            }
            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[10].ToString() == "C")
            {
                lblGStatus.Text = "Status: Closed";
                chkClosed.Checked = true;
                strStatus = "C";
                strEmployeeClosed = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[8].ToString();
                dtDateClosed = Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[9].ToString());
                lblGClosed.Visible = true;
            }
            else
            {
                lblGStatus.Text = "Status: Opened";
                chkClosed.Checked = false;
                strStatus = "O";
                strEmployeeClosed = "";
            }
            dtpGDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[11].ToString());
            dtpGDateR.Value = Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[12].ToString());
            txtGEstimatedCost.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[13].ToString();               
            txtGFinalCost.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[14].ToString();
            txtGVouchers.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[22].ToString();

            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[15].ToString() == "Y")
            {
                chkGPolice.Checked = true;
                dtpGDatePolice.Value = Convert.ToDateTime(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[16].ToString());
                txtGBadge.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[17].ToString();
            }
            else
            {
                chkGPolice.Checked = false;
                dtpGDatePolice.Visible = false;
                txtGBadge.Visible =false;
            }

            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[21].ToString() == "Y")
            {
                chkAssignToRM.Checked = true;
                strAssignedToRMM = "Y";
            }
            else
            {
                chkAssignToRM.Checked = false;
                strAssignedToRMM = "N";
            }

            cmbVFault.SelectedIndex = cmbVFault.FindString(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[18].ToString());
            cmbVAccident.SelectedIndex = cmbVAccident.FindString(dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[19].ToString());

            //cmbVFault.SelectedText = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[18].ToString();
            //cmbVAccident.SelectedText = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[19].ToString();

            rdbGOther.Checked = true;
            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[20].ToString()== "T")
            {
                rdbGTelephone.Checked = true;
            }
            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[20].ToString()== "C")
            {
                rdbGCheckOut.Checked = true;
            }

            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[20].ToString()== "E")
            {
                rdbGEmail.Checked = true;
            }

            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[20].ToString()== "O")
            {
                rdbGOther.Checked = true;
            }

            if (dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[23].ToString() == "Y")
            {
                chkLTInjury.Checked = true;
                txtGLostDays.Text = dsOccurrence.Tables["Occurrences"].Rows[0].ItemArray[24].ToString();
                txtGLostDays.Visible = true;
            }
            else
            {
                chkLTInjury.Checked = false;
                txtGLostDays.Visible = false;
            }

            #endregion

            #region Fill out Data fields

            txtVTicketID.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[1].ToString();
            txtVTicketNumber.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[2].ToString();
            txtVIssueNumber.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[3].ToString();
            txtVPinNumber.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[4].ToString();
            txtVBarCode.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[5].ToString();
            txtVOpenDate.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[6].ToString();
            txtVCloseDate.Text = dsOccurrence.Tables["Tickets"].Rows[0].ItemArray[7].ToString();
            txtVVehicleID.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[1].ToString();
            txtVCustomerID.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[1].ToString();
            txtVLicensePlate.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[2].ToString();
            txtVMake.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[4].ToString();
            txtVModel.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[5].ToString();
            txtVModelYear.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[6].ToString();
            txtVColour.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[7].ToString();
            txtVOdometer.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[8].ToString();
            txtVPNFUnitNumber.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[9].ToString();
            cmbVPNFVehicleType.SelectedValue = Convert.ToInt32(dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[10].ToString());
            txtVFirstName.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[3].ToString();
            txtVLastName.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[2].ToString();
            txtVNumberOfParks.Text = dsOccurrence.Tables["Vehicles"].Rows[0].ItemArray[12].ToString();
            txtVPostalCode.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[8].ToString();
            txtVCity.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[6].ToString();
            txtVProvince.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[7].ToString();
            txtVAddress1.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[4].ToString();
            txtVAddress2.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[5].ToString();
            txtVEmail.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[9].ToString();
            txtVHomePhone.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[10].ToString();
            txtVWorkPhone.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[11].ToString();
            txtVCellPhone.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[12].ToString();
            txtVFax.Text = dsOccurrence.Tables["Customers"].Rows[0].ItemArray[13].ToString();

            dgvVCarMovements.DataSource = dsOccurrence.Tables["Movements"];
            dgvVCarMovements.Refresh();

            dgvVServices.DataSource = dsOccurrence.Tables["Services"]; ;
            dgvVServices.Refresh();

            dtGHistory.Clear();

            foreach (DataRow dr in dsOccurrence.Tables["History"].Rows)
            {
                dtGHistory.Rows.Add(dr.ItemArray[0], dr.ItemArray[1], dr.ItemArray[2], dr.ItemArray[3], dr.ItemArray[4], dr.ItemArray[5], 
                    dr.ItemArray[6], dr.ItemArray[7], dr.ItemArray[8]);
                dtGHistoryM.Rows.Add(dr.ItemArray[0], dr.ItemArray[1], dr.ItemArray[2], dr.ItemArray[3], dr.ItemArray[4], dr.ItemArray[5], 
                    dr.ItemArray[6], dr.ItemArray[7], dr.ItemArray[8]);
            }

            #endregion

            ShowDataTab();

            chkClosed.Visible = true;
            lblGStatus.Visible = true;
            btnGGetData.Visible = false;

            FillOutPictureList();

            strOpenCheck = "Yes";

            lstPictures.Visible = true;
            btnAddPicture.Visible = true;
            btnDeletePicture.Visible = true;
            btnSaveAs.Visible = true;

            btnCustomerCopy.Visible = true;
            btnInternalCopy.Visible = true;
            btnFinalRelease.Visible = true;
            btnFinalReleaseVoucher.Visible = true;
            btnFinalReleaseRepair.Visible = true;

            btnCustomerCopyFrench.Visible = true;
            btnFinalReleaseFrench.Visible = true;
            btnFinalReleaseVoucherFrench.Visible = true;
            btnFinalReleaseRepairFrench.Visible = true;
            btnOccurrencesOverdue.Visible = true;


            reportViewer1.Visible = true;
            chkNoTicket.Visible = false;

            #region Fill out Risk Assessment

            if (dsOccurrence.Tables["Risk"].Rows.Count > 0)
            {
                btnRAddRecord.Visible = true;
                btnRSendB.Visible = true;
                btnRSendC.Visible = true;
                lblRNotes.Visible = true;
                txtRNotes.Visible = false;
                chkRReported.Visible = true;
                chkRAssigned.Visible = true;
                chkRResolvedC.Visible = true;
                lblRSmallClaim.Visible = true;
                chkRSOC.Visible = true;
                chkRDefence.Visible = true;
                chkRPreTrial.Visible = true;
                chkRTrial.Visible = true;
                chkRJudgement.Visible = true;
                picLine1.Visible = true;
                picLine2.Visible = true;
                picLine3.Visible = true;
                picLine4.Visible = true;
                picLine5.Visible = true;
                picLine6.Visible = true;
                picLine7.Visible = true;
                picLine8.Visible = true;
                picLine9.Visible = true;
                picLine10.Visible = true;

                txtRNotes.Text = dsOccurrence.Tables["Risk"].Rows[0].ItemArray[2].ToString();

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[3].ToString() == "Y")
                {
                    chkRReported.Checked = true;
                    dtpRReportedDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[4].ToString());
                }
                else
                {
                    chkRReported.Checked = false;
                }

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[5].ToString() == "Y")
                {
                    chkRAssigned.Checked = true;
                    txtRClaim.Text = dsOccurrence.Tables["Risk"].Rows[0].ItemArray[6].ToString();
                    dtpRAssignedDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[7].ToString());
                    txtRAdjuster.Text = dsOccurrence.Tables["Risk"].Rows[0].ItemArray[8].ToString();
                    if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[9].ToString() == "Y")
                    {
                        chkRResolvedIC.Checked = true;
                        dtpRResolvedICDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[10].ToString());
                    }
                    else
                    {
                        chkRResolvedIC.Checked = false;
                    }       
                }
                else
                {
                    chkRAssigned.Checked = false;
                }

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[11].ToString() == "Y")
                {
                    chkRResolvedC.Checked = true;
                    dtpRResolvedCDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[12].ToString());
                    if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[13].ToString() == "Y")
                    {
                        chkRLetter.Checked = true;
                        dtpRLetterDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[14].ToString());
                    }
                    else
                    {
                        chkRLetter.Checked = false;
                    }       
                }
                else
                {
                    chkRResolvedC.Checked = false;
                }

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[15].ToString() == "Y")
                {
                    chkRSOC.Checked = true;
                    dtpRSOCDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[16].ToString());
                }
                else
                {
                    chkRSOC.Checked = false;
                }  
     
                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[17].ToString() == "Y")
                {
                    chkRDefence.Checked = true;
                    dtpRDefenceDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[18].ToString());
                }
                else
                {
                    chkRDefence.Checked = false;
                }  
     
                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[19].ToString() == "Y")
                {
                    chkRPreTrial.Checked = true;
                    dtpRPreTrialDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[20].ToString());
                }
                else
                {
                    chkRPreTrial.Checked = false;
                }  

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[21].ToString() == "Y")
                {
                    chkRTrial.Checked = true;
                    dtpRTrialDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[22].ToString());
                }
                else
                {
                    chkRTrial.Checked = false;
                }  

                if (dsOccurrence.Tables["Risk"].Rows[0].ItemArray[23].ToString() == "Y")
                {
                    chkRJudgement.Checked = true;
                    dtpRJudgementDate.Value = Convert.ToDateTime(dsOccurrence.Tables["Risk"].Rows[0].ItemArray[24].ToString());
                }
                else
                {
                    chkRJudgement.Checked = false;
                }  


            }


            #endregion


        }

        #region Attachments

        private void lstPictures_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strPicsPath = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text +  "\\" + lstPictures.SelectedItem.ToString();

            if (strOpenCheck == "Yes")
            {
                string ext = Path.GetExtension(strPicsPath);

                if (ext.ToUpper() == ".PDF")
                {
                    axAcroPDF1.Visible = true;
                    pictureBox1.Visible = false;
                    axAcroPDF1.Invalidate();
                    axAcroPDF1.LoadFile(strPicsPath);
                }
                else
                {
                    axAcroPDF1.Visible = false;
                    pictureBox1.Visible = true;
                    if (pictureBox1.Image != null)
                    {
                        pictureBox1.Image.Dispose();                        
                    }

                    pictureBox1.Invalidate();

                    Image im = GetCopyImage(strPicsPath);

                    //pictureBox1.Image = Image.FromFile(strPicsPath);
                    pictureBox1.Image = im;
                    pictureBox1.Refresh();
                }
            }
        }

        private void btnAddPicture_Click(object sender, EventArgs e)
        {
            //using (frmCopyPicture frmAP = new frmCopyPicture(txtOccurrenceNumber.Text))
            //{
            //    if (frmAP.ShowDialog() == DialogResult.OK)
            //    {
            //        FillOutPictureList();
            //    }
            //}

            if (chkAttachments.Checked == true)
            {
                try
                {
                    string strFolder = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\";
                    string fileName = "";

                    OpenFileDialog myDialog = new OpenFileDialog();

                    myDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG)|*.BMP;*.JPG;*.GIF;*.PDF;*JPEG|All files (*.*)|*.*";
                    myDialog.CheckFileExists = true;
                    myDialog.Multiselect = true;

                    if (Convert.ToBoolean(myDialog.ShowDialog()) == true)
                    {
                        //lstFiles.Items.Clear();

                        foreach (string file in myDialog.FileNames)
                        {
                            //lstFiles.Items.Add(file);
                            fileName = Path.GetFileName(file);
                            FileInfo f = new FileInfo(file);
                            long s1 = f.Length;
                            if (s1 > 1048576)
                            {
                                MessageBox.Show("Sorry you can add only files less than 1 Megabyte");
                            }
                            else
                            {
                                File.Copy(file, strFolder + fileName);
                            }
                        }
                    }

                    FillOutPictureList();

                    if (lstPictures.Items.Count > 0)
                    {
                        lstPictures.SelectedIndex = lstPictures.FindString(fileName);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: " + ex.Message);
                }
            }

        }

        private void btnDeletePicture_Click(object sender, EventArgs e)
        {
            string strFolder = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\";
            string strPicFilePath = strFolder + lstPictures.SelectedItem.ToString();
            string ext = Path.GetExtension(strPicFilePath);
            string strPicFilePath0 = strFolder + lstPictures.Items[0];

            string ext0 = Path.GetExtension(strPicFilePath0);

            try
            {
                int index = lstPictures.SelectedIndex;

                if (index > 0)
                {
                    index = 0;

                    if (ext0.ToUpper() == ".PDF")
                    {
                        axAcroPDF1.Visible = true;
                        pictureBox1.Visible = false;
                        axAcroPDF1.Invalidate();
                        axAcroPDF1.LoadFile(strPicFilePath0);
                    }
                    else
                    {
                        axAcroPDF1.Visible = false;
                        pictureBox1.Visible = true;
                        pictureBox1.Invalidate();
                        Image im = GetCopyImage(strPicFilePath0);
                        pictureBox1.Image = im;
                        pictureBox1.Refresh();
                    }
                }
                else
                {
                    if (lstPictures.Items.Count > 1)
                    {
                        index = index + 1;
                        strPicFilePath0 = strFolder + lstPictures.Items[index];
                        ext0 = Path.GetExtension(strPicFilePath0);

                        if (ext0.ToUpper() == ".PDF")
                        {
                            axAcroPDF1.Visible = true;
                            pictureBox1.Visible = false;
                            axAcroPDF1.Invalidate();
                            axAcroPDF1.LoadFile(strPicFilePath0);
                        }
                        else
                        {
                            axAcroPDF1.Visible = false;
                            pictureBox1.Visible = true;
                            pictureBox1.Invalidate();
                            Image im = GetCopyImage(strPicFilePath0);
                            pictureBox1.Image = im;
                            pictureBox1.Refresh();
                        }
                    }
                    else
                    {
                        if (ext.ToUpper() == ".PDF")
                        {
                            axAcroPDF1.Invalidate();
                            axAcroPDF1.LoadFile("DONTEXISTS.pdf");
                            axAcroPDF1.Refresh();
                            axAcroPDF1.Visible = false;
                        }
                        else
                        {
                            pictureBox1.Invalidate();
                            Image im = GetCopyImage(strFolder + lstPictures.Items[index]);
                            pictureBox1.Image = im;
                            pictureBox1.Image = null;
                            pictureBox1.Refresh();
                        }
                    }
                }
 
                File.Delete(strPicFilePath);
                FillOutPictureList();
                pictureBox1.Invalidate();
                axAcroPDF1.Invalidate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            File.Delete(strPicFilePath);
        }

        private void FillOutPictureList()
        {
            //if (chkAttachments.Checked == true)
            //{
                string strFolder = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\";

                lstPictures.Items.Clear();

                //string[] filePaths = System.IO.Directory.GetFiles(strFolder, "*.jpg", SearchOption.AllDirectories);
            if (chkAttachments.Checked == true)
            {

                List<String> filePaths = new List<String>();
                String[] extensions = new String[] { "*.jpg", "*.jpeg", "*.img", "*.gif", "*.bmp", "*.pdf" };

                foreach (String extension in extensions)
                {
                    String[] files = Directory.GetFiles(strFolder, extension, SearchOption.AllDirectories);

                    foreach (String file in files)
                        filePaths.Add(file);
                }

                // string[] filePaths = System.IO.Directory.GetFiles(strFolder);


                //if (filePaths.Length > 0)
                if (filePaths.Count > 0)
                {
                    foreach (string filePath in filePaths)
                    {
                        lstPictures.Items.Add(Path.GetFileName(filePath));
                    }

                    lstPictures.SelectedIndex = 0;

                    string strPicsPath = strFolder + lstPictures.Items[0];

                    string ext = Path.GetExtension(strPicsPath);

                    if (ext.ToUpper() == ".PDF")
                    {
                        axAcroPDF1.Visible = true;
                        pictureBox1.Visible = false;
                        axAcroPDF1.Invalidate();
                        axAcroPDF1.LoadFile(strPicsPath);
                    }
                    else
                    {
                        axAcroPDF1.Visible = false;
                        pictureBox1.Visible = true;
                        if (pictureBox1.Image != null)
                        {
                            pictureBox1.Image.Dispose();
                        }

                        pictureBox1.Invalidate();

                        Image im = GetCopyImage(strPicsPath);

                        //pictureBox1.Image = Image.FromFile(strPicsPath);
                        pictureBox1.Image = im;
                        pictureBox1.Refresh();
                    }
                }              
            }

        }

        private Image GetCopyImage(string path)
        {
            try
            {
                using (Image im = Image.FromFile(path))
                {
                    Bitmap bm = new Bitmap(im);
                    return bm;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
            return null;
        }

        #endregion

        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strFOccurrenceId = new StringBuilder(String.Empty);

                using (frmFindOccurrence frmFO = new frmFindOccurrence(ref strFOccurrenceId))
                {
                    if (frmFO.ShowDialog() == DialogResult.OK)
                    {
                        txtOccurrenceNumber.Text = strFOccurrenceId.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            chkAttachments.Enabled = true;
            chkAttachments.Visible = true;

            chkClosed.Visible = false;

            dtOTypes.Clear();
            dtOSubtypes.Clear();
            dtOLocations.Clear();
            dtOWeather.Clear();
            dtOPNFVTypes.Clear();
           
            daOTypes.Fill(dtOTypes);
            cmbGType.DataSource = dtOTypes;
            cmbGType.DisplayMember = "occurrence_type";
            cmbGType.ValueMember = "occurrence_type_id";

            daOSubtypes.Fill(dtOSubtypes, "1");
            cmbGSubtype.DataSource = dtOSubtypes;
            cmbGSubtype.DisplayMember = "occurrence_subtype";
            cmbGSubtype.ValueMember = "occurrence_subtype_id";

            daOLocations.Fill(dtOLocations, GlobalData.Locations);
            cmbGLocation.DataSource = dtOLocations;
            cmbGLocation.DisplayMember = "location";
            cmbGLocation.ValueMember = "location_id";

            daOWeather.Fill(dtOWeather);
            cmbGWeather.DataSource = dtOWeather;
            cmbGWeather.DisplayMember = "weather";
            cmbGWeather.ValueMember = "weather_id";

            daPNFVTypes.Fill(dtOPNFVTypes);
            cmbVPNFVehicleType.DataSource = dtOPNFVTypes;
            cmbVPNFVehicleType.DisplayMember = "pnf_vehicle_type";
            cmbVPNFVehicleType.ValueMember = "pnf_vehicle_type_id";

            dgvVServices.ColumnHeadersDefaultCellStyle.ForeColor = Color.DarkGreen;
            dgvVServices.ColumnHeadersDefaultCellStyle.BackColor = Color.PaleGreen;
            dgvVServices.Refresh();

            //strLocationId = ConfigurationManager.AppSettings["DefaultLocation"];
            //int index =  cmbGLocation.FindString("Toronto Valet"); //get index
            //cmbGLocation.SelectedValue = strLocationId;
            //cmbGLocation.SelectedIndex = index;
            strLocationId = cmbGLocation.SelectedValue.ToString();

            //***************************************************************************

            tbcOccurrence.Visible = false;
            lblGDate.Visible = false;
            lblGDateR.Visible = false;
            lblGDescription.Visible = false;
            lblGHistory.Visible = false;
            lblGLocation.Visible = false;
            lblGOpened.Visible = false;
            lblGSubtype.Visible = false;
            lblGType.Visible = false;
            lblGWeather.Visible = false;
            txtGDescription.Visible = false;
            dtpGDate.Visible = false;
            dtpGDateR.Visible = false;
            cmbGSubtype.Visible = false;
            cmbGType.Visible = false;
            cmbGLocation.Visible = false;
            cmbGWeather.Visible = false;
            grpGPolice.Visible = false;
            chkGPolice.Visible = false;
            btnCreate.Visible = true;
            btnFind.Visible = true;
            btnOpen.Visible = true;
            btnGGetData.Visible = false;
            btnGAddRecord.Visible = false;
            dgvGHistory.Visible = false;
            txtOccurrenceNumber.Visible = true;
            lblOccurrenceNumber.Visible = true;
            grpGCost.Visible = false;
            lblGEstimatedCost.Visible = false;
            lblGFinalCost.Visible = false;
            lblGVouchers.Visible = false;
            txtGEstimatedCost.Visible = false;
            txtGFinalCost.Visible = false;
            txtGVouchers.Visible = false;
            lblGReported.Visible = false;
            rdbGCheckOut.Visible = false;
            rdbGOther.Visible = false;
            rdbGTelephone.Visible = false;
            rdbGEmail.Visible = false;
            chkAssignToRM.Visible = false;

            lblGOpened.Text = "Created by " + GlobalData.UserName + " " + DateTime.Now.ToString("MMMM dd, yyyy");

            dtGHistory.Clear();
            dtGHistoryM.Clear();

            btnCancel.Visible = false;

            txtGEstimatedCost.Text = "";
            txtGFinalCost.Text = "";
            txtGVouchers.Text = "";

            dtpGDate.Value = DateTime.Today;
            dtpGDatePolice.Value = DateTime.Today;
            dtpGDateR.Value = DateTime.Today;
            

            //*****************************************************************************

            txtVAddress1.Visible = false;
            txtVAddress2.Visible = false;
            txtVBarCode.Visible = false;
            txtVCellPhone.Visible = false;
            txtVCity.Visible = false;
            txtVCloseDate.Visible = false;
            txtVColour.Visible = false;
            txtVCustomerID.Visible = false;
            txtVEmail.Visible = false;
            txtVFax.Visible = false;
            txtVFirstName.Visible = false;
            txtVHomePhone.Visible = false;
            txtVIssueNumber.Visible = false;
            txtVLastName.Visible = false;
            txtVLicensePlate.Visible = false;
            txtVMake.Visible = false;
            txtVModel.Visible = false;
            txtVModelYear.Visible = false;
            txtVOdometer.Visible = false;
            txtVOpenDate.Visible = false;
            txtVPinNumber.Visible = false;
            txtVPNFUnitNumber.Visible = false;
            cmbVPNFVehicleType.Visible = false;
            txtVPostalCode.Visible = false;
            txtVProvince.Visible = false;
            txtVTicketID.Visible = false;
            txtVTicketNumber.Visible = false;
            txtVVehicleID.Visible = false;
            txtVWorkPhone.Visible = false;
            lblVAccident.Visible = false;
            lblVAddress1.Visible = false;
            lblVAddress2.Visible = false;
            lblVBarCode.Visible = false;
            lblVCarMovements.Visible = false;
            lblVCellPhone.Visible = false;
            lblVCity.Visible = false;
            lblVColour.Visible = false;
            lblVCustomerID.Visible = false;
            lblVDateClose.Visible = false;
            lblVDateOpen.Visible = false;
            lblVEmail.Visible = false;
            lblVFault.Visible = false;
            lblVFax.Visible = false;
            lblVFirstName.Visible = false;
            lblVHomePhone.Visible = false;
            lblVIssueNumber.Visible = false;
            lblVLastName.Visible = false;
            lblVLicensePlate.Visible = false;
            lblVMake.Visible = false;
            lblVModel.Visible = false;
            lblVModelYear.Visible = false;
            lblVNumberOfParks.Visible = false;
            lblVOdometer.Visible = false;
            lblVPinNumber.Visible = false;
            lblVPNFUnitNumber.Visible = false;
            lblVPNFVehicleType.Visible = false;
            lblVPostalCode.Visible = false;
            lblVProvince.Visible = false;
            lblVServices.Visible = false;
            lblVTicketID.Visible = false;
            lblVTicketNumber.Visible = false;
            lblVVehicleID.Visible = false;
            lblVWorkPhone.Visible = false;
            grpVCustomer.Visible = false;
            grpVPNF.Visible = false;
            grpVTicket.Visible = false;
            grpVVehicle.Visible = false;
            dgvVCarMovements.Visible = false;
            dgvVServices.Visible = false;
            cmbVFault.Visible = false;
            cmbVAccident.Visible = false;
            txtVNumberOfParks.Visible = false;

            txtVAddress1.Text = "";
            txtVAddress2.Text = "";
            txtVBarCode.Text = "";
            txtVCellPhone.Text = "";
            txtVCity.Text = "";
            txtVCloseDate.Text = "";
            txtVColour.Text = "";
            txtVCustomerID.Text = "";
            txtVEmail.Text = "";
            txtVFax.Text = "";
            txtVFirstName.Text = "";
            txtVHomePhone.Text = "";
            txtVIssueNumber.Text = "";
            txtVLastName.Text = "";
            txtVLicensePlate.Text = "";
            txtVMake.Text = "";
            txtVModel.Text = "";
            txtVModelYear.Text = "";
            txtVOdometer.Text = "";
            txtVOpenDate.Text = "";
            txtVPinNumber.Text = "";
            txtVPNFUnitNumber.Text = "";
            cmbVPNFVehicleType.Text = "";
            txtVPostalCode.Text = "";
            txtVProvince.Text = "";
            txtVTicketID.Text = "";
            txtVTicketNumber.Text = "";
            txtVVehicleID.Text = "";
            txtVWorkPhone.Text = "";
            txtVNumberOfParks.Text = "";

            //***********************************************

            btnCustomerCopy.Visible = false;
            btnInternalCopy.Visible = false;
            btnFinalRelease.Visible = false;
            btnFinalReleaseVoucher.Visible = false;
            btnFinalReleaseRepair.Visible = false;

            btnCustomerCopyFrench.Visible = false;
            btnFinalReleaseFrench.Visible = false;
            btnFinalReleaseVoucherFrench.Visible = false;
            btnFinalReleaseRepairFrench.Visible = false;
            btnOccurrencesOverdue.Visible = false;


            btnAddPicture.Visible = false;
            btnDeletePicture.Visible = false;
            btnSaveAs.Visible = false;

            reportViewer1.Visible = false;
            lstPictures.Visible = false;

            //************************************************

            txtOccurrenceNumber.Text = "";
            txtOccurrenceNumber.ReadOnly = false;

            pictureBox1.Invalidate();
            pictureBox1.Image = null;
            pictureBox1.Refresh();

            txtGDescription.Text = "";
            chkClosed.Checked = false;
            chkGPolice.Checked = false;
            chkAssignToRM.Checked = false;
            chkNoTicket.Checked = false;
            txtGEstimatedCost.Text = "";
            txtGFinalCost.Text = "";
            txtGVouchers.Text = "";
            txtGBadge.Text = "";
            chkNoTicket.Visible = false;
            btnSave.Visible = false;
            cmbVFault.SelectedIndex = 0;
            cmbVAccident.SelectedIndex = 0;

            tbcOccurrence.SelectTab(0);

            lblGClosed.Visible = false;
            lblGStatus.Visible = false;

            reportViewer1.Clear();

        }

        private void chkNoTicket_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoTicket.Checked)
            {
                btnGGetData.Visible = false;

                txtVTicketID.Text = "0";
                txtVTicketNumber.Text = "";
                txtVIssueNumber.Text = "";
                txtVPinNumber.Text = "";
                txtVBarCode.Text = "";
                txtVOpenDate.Text = "";
                txtVCloseDate.Text = "";
                txtVVehicleID.Text = "0";
                txtVCustomerID.Text = "0";
                txtVLicensePlate.Text = "Unknown";
                txtVMake.Text = "";
                txtVColour.Text = "";
                txtVFirstName.Text = "Unknown";
                txtVLastName.Text = "Unknown";
                txtVNumberOfParks.Text = "0";
                txtVPostalCode.Text = "";
                txtVCity.Text = "";
                txtVProvince.Text = "";
                txtVAddress1.Text = "";
                txtVAddress2.Text = "";
                txtVEmail.Text = "";
                txtVHomePhone.Text = "";
                txtVWorkPhone.Text = "";
                txtVCellPhone.Text = "";
                txtVFax.Text = "";

                daVParks.Fill(dtVParks, Convert.ToInt64("0"));
                dgvVCarMovements.DataSource = dtVParks;
                dgvVCarMovements.Refresh();

                daGServices.Fill(dtGServices, Convert.ToInt64("0"));
                dgvVServices.DataSource = dtGServices;
                dgvVServices.Refresh();

                tbcOccurrence.SelectedTab = tabVehicle;

                ShowDataTab();

                btnSave.Visible = true;

                cmbVFault.SelectedIndex = 0;
                cmbVAccident.SelectedIndex = 0;
            }
            else
            {
                btnGGetData.Visible = true;
            }
        }

        #region Reports Utilities

        private static string FinalCostString(string strinput)
        {
            string stroutput = "";

            char[] delimiterChars = { '.' };

            if (strinput.Length > 0)
            {
                string[] words = strinput.Split(delimiterChars);

                Int64 intNumber2 = 0;

                Int64 intNumber1 = Convert.ToInt64(words[0]);

                if (strinput.IndexOf(".", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    intNumber2 = Convert.ToInt64(words[1]);
                }

                stroutput = FirstCharToUpper(NumberToWords(intNumber1) + " dollars and " + NumberToWords(intNumber2) + " cents");
            }
            else
            {
                stroutput = "";
            }

            return stroutput;
        }

        private static string FinalCostStringFrench(string strinput)
        {
            string stroutput = "";

            char[] delimiterChars = { '.' };

            if (strinput.Length > 0)
            {
                string[] words = strinput.Split(delimiterChars);

                Int32 intNumber2 = 0;

                Int32 intNumber1 = Convert.ToInt32(words[0]);

                if (strinput.IndexOf(".", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    intNumber2 = Convert.ToInt32(words[1]);
                }

                stroutput = FirstCharToUpper(NumberToWordsFrench(intNumber1) + " et " + NumberToWordsFrench(intNumber2) );
            }
            else
            {
                stroutput = "";
            }

            return stroutput;
        }

        private static string VoucherString(string strinput)
        {
            string stroutput = "";

            if (strinput.Length > 0)
            {
                Int64 intNumber1 = Convert.ToInt64(strinput);

                stroutput = NumberToWords(intNumber1);
            }
            else
            {
                stroutput = "";
            }

            return stroutput;
        }

        private static string VoucherStringFrench(string strinput)
        {
            string stroutput = "";

            if (strinput.Length > 0)
            {
                Int32 intNumber1 = Convert.ToInt32(strinput);

                stroutput = NumberToWordsFrench(intNumber1);
            }
            else
            {
                stroutput = "";
            }

            return stroutput;
        }

        public static string NumberToWords(Int64 number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

     //*******************************************************************************************************************************
        public static string NumberToWordsFrench(int chiffre)
        {
            int centaine, dizaine, unite, reste, y;
            bool dix = false;
            string lettre = "";
            //strcpy(lettre, "");

            reste = chiffre / 1;

            for (int i = 1000000000; i >= 1; i /= 1000)
            {
                y = reste / i;
                if (y != 0)
                {
                    centaine = y / 100;
                    dizaine = (y - centaine * 100) / 10;
                    unite = y - (centaine * 100) - (dizaine * 10);
                    switch (centaine)
                    {
                        case 0:
                            break;
                        case 1:
                            lettre += "cent ";
                            break;
                        case 2:
                            if ((dizaine == 0) && (unite == 0)) lettre += "deux cents ";
                            else lettre += "deux cent ";
                            break;
                        case 3:
                            if ((dizaine == 0) && (unite == 0)) lettre += "trois cents ";
                            else lettre += "trois cent ";
                            break;
                        case 4:
                            if ((dizaine == 0) && (unite == 0)) lettre += "quatre cents ";
                            else lettre += "quatre cent ";
                            break;
                        case 5:
                            if ((dizaine == 0) && (unite == 0)) lettre += "cinq cents ";
                            else lettre += "cinq cent ";
                            break;
                        case 6:
                            if ((dizaine == 0) && (unite == 0)) lettre += "six cents ";
                            else lettre += "six cent ";
                            break;
                        case 7:
                            if ((dizaine == 0) && (unite == 0)) lettre += "sept cents ";
                            else lettre += "sept cent ";
                            break;
                        case 8:
                            if ((dizaine == 0) && (unite == 0)) lettre += "huit cents ";
                            else lettre += "huit cent ";
                            break;
                        case 9:
                            if ((dizaine == 0) && (unite == 0)) lettre += "neuf cents ";
                            else lettre += "neuf cent ";
                            break;

                    }// endSwitch(centaine)

                    switch (dizaine)
                    {
                        case 0:
                            break;
                        case 1:
                            dix = true;
                            break;
                        case 2:
                            lettre += "vingt ";
                            break;
                        case 3:
                            lettre += "trente ";
                            break;
                        case 4:
                            lettre += "quarante ";
                            break;
                        case 5:
                            lettre += "cinquante ";
                            break;
                        case 6:
                            lettre += "soixante ";
                            break;
                        case 7:
                            dix = true;
                            lettre += "soixante ";
                            break;
                        case 8:
                            lettre += "quatre-vingt ";
                            break;
                        case 9:
                            dix = true;
                            lettre += "quatre-vingt ";
                            break;

                    } // endSwitch(dizaine)

                    switch (unite)
                    {
                        case 0:
                            if (dix) lettre += "dix ";
                            break;
                        case 1:
                            if (dix) lettre += "onze ";
                            else lettre += "un ";
                            break;
                        case 2:
                            if (dix) lettre += "douze ";
                            else lettre += "deux ";
                            break;
                        case 3:
                            if (dix) lettre += "treize ";
                            else lettre += "trois ";
                            break;
                        case 4:
                            if (dix) lettre += "quatorze ";
                            else lettre += "quatre ";
                            break;
                        case 5:
                            if (dix) lettre += "quinze ";
                            else lettre += "cinq ";
                            break;
                        case 6:
                            if (dix) lettre += "seize ";
                            else lettre += "six ";
                            break;
                        case 7:
                            if (dix) lettre += "dix-sept ";
                            else lettre += "sept ";
                            break;
                        case 8:
                            if (dix) lettre += "dix-huit ";
                            else lettre += "huit ";
                            break;
                        case 9:
                            if (dix) lettre += "dix-neuf ";
                            else lettre += "neuf ";
                            break;
                    } // endSwitch(unite)

                    switch (i)
                    {
                        case 1000000000:
                            if (y > 1) lettre += "milliards ";
                            else lettre += "milliard ";
                            break;
                        case 1000000:
                            if (y > 1) lettre += "millions ";
                            else lettre += "million ";
                            break;
                        case 1000:
                            lettre += "mille ";
                            break;
                    }
                } // end if(y!=0)
                reste -= y * i;
                dix = false;
            } // end for
            if (lettre.Length == 0) lettre += "zero";

            return lettre;
        }

    //********************************************************************************************************************************

        public static string FirstCharToUpper(string input)
        {
            string strout = "";

            if (!String.IsNullOrEmpty(input))
            {
                strout = input.Substring(0,1).ToUpper() + input.Substring(1);
            }

            return strout;
        }

        #endregion

        #region Risk Checkboxes

        private void chkAssignToRM_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAssignToRM.Checked)
            {
                btnRAddRecord.Visible = true;
                btnRSendB.Visible = true;
                btnRSendC.Visible = true;
                lblRNotes.Visible = true;
                txtRNotes.Visible = false;
                chkRReported.Visible = true;
                chkRAssigned.Visible = true;
                chkRResolvedC.Visible = true;
                lblRSmallClaim.Visible = true;
                chkRSOC.Visible = true;
                chkRDefence.Visible = true;
                chkRPreTrial.Visible = true;
                chkRTrial.Visible = true;
                chkRJudgement.Visible = true;
                picLine1.Visible = true;
                picLine2.Visible = true;
                picLine3.Visible = true;
                picLine4.Visible = true;
                picLine5.Visible = true;
                picLine6.Visible = true;
                picLine7.Visible = true;
                picLine8.Visible = true;
                picLine9.Visible = true;
                picLine10.Visible = true;               
            }
            else
            {
                btnRAddRecord.Visible = false;
                btnRSendB.Visible = false;
                btnRSendC.Visible = false;
                lblRNotes.Visible = false;
                txtRNotes.Visible = false;
                chkRReported.Visible = false;
                chkRAssigned.Visible = false;
                chkRResolvedC.Visible = false;
                lblRSmallClaim.Visible = false;
                chkRSOC.Visible = false;
                chkRDefence.Visible = false;
                chkRPreTrial.Visible = false;
                chkRTrial.Visible = false;
                chkRJudgement.Visible = false;
                picLine1.Visible = false;
                picLine2.Visible = false;
                picLine3.Visible = false;
                picLine4.Visible = false;
                picLine5.Visible = false;
                picLine6.Visible = false;
                picLine7.Visible = false;
                picLine8.Visible = false;
                picLine9.Visible = false;
                picLine10.Visible = false;        
            }
        }

        private void chkRReported_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRReported.Checked)
            {
                lblRReportedDate.Visible = true;
                dtpRReportedDate.Visible = true;
            }
            else
            {
                lblRReportedDate.Visible = false;
                dtpRReportedDate.Visible = false;
            }
        }

        private void chkRAssigned_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRAssigned.Checked)
            {
                lblRClaim.Visible = true;
                txtRClaim.Visible = true;
                lblRAssignedDate.Visible = true;
                dtpRAssignedDate.Visible = true;
                lblRAdjuster.Visible = true;
                txtRAdjuster.Visible = true;
                chkRResolvedIC.Visible = true;
            }
            else
            {
                lblRClaim.Visible = false;
                txtRClaim.Visible = false;
                lblRAssignedDate.Visible = false;
                dtpRAssignedDate.Visible = false;
                lblRAdjuster.Visible = false;
                txtRAdjuster.Visible = false;
                chkRResolvedIC.Visible = false;
                lblRResolvedICDate.Visible = false;
                dtpRResolvedICDate.Visible = false;
                chkRResolvedIC.Checked = false;
            }
        }

        private void chkRResolvedIC_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRResolvedIC.Checked)
            {
                lblRResolvedICDate.Visible = true;
                dtpRResolvedICDate.Visible = true;
            }
            else
            {
                lblRResolvedICDate.Visible = false;
                dtpRResolvedICDate.Visible = false;
            }
        }

        private void chkRResolvedC_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRResolvedC.Checked)
            {
                lblRResolvedCDate.Visible = true;
                dtpRResolvedCDate.Visible = true;
                chkRLetter.Visible = true;
            }
            else
            {
                lblRResolvedCDate.Visible = false;
                dtpRResolvedCDate.Visible = false;
                chkRLetter.Visible = false;
                lblRLetterDate.Visible = false;
                dtpRLetterDate.Visible = false;
                chkRLetter.Checked = false;
            }
        }

        private void chkRLetter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRLetter.Checked)
            {
                lblRLetterDate.Visible = true;
                dtpRLetterDate.Visible = true;
            }
            else
            {
                lblRLetterDate.Visible = false;
                dtpRLetterDate.Visible = false;
            }
        }

        private void chkRSOC_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRSOC.Checked)
            {
                lblRSOCDate.Visible = true;
                dtpRSOCDate.Visible = true;
            }
            else
            {
                lblRSOCDate.Visible = false;
                dtpRSOCDate.Visible = false;
            }
        }

        private void chkRDefence_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRDefence.Checked)
            {
                lblRDefenceDate.Visible = true;
                dtpRDefenceDate.Visible = true;
            }
            else
            {
                lblRDefenceDate.Visible = false;
                dtpRDefenceDate.Visible = false;
            }
        }

        private void chkRPreTrial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRPreTrial.Checked)
            {
                lblRPreTrialDate.Visible = true;
                dtpRPreTrialDate.Visible = true;
            }
            else
            {
                lblRPreTrialDate.Visible = false;
                dtpRPreTrialDate.Visible = false;
            }
        }

        private void chkRTrial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRTrial.Checked)
            {
                lblRTrialDate.Visible = true;
                dtpRTrialDate.Visible = true;
            }
            else
            {
                lblRTrialDate.Visible = false;
                dtpRTrialDate.Visible = false;
            }
        }

        private void chkRJudgement_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRJudgement.Checked)
            {
                lblRJudgementDate.Visible = true;
                dtpRJudgementDate.Visible = true;
            }
            else
            {
                lblRJudgementDate.Visible = false;
                dtpRJudgementDate.Visible = false;
            }
        }

        private void SendMail(string strOccurrenceNumberIn)
        {
            //string to = ConfigurationManager.AppSettings["RiskAssignmentMail"];
            //string from = "occurrence@parknfly.ca";
            //MailMessage message = new MailMessage(from, to);
            //message.Subject = "Occurrence Management";
            //message.Body = @"Occurrence # " + strOccurrenceNumberIn + " was assigned to Risk Management or is Personal injury type.";
            ////SmtpClient client = new SmtpClient("mail.exchange.telus.com");
            //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTPServer"]);
            ////SmtpClient client = new SmtpClient("parknfly-ca.mail.protection.outlook.com");

            //// Credentials are necessary if the server requires the client  
            //// to authenticate before it will send e-mail on the client's behalf.
            //client.EnableSsl = true;
            //// The server requires user's credentials
            //// not the default credentials
            //client.UseDefaultCredentials = false;
            //// Provide your credentials
            //client.Credentials = new System.Net.NetworkCredential("occurrence@parknfly.ca", "mngmnt1210");
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;

            ////client.UseDefaultCredentials = true;

            try
            {
                string Subject = "Occurrence Management";
                string Body = @"Occurrence # " + strOccurrenceNumberIn + " was assigned to Risk Management or is Personal injury type.";

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.PARKERConnectionString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("msdb.dbo.sp_send_dbmail", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@recipients", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["RiskAssignmentMail"];
                        cmd.Parameters.Add("@profile_name", SqlDbType.VarChar).Value = "Occurrence Management";
                        cmd.Parameters.Add("@from_address", SqlDbType.VarChar).Value = "occurrence@parknfly.ca";
                        cmd.Parameters.Add("@subject", SqlDbType.VarChar).Value = Subject;
                        cmd.Parameters.Add("@body", SqlDbType.VarChar).Value = Body;

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                //client.Send(message);
                MessageBox.Show("Risk Management Assignment Mail was sent");
            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("Exception caught in sending mail: {0}", ex.ToString());

            }              
        }

        #endregion

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            //Stream myStream;
            try
            {

                if (lstPictures.SelectedItem != null)
                {
                    string fileName = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\" + lstPictures.SelectedItem.ToString();
                    string ext = Path.GetExtension(fileName);

                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = ext.Substring(1, ext.Length - 1) + " files (*" + ext + ")|*" + ext + "|All files (*.*)|*.*";

                    //saveFileDialog1.FilterIndex = 2;
                    //saveFileDialog1.RestoreDirectory = true;
                    //saveFileDialog1.FileName = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\" + lstPictures.SelectedItem.ToString(); ;

                    //Stream s = File.Open(fileName, FileMode.Open);



                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        using (FileStream source = File.Open(fileName, FileMode.Open))
                        using (Stream s = File.Open(saveFileDialog1.FileName, FileMode.CreateNew))
                        {
                            source.CopyTo(s);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("There is no selected item");
                }
            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("Exception: {0}", ex.ToString());
            }              


            //OpenFileDialog od = new OpenFileDialog();
            ////od.Filter = "XLS files|*.xls";
            ////od.Multiselect = true;
            //string fileName = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\" + lstPictures.SelectedItem.ToString();

            //if (od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    string tempFolder = System.IO.Path.GetTempPath();

            //    //foreach (string fileName in od.FileNames)
            //    //{
            //        System.IO.File.Copy(fileName, tempFolder + @"\" + System.IO.Path.GetFileName(fileName));
            //    //}
            //}

            //string folderpath = "";

            //FolderBrowserDialog fbd=new FolderBrowserDialog();

            //DialogResult dr=fbd.ShowDialog();

            //if (dr == DialogResult.OK)
            //{
            //     folderpath=fbd.SelectedPath;
            //}

            //if (folderpath != "")
            //{

            //}

            //OpenFileDialog od = new OpenFileDialog();
            //SaveFileDialog od = new SaveFileDialog();
            //od.Filter = "XLS files|*.xls";
            //od.Multiselect = true;
            //if (od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    string tempFolder = System.IO.Path.GetTempPath();

            //    //foreach (string fileName in od.FileNames)
            //    //{
            //    System.IO.File.Copy(ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + txtOccurrenceNumber.Text + "\\" + lstPictures.SelectedItem.ToString(), tempFolder + @"\" + System.IO.Path.GetFileName(lstPictures.SelectedItem.ToString()));
            //    //}
            //}



        }

        private void btnRAddRecord_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strNotes1 = new StringBuilder(String.Empty);
                string strNotes = "";
                DateTime actionDate = Convert.ToDateTime("2000-01-01");
                int action_id = 0;
                string strAction = "";

                using (frmAddRecord frmAR = new frmAddRecord(ref strNotes1))
                {
                    if (frmAR.ShowDialog() == DialogResult.OK)
                    {
                        strNotes = strNotes1.ToString();
                        actionDate = frmAR.actionDate;
                        action_id = frmAR.action_id;
                        strAction = frmAR.strAction;

                        Int64 maxID = 1;

                        if (dtGHistory.Rows.Count > 0)
                        {
                            //maxID = (Int32)dtGHistory.Compute("Max(act_id)", "");
                            maxID = (Int32)dtGHistory.Rows.Count;
                            maxID = maxID + 1;
                        }

                        dtGHistory.Rows.Add(maxID, 0, GlobalData.UserLogin, GlobalData.UserName, DateTime.Now, action_id, strAction, strNotes, actionDate);
                        dgvGHistory.Refresh();

                    }
                }



            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnRSendB_Click(object sender, EventArgs e)
        {

            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;



                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptIntCopy.rdlc";
                localReport.ReportPath = @"rptIntCopy.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.LocalReport.SubreportProcessing +=
                new SubreportProcessingEventHandler(DemoSubreportProcessingEventHandler);

                reportViewer1.RefreshReport();

                //***************************************************************************
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = reportViewer1.LocalReport.Render("PDF", null, out mimeType,
                        out encoding, out extension, out streamids, out warnings);

                string file = @"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf";
                if (Directory.Exists(Path.GetDirectoryName(file)))
                {
                    File.Delete(file);
                }

                using (FileStream fs = new FileStream(@"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf", FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                //string strTo = ConfigurationManager.AppSettings["InsuranceBrokerMail"];
                //string strFrom = "occurrence@parknfly.ca";
                //MailAddress maTo = new MailAddress(strTo);
                //MailAddress maFrom = new MailAddress(strFrom);
                //MailMessage message = new MailMessage(strFrom, strTo);
                //string to2 = ConfigurationManager.AppSettings["RiskAssignmentMail"];
                //MailAddress ma2 = new MailAddress(to2);
                //message.To.Add(ma2);
                //message.Subject = ConfigurationManager.AppSettings["InsuranceBrokerMailSubject"];
                string strL = cmbGLocation.Text;
                string firstWord = strL.IndexOf(" ") > -1
                  ? strL.Substring(0, strL.IndexOf(" "))
                  : strL;
                string Subject = firstWord + " Occurrence #" + txtOccurrenceNumber.Text;
                string Body = "Please find attached a copy of our internal occurrence report regarding the above captioned matter. " +
                    "Additional documents will follow under separate cover. Please acknowledge receipt. We look forward to your advise.";

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.PARKERConnectionString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("msdb.dbo.sp_send_dbmail", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@recipients", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["InsuranceBrokerMail"]; 
                        cmd.Parameters.Add("@profile_name", SqlDbType.VarChar).Value = "Occurrence Management";
                        cmd.Parameters.Add("@from_address", SqlDbType.VarChar).Value = "occurrence@parknfly.ca";
                        cmd.Parameters.Add("@subject", SqlDbType.VarChar).Value = Subject;
                        cmd.Parameters.Add("@body", SqlDbType.VarChar).Value = Body;
                        cmd.Parameters.Add("@file_attachments", SqlDbType.VarChar).Value = @"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf";

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                MessageBox.Show("Mail to Insurance Broker was sent");


                //MemoryStream s = new MemoryStream(bytes);
                //s.Seek(0, SeekOrigin.Begin);

                //Attachment a = new Attachment(s, "PNFReport.pdf");

                //string strTo = ConfigurationManager.AppSettings["InsuranceBrokerMail"];
                //string strFrom = "occurrence@parknfly.ca";
                ////MailAddress maTo = new MailAddress(strTo);
                ////MailAddress maFrom = new MailAddress(strFrom);
                ////MailMessage message = new MailMessage(strFrom, strTo);
                //string to2 = ConfigurationManager.AppSettings["RiskAssignmentMail"];
                ////MailAddress ma2 = new MailAddress(to2);
                ////message.To.Add(ma2);
                ////message.Subject = ConfigurationManager.AppSettings["InsuranceBrokerMailSubject"];
                //string strL = cmbGLocation.Text;
                //string firstWord = strL.IndexOf(" ") > -1
                //  ? strL.Substring(0, strL.IndexOf(" "))
                //  : strL;
                //string Subject = firstWord + " Occurrence #" + txtOccurrenceNumber.Text;
                //string Body = "Please find attached a copy of our internal occurrence report regarding the above captioned matter. " + 
                //    "Additional documents will follow under separate cover. Please acknowledge receipt. We look forward to your advise.";
                //message.Attachments.Add(a);

                //MailMessage message1 = new MailMessage(strFrom, strTo);
                //message1.Subject = Subject;
                //message1.Body = Body;
                //message1.Attachments.Add(a);

                //MailMessage message2 = new MailMessage(strFrom, to2);
                //message2.Subject = Subject;
                //message2.Body = Body;
                //message2.Attachments.Add(a);

                //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTPServer"]);

                //client.EnableSsl = true;
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential("occurrence@parknfly.ca", "mngmnt1210");
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;


                //try
                //{
                //    client.Send(message1);
                //    MessageBox.Show("Mail to Insurance Broker was sent");

                //    client.Send(message2);
                //    MessageBox.Show("Copy to RiskAssignment was sent");

                //}
                //catch (Exception ex)
                //{
                //    //MessageBox.Show("Exception caught in sending mail: {0}", ex.ToString());
                //    MessageBox.Show("Message - " + ex.ToString());
                //}

                //***************************************************************************



            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }

        private void btnRSendC_Click(object sender, EventArgs e)
        {
            try
            {
                reportViewer1.ProcessingMode = ProcessingMode.Local;

                LocalReport localReport = reportViewer1.LocalReport;



                //localReport.ReportPath = @"C:\Users\ateterin\Documents\Visual Studio 2012\Projects\OccurrenceManagement\OccurrenceManagement\rptIntCopy.rdlc";
                localReport.ReportPath = @"rptIntCopy.rdlc";

                dtRepOccurrenceData.Clear();
                daRepOccurrenceData.Fill(dtRepOccurrenceData, Convert.ToInt64(txtOccurrenceNumber.Text));

                ReportDataSource rdsOccurrenceData = new ReportDataSource();
                rdsOccurrenceData.Value = dtRepOccurrenceData;
                rdsOccurrenceData.Name = "dtRepOccurrenceData";

                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(rdsOccurrenceData);

                reportViewer1.LocalReport.SubreportProcessing +=
                new SubreportProcessingEventHandler(DemoSubreportProcessingEventHandler);

                reportViewer1.RefreshReport();

                //***************************************************************************
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = reportViewer1.LocalReport.Render("PDF", null, out mimeType,
                        out encoding, out extension, out streamids, out warnings);

                string file = @"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf";
                if (Directory.Exists(Path.GetDirectoryName(file)))
                {
                    File.Delete(file);
                }

                using (FileStream fs = new FileStream(@"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf", FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                //string strTo = ConfigurationManager.AppSettings["InsuranceBrokerMail"];
                //string strFrom = "occurrence@parknfly.ca";
                //MailAddress maTo = new MailAddress(strTo);
                //MailAddress maFrom = new MailAddress(strFrom);
                //MailMessage message = new MailMessage(strFrom, strTo);
                //string to2 = ConfigurationManager.AppSettings["RiskAssignmentMail"];
                //MailAddress ma2 = new MailAddress(to2);
                //message.To.Add(ma2);
                //message.Subject = ConfigurationManager.AppSettings["InsuranceBrokerMailSubject"];
                string strL = cmbGLocation.Text;
                string firstWord = strL.IndexOf(" ") > -1
                  ? strL.Substring(0, strL.IndexOf(" "))
                  : strL;
                string Subject = firstWord + " Occurrence #" + txtOccurrenceNumber.Text;
                string Body = "Please find attached a copy of our internal occurrence report regarding the above captioned matter. " +
                    "Additional documents will follow under separate cover. Please advise when this has been assigned to an adjuster " +
                    "and have the adjuster contact us for any questions or to arrange any meetings with our staff if needed.";

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["OccurrenceManagement.Properties.Settings.PARKERConnectionString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("msdb.dbo.sp_send_dbmail", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@recipients", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["InsuranceCompanyMail"];
                        cmd.Parameters.Add("@profile_name", SqlDbType.VarChar).Value = "Occurrence Management";
                        cmd.Parameters.Add("@from_address", SqlDbType.VarChar).Value = "occurrence@parknfly.ca";
                        cmd.Parameters.Add("@subject", SqlDbType.VarChar).Value = Subject;
                        cmd.Parameters.Add("@body", SqlDbType.VarChar).Value = Body;
                        cmd.Parameters.Add("@file_attachments", SqlDbType.VarChar).Value = @"\\pnfsqlcluster\OccurrenceMail\PNFReport.pdf";

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }

                MessageBox.Show("Mail to Insurance Company was sent");


                //MemoryStream s = new MemoryStream(bytes);
                //s.Seek(0, SeekOrigin.Begin);

                //Attachment a = new Attachment(s, "PNFReport.pdf");

                //string to = ConfigurationManager.AppSettings["InsuranceCompanyMail1"];
                //string from = "occurrence@parknfly.ca";

                ////MailMessage message = new MailMessage(from, to);
                //string to2 = ConfigurationManager.AppSettings["InsuranceCompanyMail2"];
                ////message.To.Add(to2);
                //string to3 = ConfigurationManager.AppSettings["RiskAssignmentMail"];
                ////message.To.Add(to3);
                //string strL = cmbGLocation.Text;
                //string firstWord = strL.IndexOf(" ") > -1
                //  ? strL.Substring(0, strL.IndexOf(" "))
                //  : strL;
                //string Subject = firstWord + " Occurrence #" + txtOccurrenceNumber.Text;
                //string Body = "Please find attached a copy of our internal occurrence report regarding the above captioned matter. " +
                //    "Additional documents will follow under separate cover. Please advise when this has been assigned to an adjuster " +
                //    "and have the adjuster contact us for any questions or to arrange any meetings with our staff if needed.";
                ////message.Attachments.Add(a);

                //MailMessage message1 = new MailMessage(from, to);
                //message1.Subject = Subject;
                //message1.Body = Body;
                //message1.Attachments.Add(a);

                //MailMessage message2 = new MailMessage(from, to2);
                //message2.Subject = Subject;
                //message2.Body = Body;
                //message2.Attachments.Add(a);

                //MailMessage message3 = new MailMessage(from, to3);
                //message3.Subject = Subject;
                //message3.Body = Body;
                //message3.Attachments.Add(a);


                //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailSMTPServer"]);

                //client.EnableSsl = true;
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential("occurrence@parknfly.ca", "mngmnt1210");
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;

                //try
                //{
                //    client.Send(message1);
                //    MessageBox.Show("First mail to Insurance Company was sent");

                //    client.Send(message2);
                //    MessageBox.Show("Second mail to Insurance Company was sent");

                //    client.Send(message3);
                //    MessageBox.Show("Copy to RiskAssignment was sent");

                //}
                //catch (Exception ex)
                //{
                //    //MessageBox.Show("Exception caught in sending mail: {0}", ex.ToString());
                //    MessageBox.Show("Message - " + ex.ToString());
                //}

                //***************************************************************************



            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
                MessageBox.Show("Report can NOT be generated. ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void LogMessage(string strMessage)
        {
            // stream writer to write in the log file
            StreamWriter objWriter = null;

            // get the current date and time
            DateTime dtNow = DateTime.Now;

            // get the time stamp
            String strTimeStamp = dtNow.ToString("yyyy-MM-dd HH:mm:ss.FFFF");

            StringBuilder strLoggedMessage = new StringBuilder();

            // construct the message to be logged
            // append the date and time stamp
            strLoggedMessage.Append(strTimeStamp);
            // append a blank space
            strLoggedMessage.Append(" ");
            // append the message
            strLoggedMessage.Append(strMessage);

            try
            {
                // enter the monitor to synchronize access to the file
                Monitor.Enter(this.strFileLock_);

                // create a file stream
                // create a file stream
                // PY : Oct 01.10 - changed the folder to log to
                FileStream objLogFile = new FileStream(string.Format(".\\Logs\\OccurMgr{0}.log", dtNow.ToString("yyyy_MM_dd")),
                                                       FileMode.OpenOrCreate,
                                                       FileAccess.Write);

                // move at the end of it
                objLogFile.Seek(0, SeekOrigin.End);

                // create a stream writer
                objWriter = new StreamWriter(objLogFile);

                // write the message that must be logged
                objWriter.WriteLine(strLoggedMessage.ToString());

                // flush the buffer
                objWriter.Flush();
            }
            catch (Exception ex)
            {
                this.LogMessage(string.Format("ERROR: {0}", ex.ToString()));
            }
            finally
            {
                // close the file if opened
                if (objWriter != null) objWriter.Close();

                // exit the monitor to allow other threads to access the log file
                Monitor.Exit(this.strFileLock_);
            }
        }

    }
}
