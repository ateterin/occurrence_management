﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OccurrenceManagement
{
    public partial class frmFindOccurrence : Form
    {

        dsLocation.dtLocationsDataTable dtFLocations = new dsLocation.dtLocationsDataTable();
        dsLocationTableAdapters.dtLocationsTableAdapter daFLocations = new dsLocationTableAdapters.dtLocationsTableAdapter();

        dsFindOccurrence.dtFindOccurrenceDataTable dtFOccurrences = new dsFindOccurrence.dtFindOccurrenceDataTable();
        dsFindOccurrenceTableAdapters.dtFindOccurrenceTableAdapter daFOccurrences = new dsFindOccurrenceTableAdapters.dtFindOccurrenceTableAdapter();

        private StringBuilder strOccurrenceId_;
        int statusData = 0;
        string strLocationId = "";


        public frmFindOccurrence(ref StringBuilder strOccurrenceId)
        {
            strOccurrenceId_ = strOccurrenceId;
            InitializeComponent();
        }

        private void frmFindOccurrence_Load(object sender, EventArgs e)
        {
            //this.Font = new Font(this.Font.FontFamily, this.Font.SizeInPoints * 125 / 96);
            //base.OnLoad(e);

            try
            {
                daFLocations.Fill(dtFLocations, GlobalData.Locations);
                cmbFLocation.DataSource = dtFLocations;
                cmbFLocation.DisplayMember = "location";
                cmbFLocation.ValueMember = "location_id";

                statusData = 0;
                btnFind.Text = "Find";
                //strLocationId = "100";
                strLocationId = cmbFLocation.SelectedValue.ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                if (statusData == 0)
                {
                    statusData = statusData + 1;
                    btnFind.Text = "Select";
                    daFOccurrences.Fill(dtFOccurrences, Convert.ToInt32(strLocationId) ,dtpDateCreated.Value, txtLastName.Text, txtLicenseNumber.Text );
                    dgvOccurrences.DataSource = dtFOccurrences;
                    dgvOccurrences.Refresh();
                    dgvOccurrences.Columns[1].DefaultCellStyle.Format = "MMM dd, yyyy";
                    dgvOccurrences.Columns[2].Width = 150;
                    dgvOccurrences.Columns[1].Width = 120;
                }
                else
                {
                    if (dgvOccurrences.Rows.Count == 0)
                    {
                        statusData = 0;
                        btnFind.Text = "Find";
                        MessageBox.Show("No data was selected");
                    }
                    else
                    {
                        if (dgvOccurrences.SelectedCells[0].Value == null)
                        {
                            statusData = 0;
                            btnFind.Text = "Find";
                            MessageBox.Show("No data was selected");
                        }
                        else
                        {
                            strOccurrenceId_.Append(dgvOccurrences.SelectedCells[0].Value);
                            this.DialogResult = DialogResult.OK;
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1); ;
            }

           
        }

        private void cmbFLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            strLocationId = cmbFLocation.SelectedValue.ToString();
            btnFind.Text = "Find";
            statusData = 0;
            dgvOccurrences.DataSource = null;
            dgvOccurrences.Refresh();
           
        }

        private void dtpDateCreated_ValueChanged(object sender, EventArgs e)
        {
            btnFind.Text = "Find";
            statusData = 0;
            dgvOccurrences.DataSource = null;
            dgvOccurrences.Refresh();

        }

        private void txtLastName_TextChanged(object sender, EventArgs e)
        {
            btnFind.Text = "Find";
            statusData = 0;
            dgvOccurrences.DataSource = null;
            dgvOccurrences.Refresh();

        }

        private void txtLicenseNumber_TextChanged(object sender, EventArgs e)
        {
            btnFind.Text = "Find";
            statusData = 0;
            dgvOccurrences.DataSource = null;
            dgvOccurrences.Refresh();
        }


    }
}
