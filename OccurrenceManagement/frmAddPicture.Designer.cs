﻿namespace OccurrenceManagement
{
    partial class frmAddPicture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPictureName = new System.Windows.Forms.Label();
            this.txtPictureName = new System.Windows.Forms.TextBox();
            this.lstDrives = new System.Windows.Forms.ListBox();
            this.lstFolders = new System.Windows.Forms.ListBox();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.lblDrives = new System.Windows.Forms.Label();
            this.lblFolders = new System.Windows.Forms.Label();
            this.lblFiles = new System.Windows.Forms.Label();
            this.btnAddPicture = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPictureName
            // 
            this.lblPictureName.AutoSize = true;
            this.lblPictureName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPictureName.Location = new System.Drawing.Point(30, 32);
            this.lblPictureName.Name = "lblPictureName";
            this.lblPictureName.Size = new System.Drawing.Size(103, 18);
            this.lblPictureName.TabIndex = 0;
            this.lblPictureName.Text = "Picture Name";
            // 
            // txtPictureName
            // 
            this.txtPictureName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPictureName.Location = new System.Drawing.Point(151, 29);
            this.txtPictureName.Name = "txtPictureName";
            this.txtPictureName.Size = new System.Drawing.Size(372, 26);
            this.txtPictureName.TabIndex = 1;
            // 
            // lstDrives
            // 
            this.lstDrives.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDrives.FormattingEnabled = true;
            this.lstDrives.ItemHeight = 16;
            this.lstDrives.Location = new System.Drawing.Point(33, 94);
            this.lstDrives.Name = "lstDrives";
            this.lstDrives.Size = new System.Drawing.Size(157, 180);
            this.lstDrives.TabIndex = 2;
            this.lstDrives.SelectedIndexChanged += new System.EventHandler(this.lstDrives_SelectedIndexChanged);
            // 
            // lstFolders
            // 
            this.lstFolders.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFolders.FormattingEnabled = true;
            this.lstFolders.ItemHeight = 16;
            this.lstFolders.Location = new System.Drawing.Point(206, 94);
            this.lstFolders.Name = "lstFolders";
            this.lstFolders.Size = new System.Drawing.Size(249, 180);
            this.lstFolders.TabIndex = 2;
            this.lstFolders.SelectedIndexChanged += new System.EventHandler(this.lstFolders_SelectedIndexChanged);
            // 
            // lstFiles
            // 
            this.lstFiles.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFiles.FormattingEnabled = true;
            this.lstFiles.ItemHeight = 16;
            this.lstFiles.Location = new System.Drawing.Point(471, 94);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(312, 180);
            this.lstFiles.TabIndex = 2;
            // 
            // lblDrives
            // 
            this.lblDrives.AutoSize = true;
            this.lblDrives.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrives.Location = new System.Drawing.Point(30, 73);
            this.lblDrives.Name = "lblDrives";
            this.lblDrives.Size = new System.Drawing.Size(53, 18);
            this.lblDrives.TabIndex = 0;
            this.lblDrives.Text = "Drives";
            // 
            // lblFolders
            // 
            this.lblFolders.AutoSize = true;
            this.lblFolders.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFolders.Location = new System.Drawing.Point(203, 73);
            this.lblFolders.Name = "lblFolders";
            this.lblFolders.Size = new System.Drawing.Size(61, 18);
            this.lblFolders.TabIndex = 0;
            this.lblFolders.Text = "Folders";
            // 
            // lblFiles
            // 
            this.lblFiles.AutoSize = true;
            this.lblFiles.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFiles.Location = new System.Drawing.Point(468, 73);
            this.lblFiles.Name = "lblFiles";
            this.lblFiles.Size = new System.Drawing.Size(42, 18);
            this.lblFiles.TabIndex = 0;
            this.lblFiles.Text = "Files";
            // 
            // btnAddPicture
            // 
            this.btnAddPicture.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPicture.Location = new System.Drawing.Point(487, 295);
            this.btnAddPicture.Name = "btnAddPicture";
            this.btnAddPicture.Size = new System.Drawing.Size(149, 33);
            this.btnAddPicture.TabIndex = 3;
            this.btnAddPicture.Text = "Add Picture";
            this.btnAddPicture.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(654, 295);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(118, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // frmAddtPicture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(812, 359);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnAddPicture);
            this.Controls.Add(this.lstFiles);
            this.Controls.Add(this.lstFolders);
            this.Controls.Add(this.lstDrives);
            this.Controls.Add(this.txtPictureName);
            this.Controls.Add(this.lblFiles);
            this.Controls.Add(this.lblFolders);
            this.Controls.Add(this.lblDrives);
            this.Controls.Add(this.lblPictureName);
            this.ForeColor = System.Drawing.Color.DarkGreen;
            this.Name = "frmAddtPicture";
            this.Text = "Add Picture";
            this.Load += new System.EventHandler(this.frmAddtPicture_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPictureName;
        private System.Windows.Forms.TextBox txtPictureName;
        private System.Windows.Forms.ListBox lstDrives;
        private System.Windows.Forms.ListBox lstFolders;
        private System.Windows.Forms.ListBox lstFiles;
        private System.Windows.Forms.Label lblDrives;
        private System.Windows.Forms.Label lblFolders;
        private System.Windows.Forms.Label lblFiles;
        private System.Windows.Forms.Button btnAddPicture;
        private System.Windows.Forms.Button btnExit;
    }
}