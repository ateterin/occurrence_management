﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OccurrenceManagement
{
    public partial class frmAddRecord : Form
    {
        private StringBuilder strNotes_;
        private DateTime actionDate_;
        private int action_id_;
        private string strAction_;

        dsActionTypes.dtActionTypesDataTable dtATypes = new dsActionTypes.dtActionTypesDataTable();
        dsActionTypesTableAdapters.dtActionTypesTableAdapter daATypes = new dsActionTypesTableAdapters.dtActionTypesTableAdapter();

        public frmAddRecord(ref StringBuilder strNotes)
        {
            strNotes_ = strNotes;
            InitializeComponent();
        }

        private void frmAddRecord_Load(object sender, EventArgs e)
        {
            //this.Font = new Font(this.Font.FontFamily, this.Font.SizeInPoints * 125 / 96);
            //base.OnLoad(e);

            daATypes.Fill(dtATypes);
            cmbARActionTypes.DataSource = dtATypes;
            cmbARActionTypes.DisplayMember = "description";
            cmbARActionTypes.ValueMember = "action_id";
            

        }

        private void btnARCancel_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnARAddRecord_Click(object sender, EventArgs e)
        {

            strNotes_.Append(txtARAction.Text);
            actionDate_ = dtpARDate.Value;
            action_id_ = Convert.ToInt32(cmbARActionTypes.SelectedValue.ToString());
            strAction_ = cmbARActionTypes.Text;
          
            this.DialogResult = DialogResult.OK;
        }

        public int action_id 
        { 
            get 
            { 
                return (action_id_); 
            } 
        }

        public DateTime actionDate 
        { 
            get 
            { 
                return (actionDate_); 
            } 
        }

        public string strAction 
        { 
            get 
            { 
                return (strAction_); 
            } 
        }

        private void cmbARActionTypes_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = (char)Keys.None;
        }

    }
}
