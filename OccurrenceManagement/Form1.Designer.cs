﻿namespace OccurrenceManagement
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.lblOccurrenceNumber = new System.Windows.Forms.Label();
            this.txtOccurrenceNumber = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.tbcOccurrence = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.rdbGOther = new System.Windows.Forms.RadioButton();
            this.rdbGEmail = new System.Windows.Forms.RadioButton();
            this.txtGLostDays = new System.Windows.Forms.TextBox();
            this.rdbGTelephone = new System.Windows.Forms.RadioButton();
            this.rdbGCheckOut = new System.Windows.Forms.RadioButton();
            this.chkLTInjury = new System.Windows.Forms.CheckBox();
            this.chkAssignToRM = new System.Windows.Forms.CheckBox();
            this.chkNoTicket = new System.Windows.Forms.CheckBox();
            this.btnGAddRecord = new System.Windows.Forms.Button();
            this.btnGGetData = new System.Windows.Forms.Button();
            this.dgvGHistory = new System.Windows.Forms.DataGridView();
            this.grpGPolice = new System.Windows.Forms.GroupBox();
            this.lblGDatePolice = new System.Windows.Forms.Label();
            this.lblGBadge = new System.Windows.Forms.Label();
            this.dtpGDatePolice = new System.Windows.Forms.DateTimePicker();
            this.chkGPolice = new System.Windows.Forms.CheckBox();
            this.txtGBadge = new System.Windows.Forms.TextBox();
            this.txtGDescription = new System.Windows.Forms.TextBox();
            this.dtpGDateR = new System.Windows.Forms.DateTimePicker();
            this.dtpGDate = new System.Windows.Forms.DateTimePicker();
            this.cmbGWeather = new System.Windows.Forms.ComboBox();
            this.cmbGSubtype = new System.Windows.Forms.ComboBox();
            this.cmbGLocation = new System.Windows.Forms.ComboBox();
            this.lblGDateR = new System.Windows.Forms.Label();
            this.lblGWeather = new System.Windows.Forms.Label();
            this.cmbGType = new System.Windows.Forms.ComboBox();
            this.lblGHistory = new System.Windows.Forms.Label();
            this.lblGDescription = new System.Windows.Forms.Label();
            this.lblGDate = new System.Windows.Forms.Label();
            this.lblGReported = new System.Windows.Forms.Label();
            this.lblGLocation = new System.Windows.Forms.Label();
            this.lblGStatus = new System.Windows.Forms.Label();
            this.lblGClosed = new System.Windows.Forms.Label();
            this.lblGOpened = new System.Windows.Forms.Label();
            this.lblGSubtype = new System.Windows.Forms.Label();
            this.lblGLostDays = new System.Windows.Forms.Label();
            this.lblGType = new System.Windows.Forms.Label();
            this.grpGCost = new System.Windows.Forms.GroupBox();
            this.txtGVouchers = new System.Windows.Forms.TextBox();
            this.txtGFinalCost = new System.Windows.Forms.TextBox();
            this.txtGEstimatedCost = new System.Windows.Forms.TextBox();
            this.lblGVouchers = new System.Windows.Forms.Label();
            this.lblGEstimatedCost = new System.Windows.Forms.Label();
            this.lblGFinalCost = new System.Windows.Forms.Label();
            this.tabVehicle = new System.Windows.Forms.TabPage();
            this.dgvVCarMovements = new System.Windows.Forms.DataGridView();
            this.lblVCarMovements = new System.Windows.Forms.Label();
            this.lblVServices = new System.Windows.Forms.Label();
            this.dgvVServices = new System.Windows.Forms.DataGridView();
            this.grpVPNF = new System.Windows.Forms.GroupBox();
            this.cmbVAccident = new System.Windows.Forms.ComboBox();
            this.lblVPNFVehicleType = new System.Windows.Forms.Label();
            this.lblVAccident = new System.Windows.Forms.Label();
            this.cmbVPNFVehicleType = new System.Windows.Forms.ComboBox();
            this.cmbVFault = new System.Windows.Forms.ComboBox();
            this.lblVFault = new System.Windows.Forms.Label();
            this.txtVPNFUnitNumber = new System.Windows.Forms.TextBox();
            this.lblVPNFUnitNumber = new System.Windows.Forms.Label();
            this.grpVVehicle = new System.Windows.Forms.GroupBox();
            this.txtVMake = new System.Windows.Forms.TextBox();
            this.lblVVehicleID = new System.Windows.Forms.Label();
            this.txtVVehicleID = new System.Windows.Forms.TextBox();
            this.lblVLicensePlate = new System.Windows.Forms.Label();
            this.txtVLicensePlate = new System.Windows.Forms.TextBox();
            this.txtVNumberOfParks = new System.Windows.Forms.TextBox();
            this.lblVMake = new System.Windows.Forms.Label();
            this.lblVNumberOfParks = new System.Windows.Forms.Label();
            this.lblVModel = new System.Windows.Forms.Label();
            this.txtVOdometer = new System.Windows.Forms.TextBox();
            this.txtVModel = new System.Windows.Forms.TextBox();
            this.lblVOdometer = new System.Windows.Forms.Label();
            this.lblVModelYear = new System.Windows.Forms.Label();
            this.txtVColour = new System.Windows.Forms.TextBox();
            this.txtVModelYear = new System.Windows.Forms.TextBox();
            this.lblVColour = new System.Windows.Forms.Label();
            this.grpVCustomer = new System.Windows.Forms.GroupBox();
            this.txtVProvince = new System.Windows.Forms.TextBox();
            this.lblVCustomerID = new System.Windows.Forms.Label();
            this.txtVCustomerID = new System.Windows.Forms.TextBox();
            this.lblVFirstName = new System.Windows.Forms.Label();
            this.txtVFirstName = new System.Windows.Forms.TextBox();
            this.lblVLastName = new System.Windows.Forms.Label();
            this.txtVLastName = new System.Windows.Forms.TextBox();
            this.lblVAddress1 = new System.Windows.Forms.Label();
            this.txtVAddress1 = new System.Windows.Forms.TextBox();
            this.lblVAddress2 = new System.Windows.Forms.Label();
            this.txtVAddress2 = new System.Windows.Forms.TextBox();
            this.lblVCity = new System.Windows.Forms.Label();
            this.txtVCity = new System.Windows.Forms.TextBox();
            this.lblVProvince = new System.Windows.Forms.Label();
            this.lblVPostalCode = new System.Windows.Forms.Label();
            this.txtVPostalCode = new System.Windows.Forms.TextBox();
            this.lblVEmail = new System.Windows.Forms.Label();
            this.txtVFax = new System.Windows.Forms.TextBox();
            this.txtVEmail = new System.Windows.Forms.TextBox();
            this.lblVFax = new System.Windows.Forms.Label();
            this.lblVHomePhone = new System.Windows.Forms.Label();
            this.txtVCellPhone = new System.Windows.Forms.TextBox();
            this.txtVHomePhone = new System.Windows.Forms.TextBox();
            this.lblVCellPhone = new System.Windows.Forms.Label();
            this.lblVWorkPhone = new System.Windows.Forms.Label();
            this.txtVWorkPhone = new System.Windows.Forms.TextBox();
            this.grpVTicket = new System.Windows.Forms.GroupBox();
            this.txtVCloseDate = new System.Windows.Forms.TextBox();
            this.txtVOpenDate = new System.Windows.Forms.TextBox();
            this.txtVTicketID = new System.Windows.Forms.TextBox();
            this.lblVTicketID = new System.Windows.Forms.Label();
            this.lblVTicketNumber = new System.Windows.Forms.Label();
            this.txtVBarCode = new System.Windows.Forms.TextBox();
            this.txtVTicketNumber = new System.Windows.Forms.TextBox();
            this.lblVDateClose = new System.Windows.Forms.Label();
            this.lblVIssueNumber = new System.Windows.Forms.Label();
            this.lblVDateOpen = new System.Windows.Forms.Label();
            this.txtVIssueNumber = new System.Windows.Forms.TextBox();
            this.lblVBarCode = new System.Windows.Forms.Label();
            this.lblVPinNumber = new System.Windows.Forms.Label();
            this.txtVPinNumber = new System.Windows.Forms.TextBox();
            this.tabAttachments = new System.Windows.Forms.TabPage();
            this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
            this.btnDeletePicture = new System.Windows.Forms.Button();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.btnAddPicture = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lstPictures = new System.Windows.Forms.ListBox();
            this.tabDocuments = new System.Windows.Forms.TabPage();
            this.btnFinalReleaseRepairFrench = new System.Windows.Forms.Button();
            this.btnFinalReleaseVoucherFrench = new System.Windows.Forms.Button();
            this.btnFinalReleaseFrench = new System.Windows.Forms.Button();
            this.btnCustomerCopyFrench = new System.Windows.Forms.Button();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btnOccurrencesOverdue = new System.Windows.Forms.Button();
            this.btnFinalReleaseRepair = new System.Windows.Forms.Button();
            this.btnFinalReleaseVoucher = new System.Windows.Forms.Button();
            this.btnFinalRelease = new System.Windows.Forms.Button();
            this.btnInternalCopy = new System.Windows.Forms.Button();
            this.btnCustomerCopy = new System.Windows.Forms.Button();
            this.tabRisk = new System.Windows.Forms.TabPage();
            this.btnRSendC = new System.Windows.Forms.Button();
            this.btnRSendB = new System.Windows.Forms.Button();
            this.btnRAddRecord = new System.Windows.Forms.Button();
            this.picLine10 = new System.Windows.Forms.PictureBox();
            this.picLine9 = new System.Windows.Forms.PictureBox();
            this.picLine8 = new System.Windows.Forms.PictureBox();
            this.picLine7 = new System.Windows.Forms.PictureBox();
            this.picLine6 = new System.Windows.Forms.PictureBox();
            this.picLine5 = new System.Windows.Forms.PictureBox();
            this.picLine4 = new System.Windows.Forms.PictureBox();
            this.picLine3 = new System.Windows.Forms.PictureBox();
            this.picLine1 = new System.Windows.Forms.PictureBox();
            this.picLine2 = new System.Windows.Forms.PictureBox();
            this.dtpRJudgementDate = new System.Windows.Forms.DateTimePicker();
            this.dtpRTrialDate = new System.Windows.Forms.DateTimePicker();
            this.lblRJudgementDate = new System.Windows.Forms.Label();
            this.dtpRPreTrialDate = new System.Windows.Forms.DateTimePicker();
            this.lblRTrialDate = new System.Windows.Forms.Label();
            this.dtpRDefenceDate = new System.Windows.Forms.DateTimePicker();
            this.lblRPreTrialDate = new System.Windows.Forms.Label();
            this.dtpRSOCDate = new System.Windows.Forms.DateTimePicker();
            this.lblRDefenceDate = new System.Windows.Forms.Label();
            this.chkRJudgement = new System.Windows.Forms.CheckBox();
            this.dtpRLetterDate = new System.Windows.Forms.DateTimePicker();
            this.chkRTrial = new System.Windows.Forms.CheckBox();
            this.lblRSOCDate = new System.Windows.Forms.Label();
            this.chkRPreTrial = new System.Windows.Forms.CheckBox();
            this.lblRSmallClaim = new System.Windows.Forms.Label();
            this.chkRDefence = new System.Windows.Forms.CheckBox();
            this.lblRLetterDate = new System.Windows.Forms.Label();
            this.chkRSOC = new System.Windows.Forms.CheckBox();
            this.dtpRResolvedCDate = new System.Windows.Forms.DateTimePicker();
            this.chkRLetter = new System.Windows.Forms.CheckBox();
            this.lblRResolvedCDate = new System.Windows.Forms.Label();
            this.dtpRResolvedICDate = new System.Windows.Forms.DateTimePicker();
            this.chkRResolvedC = new System.Windows.Forms.CheckBox();
            this.lblRResolvedICDate = new System.Windows.Forms.Label();
            this.txtRAdjuster = new System.Windows.Forms.TextBox();
            this.chkRResolvedIC = new System.Windows.Forms.CheckBox();
            this.dtpRReportedDate = new System.Windows.Forms.DateTimePicker();
            this.txtRClaim = new System.Windows.Forms.TextBox();
            this.lblRReportedDate = new System.Windows.Forms.Label();
            this.lblRAdjuster = new System.Windows.Forms.Label();
            this.dtpRAssignedDate = new System.Windows.Forms.DateTimePicker();
            this.txtRNotes = new System.Windows.Forms.TextBox();
            this.lblRClaim = new System.Windows.Forms.Label();
            this.chkRReported = new System.Windows.Forms.CheckBox();
            this.lblRAssignedDate = new System.Windows.Forms.Label();
            this.lblRNotes = new System.Windows.Forms.Label();
            this.chkRAssigned = new System.Windows.Forms.CheckBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.chkClosed = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkAttachments = new System.Windows.Forms.CheckBox();
            this.btnChequeRequisition = new System.Windows.Forms.Button();
            this.tbcOccurrence.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGHistory)).BeginInit();
            this.grpGPolice.SuspendLayout();
            this.grpGCost.SuspendLayout();
            this.tabVehicle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVCarMovements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVServices)).BeginInit();
            this.grpVPNF.SuspendLayout();
            this.grpVVehicle.SuspendLayout();
            this.grpVCustomer.SuspendLayout();
            this.grpVTicket.SuspendLayout();
            this.tabAttachments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabDocuments.SuspendLayout();
            this.tabRisk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLine10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblTitle.Location = new System.Drawing.Point(325, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(489, 36);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "OCCURRENCE  MANAGEMENT";
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.ForeColor = System.Drawing.Color.Firebrick;
            this.btnCreate.Location = new System.Drawing.Point(566, 65);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(78, 27);
            this.btnCreate.TabIndex = 2;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpen.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnOpen.Location = new System.Drawing.Point(415, 65);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 27);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // lblOccurrenceNumber
            // 
            this.lblOccurrenceNumber.AutoSize = true;
            this.lblOccurrenceNumber.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOccurrenceNumber.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblOccurrenceNumber.Location = new System.Drawing.Point(148, 67);
            this.lblOccurrenceNumber.Name = "lblOccurrenceNumber";
            this.lblOccurrenceNumber.Size = new System.Drawing.Size(101, 18);
            this.lblOccurrenceNumber.TabIndex = 3;
            this.lblOccurrenceNumber.Text = "Occurrence #";
            // 
            // txtOccurrenceNumber
            // 
            this.txtOccurrenceNumber.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOccurrenceNumber.Location = new System.Drawing.Point(255, 64);
            this.txtOccurrenceNumber.Name = "txtOccurrenceNumber";
            this.txtOccurrenceNumber.Size = new System.Drawing.Size(154, 26);
            this.txtOccurrenceNumber.TabIndex = 4;
            // 
            // btnFind
            // 
            this.btnFind.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFind.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnFind.Location = new System.Drawing.Point(496, 65);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(64, 27);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // tbcOccurrence
            // 
            this.tbcOccurrence.Controls.Add(this.tabGeneral);
            this.tbcOccurrence.Controls.Add(this.tabVehicle);
            this.tbcOccurrence.Controls.Add(this.tabAttachments);
            this.tbcOccurrence.Controls.Add(this.tabDocuments);
            this.tbcOccurrence.Controls.Add(this.tabRisk);
            this.tbcOccurrence.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcOccurrence.Location = new System.Drawing.Point(12, 107);
            this.tbcOccurrence.Name = "tbcOccurrence";
            this.tbcOccurrence.SelectedIndex = 0;
            this.tbcOccurrence.Size = new System.Drawing.Size(1057, 594);
            this.tbcOccurrence.TabIndex = 5;
            this.tbcOccurrence.Visible = false;
            // 
            // tabGeneral
            // 
            this.tabGeneral.BackColor = System.Drawing.Color.PaleGreen;
            this.tabGeneral.Controls.Add(this.rdbGOther);
            this.tabGeneral.Controls.Add(this.rdbGEmail);
            this.tabGeneral.Controls.Add(this.txtGLostDays);
            this.tabGeneral.Controls.Add(this.rdbGTelephone);
            this.tabGeneral.Controls.Add(this.rdbGCheckOut);
            this.tabGeneral.Controls.Add(this.chkLTInjury);
            this.tabGeneral.Controls.Add(this.chkAssignToRM);
            this.tabGeneral.Controls.Add(this.chkNoTicket);
            this.tabGeneral.Controls.Add(this.btnGAddRecord);
            this.tabGeneral.Controls.Add(this.btnGGetData);
            this.tabGeneral.Controls.Add(this.dgvGHistory);
            this.tabGeneral.Controls.Add(this.grpGPolice);
            this.tabGeneral.Controls.Add(this.txtGDescription);
            this.tabGeneral.Controls.Add(this.dtpGDateR);
            this.tabGeneral.Controls.Add(this.dtpGDate);
            this.tabGeneral.Controls.Add(this.cmbGWeather);
            this.tabGeneral.Controls.Add(this.cmbGSubtype);
            this.tabGeneral.Controls.Add(this.cmbGLocation);
            this.tabGeneral.Controls.Add(this.lblGDateR);
            this.tabGeneral.Controls.Add(this.lblGWeather);
            this.tabGeneral.Controls.Add(this.cmbGType);
            this.tabGeneral.Controls.Add(this.lblGHistory);
            this.tabGeneral.Controls.Add(this.lblGDescription);
            this.tabGeneral.Controls.Add(this.lblGDate);
            this.tabGeneral.Controls.Add(this.lblGReported);
            this.tabGeneral.Controls.Add(this.lblGLocation);
            this.tabGeneral.Controls.Add(this.lblGStatus);
            this.tabGeneral.Controls.Add(this.lblGClosed);
            this.tabGeneral.Controls.Add(this.lblGOpened);
            this.tabGeneral.Controls.Add(this.lblGSubtype);
            this.tabGeneral.Controls.Add(this.lblGLostDays);
            this.tabGeneral.Controls.Add(this.lblGType);
            this.tabGeneral.Controls.Add(this.grpGCost);
            this.tabGeneral.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabGeneral.ForeColor = System.Drawing.Color.DarkGreen;
            this.tabGeneral.Location = new System.Drawing.Point(4, 26);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(1049, 564);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "General";
            // 
            // rdbGOther
            // 
            this.rdbGOther.AutoSize = true;
            this.rdbGOther.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGOther.Location = new System.Drawing.Point(961, 111);
            this.rdbGOther.Name = "rdbGOther";
            this.rdbGOther.Size = new System.Drawing.Size(58, 20);
            this.rdbGOther.TabIndex = 26;
            this.rdbGOther.TabStop = true;
            this.rdbGOther.Text = "Other";
            this.rdbGOther.UseVisualStyleBackColor = true;
            this.rdbGOther.Visible = false;
            // 
            // rdbGEmail
            // 
            this.rdbGEmail.AutoSize = true;
            this.rdbGEmail.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGEmail.Location = new System.Drawing.Point(888, 111);
            this.rdbGEmail.Name = "rdbGEmail";
            this.rdbGEmail.Size = new System.Drawing.Size(59, 20);
            this.rdbGEmail.TabIndex = 26;
            this.rdbGEmail.TabStop = true;
            this.rdbGEmail.Text = "Email";
            this.rdbGEmail.UseVisualStyleBackColor = true;
            this.rdbGEmail.Visible = false;
            // 
            // txtGLostDays
            // 
            this.txtGLostDays.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGLostDays.Location = new System.Drawing.Point(550, 257);
            this.txtGLostDays.Name = "txtGLostDays";
            this.txtGLostDays.Size = new System.Drawing.Size(53, 23);
            this.txtGLostDays.TabIndex = 5;
            this.txtGLostDays.Visible = false;
            // 
            // rdbGTelephone
            // 
            this.rdbGTelephone.AutoSize = true;
            this.rdbGTelephone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGTelephone.Location = new System.Drawing.Point(815, 111);
            this.rdbGTelephone.Name = "rdbGTelephone";
            this.rdbGTelephone.Size = new System.Drawing.Size(63, 20);
            this.rdbGTelephone.TabIndex = 26;
            this.rdbGTelephone.TabStop = true;
            this.rdbGTelephone.Text = "Phone";
            this.rdbGTelephone.UseVisualStyleBackColor = true;
            this.rdbGTelephone.Visible = false;
            // 
            // rdbGCheckOut
            // 
            this.rdbGCheckOut.AutoSize = true;
            this.rdbGCheckOut.Checked = true;
            this.rdbGCheckOut.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbGCheckOut.Location = new System.Drawing.Point(721, 112);
            this.rdbGCheckOut.Name = "rdbGCheckOut";
            this.rdbGCheckOut.Size = new System.Drawing.Size(88, 20);
            this.rdbGCheckOut.TabIndex = 26;
            this.rdbGCheckOut.TabStop = true;
            this.rdbGCheckOut.Text = "Check-Out";
            this.rdbGCheckOut.UseVisualStyleBackColor = true;
            this.rdbGCheckOut.Visible = false;
            // 
            // chkLTInjury
            // 
            this.chkLTInjury.AutoSize = true;
            this.chkLTInjury.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLTInjury.Location = new System.Drawing.Point(348, 260);
            this.chkLTInjury.Name = "chkLTInjury";
            this.chkLTInjury.Size = new System.Drawing.Size(126, 20);
            this.chkLTInjury.TabIndex = 4;
            this.chkLTInjury.Text = "Lost Time Injury";
            this.chkLTInjury.UseVisualStyleBackColor = true;
            this.chkLTInjury.Visible = false;
            this.chkLTInjury.CheckedChanged += new System.EventHandler(this.chkLTInjury_CheckedChanged);
            // 
            // chkAssignToRM
            // 
            this.chkAssignToRM.AutoSize = true;
            this.chkAssignToRM.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAssignToRM.Location = new System.Drawing.Point(95, 260);
            this.chkAssignToRM.Name = "chkAssignToRM";
            this.chkAssignToRM.Size = new System.Drawing.Size(202, 20);
            this.chkAssignToRM.TabIndex = 4;
            this.chkAssignToRM.Text = "Assign to Risk Management";
            this.chkAssignToRM.UseVisualStyleBackColor = true;
            this.chkAssignToRM.Visible = false;
            this.chkAssignToRM.CheckedChanged += new System.EventHandler(this.chkAssignToRM_CheckedChanged);
            // 
            // chkNoTicket
            // 
            this.chkNoTicket.AutoSize = true;
            this.chkNoTicket.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNoTicket.Location = new System.Drawing.Point(253, 301);
            this.chkNoTicket.Name = "chkNoTicket";
            this.chkNoTicket.Size = new System.Drawing.Size(85, 20);
            this.chkNoTicket.TabIndex = 4;
            this.chkNoTicket.Text = "No Ticket";
            this.chkNoTicket.UseVisualStyleBackColor = true;
            this.chkNoTicket.Visible = false;
            this.chkNoTicket.CheckedChanged += new System.EventHandler(this.chkNoTicket_CheckedChanged);
            // 
            // btnGAddRecord
            // 
            this.btnGAddRecord.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGAddRecord.Location = new System.Drawing.Point(408, 290);
            this.btnGAddRecord.Name = "btnGAddRecord";
            this.btnGAddRecord.Size = new System.Drawing.Size(170, 39);
            this.btnGAddRecord.TabIndex = 24;
            this.btnGAddRecord.Text = "Add History Record";
            this.btnGAddRecord.UseVisualStyleBackColor = true;
            this.btnGAddRecord.Visible = false;
            this.btnGAddRecord.Click += new System.EventHandler(this.btnGAddRecord_Click);
            // 
            // btnGGetData
            // 
            this.btnGGetData.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGGetData.Location = new System.Drawing.Point(93, 289);
            this.btnGGetData.Name = "btnGGetData";
            this.btnGGetData.Size = new System.Drawing.Size(138, 39);
            this.btnGGetData.TabIndex = 24;
            this.btnGGetData.Text = "Get Ticket Data";
            this.btnGGetData.UseVisualStyleBackColor = true;
            this.btnGGetData.Visible = false;
            this.btnGGetData.Click += new System.EventHandler(this.btnGGetData_Click);
            // 
            // dgvGHistory
            // 
            this.dgvGHistory.AllowUserToAddRows = false;
            this.dgvGHistory.AllowUserToDeleteRows = false;
            this.dgvGHistory.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvGHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvGHistory.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvGHistory.Location = new System.Drawing.Point(17, 363);
            this.dgvGHistory.Name = "dgvGHistory";
            this.dgvGHistory.RowHeadersVisible = false;
            this.dgvGHistory.Size = new System.Drawing.Size(1014, 190);
            this.dgvGHistory.TabIndex = 23;
            this.dgvGHistory.Visible = false;
            // 
            // grpGPolice
            // 
            this.grpGPolice.Controls.Add(this.lblGDatePolice);
            this.grpGPolice.Controls.Add(this.lblGBadge);
            this.grpGPolice.Controls.Add(this.dtpGDatePolice);
            this.grpGPolice.Controls.Add(this.chkGPolice);
            this.grpGPolice.Controls.Add(this.txtGBadge);
            this.grpGPolice.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGPolice.Location = new System.Drawing.Point(642, 133);
            this.grpGPolice.Name = "grpGPolice";
            this.grpGPolice.Size = new System.Drawing.Size(389, 112);
            this.grpGPolice.TabIndex = 22;
            this.grpGPolice.TabStop = false;
            this.grpGPolice.Text = "Police";
            this.grpGPolice.Visible = false;
            // 
            // lblGDatePolice
            // 
            this.lblGDatePolice.AutoSize = true;
            this.lblGDatePolice.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDatePolice.Location = new System.Drawing.Point(17, 58);
            this.lblGDatePolice.Name = "lblGDatePolice";
            this.lblGDatePolice.Size = new System.Drawing.Size(79, 16);
            this.lblGDatePolice.TabIndex = 0;
            this.lblGDatePolice.Text = "Date called";
            this.lblGDatePolice.Visible = false;
            // 
            // lblGBadge
            // 
            this.lblGBadge.AutoSize = true;
            this.lblGBadge.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGBadge.Location = new System.Drawing.Point(17, 85);
            this.lblGBadge.Name = "lblGBadge";
            this.lblGBadge.Size = new System.Drawing.Size(138, 16);
            this.lblGBadge.TabIndex = 0;
            this.lblGBadge.Text = "Police Report/Badge";
            this.lblGBadge.Visible = false;
            // 
            // dtpGDatePolice
            // 
            this.dtpGDatePolice.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGDatePolice.Location = new System.Drawing.Point(122, 51);
            this.dtpGDatePolice.Name = "dtpGDatePolice";
            this.dtpGDatePolice.Size = new System.Drawing.Size(245, 23);
            this.dtpGDatePolice.TabIndex = 2;
            this.dtpGDatePolice.Visible = false;
            // 
            // chkGPolice
            // 
            this.chkGPolice.AutoSize = true;
            this.chkGPolice.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGPolice.Location = new System.Drawing.Point(19, 26);
            this.chkGPolice.Name = "chkGPolice";
            this.chkGPolice.Size = new System.Drawing.Size(106, 20);
            this.chkGPolice.TabIndex = 4;
            this.chkGPolice.Text = "Police called";
            this.chkGPolice.UseVisualStyleBackColor = true;
            this.chkGPolice.Visible = false;
            this.chkGPolice.CheckedChanged += new System.EventHandler(this.chkGPolice_CheckedChanged);
            // 
            // txtGBadge
            // 
            this.txtGBadge.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGBadge.Location = new System.Drawing.Point(167, 80);
            this.txtGBadge.Name = "txtGBadge";
            this.txtGBadge.Size = new System.Drawing.Size(200, 23);
            this.txtGBadge.TabIndex = 5;
            this.txtGBadge.Visible = false;
            // 
            // txtGDescription
            // 
            this.txtGDescription.Location = new System.Drawing.Point(95, 116);
            this.txtGDescription.Multiline = true;
            this.txtGDescription.Name = "txtGDescription";
            this.txtGDescription.Size = new System.Drawing.Size(508, 129);
            this.txtGDescription.TabIndex = 3;
            this.txtGDescription.Visible = false;
            // 
            // dtpGDateR
            // 
            this.dtpGDateR.Location = new System.Drawing.Point(764, 75);
            this.dtpGDateR.Name = "dtpGDateR";
            this.dtpGDateR.Size = new System.Drawing.Size(252, 22);
            this.dtpGDateR.TabIndex = 2;
            this.dtpGDateR.Visible = false;
            // 
            // dtpGDate
            // 
            this.dtpGDate.Location = new System.Drawing.Point(764, 42);
            this.dtpGDate.Name = "dtpGDate";
            this.dtpGDate.Size = new System.Drawing.Size(252, 22);
            this.dtpGDate.TabIndex = 2;
            this.dtpGDate.Visible = false;
            // 
            // cmbGWeather
            // 
            this.cmbGWeather.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGWeather.FormattingEnabled = true;
            this.cmbGWeather.Location = new System.Drawing.Point(424, 78);
            this.cmbGWeather.Name = "cmbGWeather";
            this.cmbGWeather.Size = new System.Drawing.Size(173, 24);
            this.cmbGWeather.TabIndex = 1;
            this.cmbGWeather.Visible = false;
            // 
            // cmbGSubtype
            // 
            this.cmbGSubtype.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGSubtype.FormattingEnabled = true;
            this.cmbGSubtype.Location = new System.Drawing.Point(97, 78);
            this.cmbGSubtype.Name = "cmbGSubtype";
            this.cmbGSubtype.Size = new System.Drawing.Size(233, 24);
            this.cmbGSubtype.TabIndex = 1;
            this.cmbGSubtype.Visible = false;
            this.cmbGSubtype.SelectedIndexChanged += new System.EventHandler(this.cmbGSubtype_SelectedIndexChanged);
            // 
            // cmbGLocation
            // 
            this.cmbGLocation.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGLocation.FormattingEnabled = true;
            this.cmbGLocation.Location = new System.Drawing.Point(424, 45);
            this.cmbGLocation.Name = "cmbGLocation";
            this.cmbGLocation.Size = new System.Drawing.Size(173, 24);
            this.cmbGLocation.TabIndex = 1;
            this.cmbGLocation.Visible = false;
            this.cmbGLocation.SelectedIndexChanged += new System.EventHandler(this.cmbGLocation_SelectedIndexChanged);
            // 
            // lblGDateR
            // 
            this.lblGDateR.AutoSize = true;
            this.lblGDateR.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDateR.Location = new System.Drawing.Point(629, 81);
            this.lblGDateR.Name = "lblGDateR";
            this.lblGDateR.Size = new System.Drawing.Size(100, 16);
            this.lblGDateR.TabIndex = 0;
            this.lblGDateR.Text = "Date  reported";
            this.lblGDateR.Visible = false;
            // 
            // lblGWeather
            // 
            this.lblGWeather.AutoSize = true;
            this.lblGWeather.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGWeather.Location = new System.Drawing.Point(353, 81);
            this.lblGWeather.Name = "lblGWeather";
            this.lblGWeather.Size = new System.Drawing.Size(62, 16);
            this.lblGWeather.TabIndex = 0;
            this.lblGWeather.Text = "Weather";
            this.lblGWeather.Visible = false;
            // 
            // cmbGType
            // 
            this.cmbGType.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGType.FormattingEnabled = true;
            this.cmbGType.Location = new System.Drawing.Point(97, 45);
            this.cmbGType.Name = "cmbGType";
            this.cmbGType.Size = new System.Drawing.Size(233, 24);
            this.cmbGType.TabIndex = 1;
            this.cmbGType.Visible = false;
            this.cmbGType.SelectedIndexChanged += new System.EventHandler(this.cmbGType_SelectedIndexChanged);
            // 
            // lblGHistory
            // 
            this.lblGHistory.AutoSize = true;
            this.lblGHistory.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGHistory.Location = new System.Drawing.Point(25, 341);
            this.lblGHistory.Name = "lblGHistory";
            this.lblGHistory.Size = new System.Drawing.Size(64, 19);
            this.lblGHistory.TabIndex = 0;
            this.lblGHistory.Text = "History";
            this.lblGHistory.Visible = false;
            // 
            // lblGDescription
            // 
            this.lblGDescription.AutoSize = true;
            this.lblGDescription.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDescription.Location = new System.Drawing.Point(14, 116);
            this.lblGDescription.Name = "lblGDescription";
            this.lblGDescription.Size = new System.Drawing.Size(79, 16);
            this.lblGDescription.TabIndex = 0;
            this.lblGDescription.Text = "Description";
            this.lblGDescription.Visible = false;
            // 
            // lblGDate
            // 
            this.lblGDate.AutoSize = true;
            this.lblGDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDate.Location = new System.Drawing.Point(629, 48);
            this.lblGDate.Name = "lblGDate";
            this.lblGDate.Size = new System.Drawing.Size(129, 16);
            this.lblGDate.TabIndex = 0;
            this.lblGDate.Text = "Date of occurrence";
            this.lblGDate.Visible = false;
            // 
            // lblGReported
            // 
            this.lblGReported.AutoSize = true;
            this.lblGReported.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGReported.Location = new System.Drawing.Point(629, 114);
            this.lblGReported.Name = "lblGReported";
            this.lblGReported.Size = new System.Drawing.Size(86, 16);
            this.lblGReported.TabIndex = 0;
            this.lblGReported.Text = "Reported by";
            this.lblGReported.Visible = false;
            // 
            // lblGLocation
            // 
            this.lblGLocation.AutoSize = true;
            this.lblGLocation.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGLocation.Location = new System.Drawing.Point(353, 48);
            this.lblGLocation.Name = "lblGLocation";
            this.lblGLocation.Size = new System.Drawing.Size(62, 16);
            this.lblGLocation.TabIndex = 0;
            this.lblGLocation.Text = "Location";
            this.lblGLocation.Visible = false;
            // 
            // lblGStatus
            // 
            this.lblGStatus.AutoSize = true;
            this.lblGStatus.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGStatus.Location = new System.Drawing.Point(819, 14);
            this.lblGStatus.Name = "lblGStatus";
            this.lblGStatus.Size = new System.Drawing.Size(109, 16);
            this.lblGStatus.TabIndex = 0;
            this.lblGStatus.Text = "Status: Closed";
            this.lblGStatus.Visible = false;
            // 
            // lblGClosed
            // 
            this.lblGClosed.AutoSize = true;
            this.lblGClosed.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGClosed.Location = new System.Drawing.Point(421, 14);
            this.lblGClosed.Name = "lblGClosed";
            this.lblGClosed.Size = new System.Drawing.Size(270, 16);
            this.lblGClosed.TabIndex = 0;
            this.lblGClosed.Text = "Closed June 6, 2014 by Alexey Teterin";
            this.lblGClosed.Visible = false;
            // 
            // lblGOpened
            // 
            this.lblGOpened.AutoSize = true;
            this.lblGOpened.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGOpened.Location = new System.Drawing.Point(16, 14);
            this.lblGOpened.Name = "lblGOpened";
            this.lblGOpened.Size = new System.Drawing.Size(277, 16);
            this.lblGOpened.TabIndex = 0;
            this.lblGOpened.Text = "Opened June 6, 2014 by Alexey Teterin";
            this.lblGOpened.Visible = false;
            // 
            // lblGSubtype
            // 
            this.lblGSubtype.AutoSize = true;
            this.lblGSubtype.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGSubtype.Location = new System.Drawing.Point(14, 81);
            this.lblGSubtype.Name = "lblGSubtype";
            this.lblGSubtype.Size = new System.Drawing.Size(60, 16);
            this.lblGSubtype.TabIndex = 0;
            this.lblGSubtype.Text = "Subtype";
            this.lblGSubtype.Visible = false;
            // 
            // lblGLostDays
            // 
            this.lblGLostDays.AutoSize = true;
            this.lblGLostDays.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGLostDays.Location = new System.Drawing.Point(477, 261);
            this.lblGLostDays.Name = "lblGLostDays";
            this.lblGLostDays.Size = new System.Drawing.Size(71, 16);
            this.lblGLostDays.TabIndex = 0;
            this.lblGLostDays.Text = "Lost Days";
            this.lblGLostDays.Visible = false;
            // 
            // lblGType
            // 
            this.lblGType.AutoSize = true;
            this.lblGType.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGType.Location = new System.Drawing.Point(16, 49);
            this.lblGType.Name = "lblGType";
            this.lblGType.Size = new System.Drawing.Size(39, 16);
            this.lblGType.TabIndex = 0;
            this.lblGType.Text = "Type";
            this.lblGType.Visible = false;
            // 
            // grpGCost
            // 
            this.grpGCost.Controls.Add(this.txtGVouchers);
            this.grpGCost.Controls.Add(this.txtGFinalCost);
            this.grpGCost.Controls.Add(this.txtGEstimatedCost);
            this.grpGCost.Controls.Add(this.lblGVouchers);
            this.grpGCost.Controls.Add(this.lblGEstimatedCost);
            this.grpGCost.Controls.Add(this.lblGFinalCost);
            this.grpGCost.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGCost.Location = new System.Drawing.Point(642, 251);
            this.grpGCost.Name = "grpGCost";
            this.grpGCost.Size = new System.Drawing.Size(389, 106);
            this.grpGCost.TabIndex = 25;
            this.grpGCost.TabStop = false;
            this.grpGCost.Text = "Cost";
            this.grpGCost.Visible = false;
            // 
            // txtGVouchers
            // 
            this.txtGVouchers.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGVouchers.Location = new System.Drawing.Point(167, 76);
            this.txtGVouchers.Name = "txtGVouchers";
            this.txtGVouchers.Size = new System.Drawing.Size(125, 23);
            this.txtGVouchers.TabIndex = 5;
            this.txtGVouchers.Visible = false;
            // 
            // txtGFinalCost
            // 
            this.txtGFinalCost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGFinalCost.Location = new System.Drawing.Point(167, 48);
            this.txtGFinalCost.Name = "txtGFinalCost";
            this.txtGFinalCost.Size = new System.Drawing.Size(125, 23);
            this.txtGFinalCost.TabIndex = 5;
            this.txtGFinalCost.Visible = false;
            // 
            // txtGEstimatedCost
            // 
            this.txtGEstimatedCost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGEstimatedCost.Location = new System.Drawing.Point(167, 20);
            this.txtGEstimatedCost.Name = "txtGEstimatedCost";
            this.txtGEstimatedCost.Size = new System.Drawing.Size(125, 23);
            this.txtGEstimatedCost.TabIndex = 5;
            this.txtGEstimatedCost.Visible = false;
            // 
            // lblGVouchers
            // 
            this.lblGVouchers.AutoSize = true;
            this.lblGVouchers.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGVouchers.Location = new System.Drawing.Point(22, 79);
            this.lblGVouchers.Name = "lblGVouchers";
            this.lblGVouchers.Size = new System.Drawing.Size(138, 16);
            this.lblGVouchers.TabIndex = 0;
            this.lblGVouchers.Text = "Settlement Vouchers";
            this.lblGVouchers.Visible = false;
            // 
            // lblGEstimatedCost
            // 
            this.lblGEstimatedCost.AutoSize = true;
            this.lblGEstimatedCost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGEstimatedCost.Location = new System.Drawing.Point(22, 23);
            this.lblGEstimatedCost.Name = "lblGEstimatedCost";
            this.lblGEstimatedCost.Size = new System.Drawing.Size(103, 16);
            this.lblGEstimatedCost.TabIndex = 0;
            this.lblGEstimatedCost.Text = "Estimated Cost";
            this.lblGEstimatedCost.Visible = false;
            // 
            // lblGFinalCost
            // 
            this.lblGFinalCost.AutoSize = true;
            this.lblGFinalCost.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGFinalCost.Location = new System.Drawing.Point(22, 51);
            this.lblGFinalCost.Name = "lblGFinalCost";
            this.lblGFinalCost.Size = new System.Drawing.Size(108, 16);
            this.lblGFinalCost.TabIndex = 0;
            this.lblGFinalCost.Text = "Settlement Cost";
            this.lblGFinalCost.Visible = false;
            // 
            // tabVehicle
            // 
            this.tabVehicle.BackColor = System.Drawing.Color.PaleGreen;
            this.tabVehicle.Controls.Add(this.dgvVCarMovements);
            this.tabVehicle.Controls.Add(this.lblVCarMovements);
            this.tabVehicle.Controls.Add(this.lblVServices);
            this.tabVehicle.Controls.Add(this.dgvVServices);
            this.tabVehicle.Controls.Add(this.grpVPNF);
            this.tabVehicle.Controls.Add(this.grpVVehicle);
            this.tabVehicle.Controls.Add(this.grpVCustomer);
            this.tabVehicle.Controls.Add(this.grpVTicket);
            this.tabVehicle.ForeColor = System.Drawing.Color.DarkGreen;
            this.tabVehicle.Location = new System.Drawing.Point(4, 26);
            this.tabVehicle.Name = "tabVehicle";
            this.tabVehicle.Padding = new System.Windows.Forms.Padding(3);
            this.tabVehicle.Size = new System.Drawing.Size(1049, 564);
            this.tabVehicle.TabIndex = 2;
            this.tabVehicle.Text = "Data";
            // 
            // dgvVCarMovements
            // 
            this.dgvVCarMovements.AllowUserToAddRows = false;
            this.dgvVCarMovements.AllowUserToDeleteRows = false;
            this.dgvVCarMovements.AllowUserToResizeColumns = false;
            this.dgvVCarMovements.AllowUserToResizeRows = false;
            this.dgvVCarMovements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVCarMovements.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVCarMovements.Location = new System.Drawing.Point(19, 423);
            this.dgvVCarMovements.Name = "dgvVCarMovements";
            this.dgvVCarMovements.ReadOnly = true;
            this.dgvVCarMovements.RowHeadersVisible = false;
            this.dgvVCarMovements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVCarMovements.Size = new System.Drawing.Size(1024, 133);
            this.dgvVCarMovements.TabIndex = 43;
            this.dgvVCarMovements.Visible = false;
            // 
            // lblVCarMovements
            // 
            this.lblVCarMovements.AutoSize = true;
            this.lblVCarMovements.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVCarMovements.Location = new System.Drawing.Point(35, 401);
            this.lblVCarMovements.Name = "lblVCarMovements";
            this.lblVCarMovements.Size = new System.Drawing.Size(128, 19);
            this.lblVCarMovements.TabIndex = 42;
            this.lblVCarMovements.Text = "Car Movements";
            this.lblVCarMovements.Visible = false;
            // 
            // lblVServices
            // 
            this.lblVServices.AutoSize = true;
            this.lblVServices.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVServices.Location = new System.Drawing.Point(35, 261);
            this.lblVServices.Name = "lblVServices";
            this.lblVServices.Size = new System.Drawing.Size(75, 19);
            this.lblVServices.TabIndex = 42;
            this.lblVServices.Text = "Services";
            this.lblVServices.Visible = false;
            // 
            // dgvVServices
            // 
            this.dgvVServices.AllowUserToAddRows = false;
            this.dgvVServices.AllowUserToDeleteRows = false;
            this.dgvVServices.AllowUserToResizeColumns = false;
            this.dgvVServices.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVServices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.DarkGreen;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVServices.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvVServices.Location = new System.Drawing.Point(19, 283);
            this.dgvVServices.Name = "dgvVServices";
            this.dgvVServices.ReadOnly = true;
            this.dgvVServices.RowHeadersVisible = false;
            this.dgvVServices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVServices.Size = new System.Drawing.Size(318, 115);
            this.dgvVServices.TabIndex = 41;
            this.dgvVServices.Visible = false;
            // 
            // grpVPNF
            // 
            this.grpVPNF.Controls.Add(this.cmbVAccident);
            this.grpVPNF.Controls.Add(this.lblVPNFVehicleType);
            this.grpVPNF.Controls.Add(this.lblVAccident);
            this.grpVPNF.Controls.Add(this.cmbVPNFVehicleType);
            this.grpVPNF.Controls.Add(this.cmbVFault);
            this.grpVPNF.Controls.Add(this.lblVFault);
            this.grpVPNF.Controls.Add(this.txtVPNFUnitNumber);
            this.grpVPNF.Controls.Add(this.lblVPNFUnitNumber);
            this.grpVPNF.Location = new System.Drawing.Point(380, 271);
            this.grpVPNF.Name = "grpVPNF";
            this.grpVPNF.Size = new System.Drawing.Size(359, 140);
            this.grpVPNF.TabIndex = 40;
            this.grpVPNF.TabStop = false;
            this.grpVPNF.Text = "PNF Special";
            this.grpVPNF.Visible = false;
            // 
            // cmbVAccident
            // 
            this.cmbVAccident.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVAccident.FormattingEnabled = true;
            this.cmbVAccident.Items.AddRange(new object[] {
            "N",
            "Y"});
            this.cmbVAccident.Location = new System.Drawing.Point(171, 107);
            this.cmbVAccident.Name = "cmbVAccident";
            this.cmbVAccident.Size = new System.Drawing.Size(51, 24);
            this.cmbVAccident.TabIndex = 38;
            this.cmbVAccident.Visible = false;
            // 
            // lblVPNFVehicleType
            // 
            this.lblVPNFVehicleType.AutoSize = true;
            this.lblVPNFVehicleType.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVPNFVehicleType.Location = new System.Drawing.Point(20, 52);
            this.lblVPNFVehicleType.Name = "lblVPNFVehicleType";
            this.lblVPNFVehicleType.Size = new System.Drawing.Size(119, 16);
            this.lblVPNFVehicleType.TabIndex = 0;
            this.lblVPNFVehicleType.Text = "PNF Vehicle Type";
            this.lblVPNFVehicleType.Visible = false;
            // 
            // lblVAccident
            // 
            this.lblVAccident.AutoSize = true;
            this.lblVAccident.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVAccident.Location = new System.Drawing.Point(23, 110);
            this.lblVAccident.Name = "lblVAccident";
            this.lblVAccident.Size = new System.Drawing.Size(142, 16);
            this.lblVAccident.TabIndex = 36;
            this.lblVAccident.Text = "Accident Preventable";
            this.lblVAccident.Visible = false;
            // 
            // cmbVPNFVehicleType
            // 
            this.cmbVPNFVehicleType.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVPNFVehicleType.FormattingEnabled = true;
            this.cmbVPNFVehicleType.Items.AddRange(new object[] {
            "N",
            "Y"});
            this.cmbVPNFVehicleType.Location = new System.Drawing.Point(171, 52);
            this.cmbVPNFVehicleType.Name = "cmbVPNFVehicleType";
            this.cmbVPNFVehicleType.Size = new System.Drawing.Size(166, 24);
            this.cmbVPNFVehicleType.TabIndex = 39;
            this.cmbVPNFVehicleType.Visible = false;
            // 
            // cmbVFault
            // 
            this.cmbVFault.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVFault.FormattingEnabled = true;
            this.cmbVFault.Items.AddRange(new object[] {
            "N",
            "Y"});
            this.cmbVFault.Location = new System.Drawing.Point(171, 78);
            this.cmbVFault.Name = "cmbVFault";
            this.cmbVFault.Size = new System.Drawing.Size(51, 24);
            this.cmbVFault.TabIndex = 39;
            this.cmbVFault.Visible = false;
            // 
            // lblVFault
            // 
            this.lblVFault.AutoSize = true;
            this.lblVFault.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVFault.Location = new System.Drawing.Point(23, 81);
            this.lblVFault.Name = "lblVFault";
            this.lblVFault.Size = new System.Drawing.Size(124, 16);
            this.lblVFault.TabIndex = 37;
            this.lblVFault.Text = "PNF Driver at fault";
            this.lblVFault.Visible = false;
            // 
            // txtVPNFUnitNumber
            // 
            this.txtVPNFUnitNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVPNFUnitNumber.Location = new System.Drawing.Point(168, 23);
            this.txtVPNFUnitNumber.Name = "txtVPNFUnitNumber";
            this.txtVPNFUnitNumber.Size = new System.Drawing.Size(63, 23);
            this.txtVPNFUnitNumber.TabIndex = 1;
            this.txtVPNFUnitNumber.Visible = false;
            // 
            // lblVPNFUnitNumber
            // 
            this.lblVPNFUnitNumber.AutoSize = true;
            this.lblVPNFUnitNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVPNFUnitNumber.Location = new System.Drawing.Point(20, 26);
            this.lblVPNFUnitNumber.Name = "lblVPNFUnitNumber";
            this.lblVPNFUnitNumber.Size = new System.Drawing.Size(116, 16);
            this.lblVPNFUnitNumber.TabIndex = 0;
            this.lblVPNFUnitNumber.Text = "PNF Unit Number";
            this.lblVPNFUnitNumber.Visible = false;
            // 
            // grpVVehicle
            // 
            this.grpVVehicle.Controls.Add(this.txtVMake);
            this.grpVVehicle.Controls.Add(this.lblVVehicleID);
            this.grpVVehicle.Controls.Add(this.txtVVehicleID);
            this.grpVVehicle.Controls.Add(this.lblVLicensePlate);
            this.grpVVehicle.Controls.Add(this.txtVLicensePlate);
            this.grpVVehicle.Controls.Add(this.txtVNumberOfParks);
            this.grpVVehicle.Controls.Add(this.lblVMake);
            this.grpVVehicle.Controls.Add(this.lblVNumberOfParks);
            this.grpVVehicle.Controls.Add(this.lblVModel);
            this.grpVVehicle.Controls.Add(this.txtVOdometer);
            this.grpVVehicle.Controls.Add(this.txtVModel);
            this.grpVVehicle.Controls.Add(this.lblVOdometer);
            this.grpVVehicle.Controls.Add(this.lblVModelYear);
            this.grpVVehicle.Controls.Add(this.txtVColour);
            this.grpVVehicle.Controls.Add(this.txtVModelYear);
            this.grpVVehicle.Controls.Add(this.lblVColour);
            this.grpVVehicle.Location = new System.Drawing.Point(381, 6);
            this.grpVVehicle.Name = "grpVVehicle";
            this.grpVVehicle.Size = new System.Drawing.Size(358, 259);
            this.grpVVehicle.TabIndex = 2;
            this.grpVVehicle.TabStop = false;
            this.grpVVehicle.Text = "Vehicle";
            this.grpVVehicle.Visible = false;
            // 
            // txtVMake
            // 
            this.txtVMake.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVMake.Location = new System.Drawing.Point(170, 84);
            this.txtVMake.Name = "txtVMake";
            this.txtVMake.Size = new System.Drawing.Size(166, 23);
            this.txtVMake.TabIndex = 1;
            this.txtVMake.Visible = false;
            // 
            // lblVVehicleID
            // 
            this.lblVVehicleID.AutoSize = true;
            this.lblVVehicleID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVVehicleID.Location = new System.Drawing.Point(16, 29);
            this.lblVVehicleID.Name = "lblVVehicleID";
            this.lblVVehicleID.Size = new System.Drawing.Size(70, 16);
            this.lblVVehicleID.TabIndex = 0;
            this.lblVVehicleID.Text = "Vehicle ID";
            this.lblVVehicleID.Visible = false;
            // 
            // txtVVehicleID
            // 
            this.txtVVehicleID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVVehicleID.Location = new System.Drawing.Point(170, 26);
            this.txtVVehicleID.Name = "txtVVehicleID";
            this.txtVVehicleID.Size = new System.Drawing.Size(166, 23);
            this.txtVVehicleID.TabIndex = 1;
            this.txtVVehicleID.Visible = false;
            // 
            // lblVLicensePlate
            // 
            this.lblVLicensePlate.AutoSize = true;
            this.lblVLicensePlate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVLicensePlate.Location = new System.Drawing.Point(16, 58);
            this.lblVLicensePlate.Name = "lblVLicensePlate";
            this.lblVLicensePlate.Size = new System.Drawing.Size(102, 16);
            this.lblVLicensePlate.TabIndex = 0;
            this.lblVLicensePlate.Text = "License Plate";
            this.lblVLicensePlate.Visible = false;
            // 
            // txtVLicensePlate
            // 
            this.txtVLicensePlate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVLicensePlate.Location = new System.Drawing.Point(170, 55);
            this.txtVLicensePlate.Name = "txtVLicensePlate";
            this.txtVLicensePlate.Size = new System.Drawing.Size(166, 23);
            this.txtVLicensePlate.TabIndex = 1;
            this.txtVLicensePlate.Visible = false;
            // 
            // txtVNumberOfParks
            // 
            this.txtVNumberOfParks.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVNumberOfParks.Location = new System.Drawing.Point(170, 229);
            this.txtVNumberOfParks.Name = "txtVNumberOfParks";
            this.txtVNumberOfParks.Size = new System.Drawing.Size(63, 23);
            this.txtVNumberOfParks.TabIndex = 1;
            this.txtVNumberOfParks.Visible = false;
            // 
            // lblVMake
            // 
            this.lblVMake.AutoSize = true;
            this.lblVMake.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVMake.Location = new System.Drawing.Point(16, 87);
            this.lblVMake.Name = "lblVMake";
            this.lblVMake.Size = new System.Drawing.Size(42, 16);
            this.lblVMake.TabIndex = 0;
            this.lblVMake.Text = "Make";
            this.lblVMake.Visible = false;
            // 
            // lblVNumberOfParks
            // 
            this.lblVNumberOfParks.AutoSize = true;
            this.lblVNumberOfParks.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVNumberOfParks.Location = new System.Drawing.Point(16, 232);
            this.lblVNumberOfParks.Name = "lblVNumberOfParks";
            this.lblVNumberOfParks.Size = new System.Drawing.Size(113, 16);
            this.lblVNumberOfParks.TabIndex = 0;
            this.lblVNumberOfParks.Text = "Number of Parks";
            this.lblVNumberOfParks.Visible = false;
            // 
            // lblVModel
            // 
            this.lblVModel.AutoSize = true;
            this.lblVModel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVModel.Location = new System.Drawing.Point(16, 116);
            this.lblVModel.Name = "lblVModel";
            this.lblVModel.Size = new System.Drawing.Size(46, 16);
            this.lblVModel.TabIndex = 0;
            this.lblVModel.Text = "Model";
            this.lblVModel.Visible = false;
            // 
            // txtVOdometer
            // 
            this.txtVOdometer.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVOdometer.Location = new System.Drawing.Point(170, 200);
            this.txtVOdometer.Name = "txtVOdometer";
            this.txtVOdometer.Size = new System.Drawing.Size(127, 23);
            this.txtVOdometer.TabIndex = 1;
            this.txtVOdometer.Visible = false;
            // 
            // txtVModel
            // 
            this.txtVModel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVModel.Location = new System.Drawing.Point(170, 113);
            this.txtVModel.Name = "txtVModel";
            this.txtVModel.Size = new System.Drawing.Size(166, 23);
            this.txtVModel.TabIndex = 1;
            this.txtVModel.Visible = false;
            // 
            // lblVOdometer
            // 
            this.lblVOdometer.AutoSize = true;
            this.lblVOdometer.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVOdometer.Location = new System.Drawing.Point(16, 203);
            this.lblVOdometer.Name = "lblVOdometer";
            this.lblVOdometer.Size = new System.Drawing.Size(71, 16);
            this.lblVOdometer.TabIndex = 0;
            this.lblVOdometer.Text = "Odometer";
            this.lblVOdometer.Visible = false;
            // 
            // lblVModelYear
            // 
            this.lblVModelYear.AutoSize = true;
            this.lblVModelYear.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVModelYear.Location = new System.Drawing.Point(16, 145);
            this.lblVModelYear.Name = "lblVModelYear";
            this.lblVModelYear.Size = new System.Drawing.Size(79, 16);
            this.lblVModelYear.TabIndex = 0;
            this.lblVModelYear.Text = "Model Year";
            this.lblVModelYear.Visible = false;
            // 
            // txtVColour
            // 
            this.txtVColour.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVColour.Location = new System.Drawing.Point(170, 171);
            this.txtVColour.Name = "txtVColour";
            this.txtVColour.Size = new System.Drawing.Size(127, 23);
            this.txtVColour.TabIndex = 1;
            this.txtVColour.Visible = false;
            // 
            // txtVModelYear
            // 
            this.txtVModelYear.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVModelYear.Location = new System.Drawing.Point(170, 142);
            this.txtVModelYear.Name = "txtVModelYear";
            this.txtVModelYear.Size = new System.Drawing.Size(127, 23);
            this.txtVModelYear.TabIndex = 1;
            this.txtVModelYear.Visible = false;
            // 
            // lblVColour
            // 
            this.lblVColour.AutoSize = true;
            this.lblVColour.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVColour.Location = new System.Drawing.Point(16, 174);
            this.lblVColour.Name = "lblVColour";
            this.lblVColour.Size = new System.Drawing.Size(50, 16);
            this.lblVColour.TabIndex = 0;
            this.lblVColour.Text = "Colour";
            this.lblVColour.Visible = false;
            // 
            // grpVCustomer
            // 
            this.grpVCustomer.Controls.Add(this.txtVProvince);
            this.grpVCustomer.Controls.Add(this.lblVCustomerID);
            this.grpVCustomer.Controls.Add(this.txtVCustomerID);
            this.grpVCustomer.Controls.Add(this.lblVFirstName);
            this.grpVCustomer.Controls.Add(this.txtVFirstName);
            this.grpVCustomer.Controls.Add(this.lblVLastName);
            this.grpVCustomer.Controls.Add(this.txtVLastName);
            this.grpVCustomer.Controls.Add(this.lblVAddress1);
            this.grpVCustomer.Controls.Add(this.txtVAddress1);
            this.grpVCustomer.Controls.Add(this.lblVAddress2);
            this.grpVCustomer.Controls.Add(this.txtVAddress2);
            this.grpVCustomer.Controls.Add(this.lblVCity);
            this.grpVCustomer.Controls.Add(this.txtVCity);
            this.grpVCustomer.Controls.Add(this.lblVProvince);
            this.grpVCustomer.Controls.Add(this.lblVPostalCode);
            this.grpVCustomer.Controls.Add(this.txtVPostalCode);
            this.grpVCustomer.Controls.Add(this.lblVEmail);
            this.grpVCustomer.Controls.Add(this.txtVFax);
            this.grpVCustomer.Controls.Add(this.txtVEmail);
            this.grpVCustomer.Controls.Add(this.lblVFax);
            this.grpVCustomer.Controls.Add(this.lblVHomePhone);
            this.grpVCustomer.Controls.Add(this.txtVCellPhone);
            this.grpVCustomer.Controls.Add(this.txtVHomePhone);
            this.grpVCustomer.Controls.Add(this.lblVCellPhone);
            this.grpVCustomer.Controls.Add(this.lblVWorkPhone);
            this.grpVCustomer.Controls.Add(this.txtVWorkPhone);
            this.grpVCustomer.Location = new System.Drawing.Point(745, 6);
            this.grpVCustomer.Name = "grpVCustomer";
            this.grpVCustomer.Size = new System.Drawing.Size(296, 405);
            this.grpVCustomer.TabIndex = 3;
            this.grpVCustomer.TabStop = false;
            this.grpVCustomer.Text = "Customer";
            this.grpVCustomer.Visible = false;
            // 
            // txtVProvince
            // 
            this.txtVProvince.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVProvince.Location = new System.Drawing.Point(116, 197);
            this.txtVProvince.Name = "txtVProvince";
            this.txtVProvince.Size = new System.Drawing.Size(158, 23);
            this.txtVProvince.TabIndex = 1;
            this.txtVProvince.Visible = false;
            // 
            // lblVCustomerID
            // 
            this.lblVCustomerID.AutoSize = true;
            this.lblVCustomerID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVCustomerID.Location = new System.Drawing.Point(19, 26);
            this.lblVCustomerID.Name = "lblVCustomerID";
            this.lblVCustomerID.Size = new System.Drawing.Size(86, 16);
            this.lblVCustomerID.TabIndex = 0;
            this.lblVCustomerID.Text = "Customer ID";
            this.lblVCustomerID.Visible = false;
            // 
            // txtVCustomerID
            // 
            this.txtVCustomerID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVCustomerID.Location = new System.Drawing.Point(116, 23);
            this.txtVCustomerID.Name = "txtVCustomerID";
            this.txtVCustomerID.Size = new System.Drawing.Size(158, 23);
            this.txtVCustomerID.TabIndex = 1;
            this.txtVCustomerID.Visible = false;
            // 
            // lblVFirstName
            // 
            this.lblVFirstName.AutoSize = true;
            this.lblVFirstName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVFirstName.Location = new System.Drawing.Point(19, 55);
            this.lblVFirstName.Name = "lblVFirstName";
            this.lblVFirstName.Size = new System.Drawing.Size(83, 16);
            this.lblVFirstName.TabIndex = 0;
            this.lblVFirstName.Text = "First Name";
            this.lblVFirstName.Visible = false;
            // 
            // txtVFirstName
            // 
            this.txtVFirstName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVFirstName.Location = new System.Drawing.Point(116, 52);
            this.txtVFirstName.Name = "txtVFirstName";
            this.txtVFirstName.Size = new System.Drawing.Size(158, 23);
            this.txtVFirstName.TabIndex = 1;
            this.txtVFirstName.Visible = false;
            // 
            // lblVLastName
            // 
            this.lblVLastName.AutoSize = true;
            this.lblVLastName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVLastName.Location = new System.Drawing.Point(19, 84);
            this.lblVLastName.Name = "lblVLastName";
            this.lblVLastName.Size = new System.Drawing.Size(80, 16);
            this.lblVLastName.TabIndex = 0;
            this.lblVLastName.Text = "Last Name";
            this.lblVLastName.Visible = false;
            // 
            // txtVLastName
            // 
            this.txtVLastName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVLastName.Location = new System.Drawing.Point(116, 81);
            this.txtVLastName.Name = "txtVLastName";
            this.txtVLastName.Size = new System.Drawing.Size(158, 23);
            this.txtVLastName.TabIndex = 1;
            this.txtVLastName.Visible = false;
            // 
            // lblVAddress1
            // 
            this.lblVAddress1.AutoSize = true;
            this.lblVAddress1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVAddress1.Location = new System.Drawing.Point(19, 113);
            this.lblVAddress1.Name = "lblVAddress1";
            this.lblVAddress1.Size = new System.Drawing.Size(72, 16);
            this.lblVAddress1.TabIndex = 0;
            this.lblVAddress1.Text = "Address 1";
            this.lblVAddress1.Visible = false;
            // 
            // txtVAddress1
            // 
            this.txtVAddress1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVAddress1.Location = new System.Drawing.Point(116, 110);
            this.txtVAddress1.Name = "txtVAddress1";
            this.txtVAddress1.Size = new System.Drawing.Size(158, 23);
            this.txtVAddress1.TabIndex = 1;
            this.txtVAddress1.Visible = false;
            // 
            // lblVAddress2
            // 
            this.lblVAddress2.AutoSize = true;
            this.lblVAddress2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVAddress2.Location = new System.Drawing.Point(19, 142);
            this.lblVAddress2.Name = "lblVAddress2";
            this.lblVAddress2.Size = new System.Drawing.Size(72, 16);
            this.lblVAddress2.TabIndex = 0;
            this.lblVAddress2.Text = "Address 2";
            this.lblVAddress2.Visible = false;
            // 
            // txtVAddress2
            // 
            this.txtVAddress2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVAddress2.Location = new System.Drawing.Point(116, 139);
            this.txtVAddress2.Name = "txtVAddress2";
            this.txtVAddress2.Size = new System.Drawing.Size(158, 23);
            this.txtVAddress2.TabIndex = 1;
            this.txtVAddress2.Visible = false;
            // 
            // lblVCity
            // 
            this.lblVCity.AutoSize = true;
            this.lblVCity.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVCity.Location = new System.Drawing.Point(19, 171);
            this.lblVCity.Name = "lblVCity";
            this.lblVCity.Size = new System.Drawing.Size(32, 16);
            this.lblVCity.TabIndex = 0;
            this.lblVCity.Text = "City";
            this.lblVCity.Visible = false;
            // 
            // txtVCity
            // 
            this.txtVCity.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVCity.Location = new System.Drawing.Point(116, 168);
            this.txtVCity.Name = "txtVCity";
            this.txtVCity.Size = new System.Drawing.Size(158, 23);
            this.txtVCity.TabIndex = 1;
            this.txtVCity.Visible = false;
            // 
            // lblVProvince
            // 
            this.lblVProvince.AutoSize = true;
            this.lblVProvince.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVProvince.Location = new System.Drawing.Point(19, 200);
            this.lblVProvince.Name = "lblVProvince";
            this.lblVProvince.Size = new System.Drawing.Size(63, 16);
            this.lblVProvince.TabIndex = 0;
            this.lblVProvince.Text = "Province";
            this.lblVProvince.Visible = false;
            // 
            // lblVPostalCode
            // 
            this.lblVPostalCode.AutoSize = true;
            this.lblVPostalCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVPostalCode.Location = new System.Drawing.Point(19, 229);
            this.lblVPostalCode.Name = "lblVPostalCode";
            this.lblVPostalCode.Size = new System.Drawing.Size(85, 16);
            this.lblVPostalCode.TabIndex = 0;
            this.lblVPostalCode.Text = "Postal Code";
            this.lblVPostalCode.Visible = false;
            // 
            // txtVPostalCode
            // 
            this.txtVPostalCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVPostalCode.Location = new System.Drawing.Point(116, 226);
            this.txtVPostalCode.Name = "txtVPostalCode";
            this.txtVPostalCode.Size = new System.Drawing.Size(158, 23);
            this.txtVPostalCode.TabIndex = 1;
            this.txtVPostalCode.Visible = false;
            // 
            // lblVEmail
            // 
            this.lblVEmail.AutoSize = true;
            this.lblVEmail.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVEmail.Location = new System.Drawing.Point(19, 258);
            this.lblVEmail.Name = "lblVEmail";
            this.lblVEmail.Size = new System.Drawing.Size(42, 16);
            this.lblVEmail.TabIndex = 0;
            this.lblVEmail.Text = "Email";
            this.lblVEmail.Visible = false;
            // 
            // txtVFax
            // 
            this.txtVFax.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVFax.Location = new System.Drawing.Point(116, 371);
            this.txtVFax.Name = "txtVFax";
            this.txtVFax.Size = new System.Drawing.Size(158, 23);
            this.txtVFax.TabIndex = 1;
            this.txtVFax.Visible = false;
            // 
            // txtVEmail
            // 
            this.txtVEmail.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVEmail.Location = new System.Drawing.Point(116, 255);
            this.txtVEmail.Name = "txtVEmail";
            this.txtVEmail.Size = new System.Drawing.Size(158, 23);
            this.txtVEmail.TabIndex = 1;
            this.txtVEmail.Visible = false;
            // 
            // lblVFax
            // 
            this.lblVFax.AutoSize = true;
            this.lblVFax.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVFax.Location = new System.Drawing.Point(19, 374);
            this.lblVFax.Name = "lblVFax";
            this.lblVFax.Size = new System.Drawing.Size(31, 16);
            this.lblVFax.TabIndex = 0;
            this.lblVFax.Text = "Fax";
            this.lblVFax.Visible = false;
            // 
            // lblVHomePhone
            // 
            this.lblVHomePhone.AutoSize = true;
            this.lblVHomePhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVHomePhone.Location = new System.Drawing.Point(19, 287);
            this.lblVHomePhone.Name = "lblVHomePhone";
            this.lblVHomePhone.Size = new System.Drawing.Size(89, 16);
            this.lblVHomePhone.TabIndex = 0;
            this.lblVHomePhone.Text = "Home Phone";
            this.lblVHomePhone.Visible = false;
            // 
            // txtVCellPhone
            // 
            this.txtVCellPhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVCellPhone.Location = new System.Drawing.Point(116, 342);
            this.txtVCellPhone.Name = "txtVCellPhone";
            this.txtVCellPhone.Size = new System.Drawing.Size(158, 23);
            this.txtVCellPhone.TabIndex = 1;
            this.txtVCellPhone.Visible = false;
            // 
            // txtVHomePhone
            // 
            this.txtVHomePhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVHomePhone.Location = new System.Drawing.Point(116, 284);
            this.txtVHomePhone.Name = "txtVHomePhone";
            this.txtVHomePhone.Size = new System.Drawing.Size(158, 23);
            this.txtVHomePhone.TabIndex = 1;
            this.txtVHomePhone.Visible = false;
            // 
            // lblVCellPhone
            // 
            this.lblVCellPhone.AutoSize = true;
            this.lblVCellPhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVCellPhone.Location = new System.Drawing.Point(19, 345);
            this.lblVCellPhone.Name = "lblVCellPhone";
            this.lblVCellPhone.Size = new System.Drawing.Size(77, 16);
            this.lblVCellPhone.TabIndex = 0;
            this.lblVCellPhone.Text = "Cell Phone";
            this.lblVCellPhone.Visible = false;
            // 
            // lblVWorkPhone
            // 
            this.lblVWorkPhone.AutoSize = true;
            this.lblVWorkPhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVWorkPhone.Location = new System.Drawing.Point(19, 316);
            this.lblVWorkPhone.Name = "lblVWorkPhone";
            this.lblVWorkPhone.Size = new System.Drawing.Size(86, 16);
            this.lblVWorkPhone.TabIndex = 0;
            this.lblVWorkPhone.Text = "Work Phone";
            this.lblVWorkPhone.Visible = false;
            // 
            // txtVWorkPhone
            // 
            this.txtVWorkPhone.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVWorkPhone.Location = new System.Drawing.Point(116, 313);
            this.txtVWorkPhone.Name = "txtVWorkPhone";
            this.txtVWorkPhone.Size = new System.Drawing.Size(158, 23);
            this.txtVWorkPhone.TabIndex = 1;
            this.txtVWorkPhone.Visible = false;
            // 
            // grpVTicket
            // 
            this.grpVTicket.Controls.Add(this.txtVCloseDate);
            this.grpVTicket.Controls.Add(this.txtVOpenDate);
            this.grpVTicket.Controls.Add(this.txtVTicketID);
            this.grpVTicket.Controls.Add(this.lblVTicketID);
            this.grpVTicket.Controls.Add(this.lblVTicketNumber);
            this.grpVTicket.Controls.Add(this.txtVBarCode);
            this.grpVTicket.Controls.Add(this.txtVTicketNumber);
            this.grpVTicket.Controls.Add(this.lblVDateClose);
            this.grpVTicket.Controls.Add(this.lblVIssueNumber);
            this.grpVTicket.Controls.Add(this.lblVDateOpen);
            this.grpVTicket.Controls.Add(this.txtVIssueNumber);
            this.grpVTicket.Controls.Add(this.lblVBarCode);
            this.grpVTicket.Controls.Add(this.lblVPinNumber);
            this.grpVTicket.Controls.Add(this.txtVPinNumber);
            this.grpVTicket.Location = new System.Drawing.Point(19, 6);
            this.grpVTicket.Name = "grpVTicket";
            this.grpVTicket.Size = new System.Drawing.Size(356, 245);
            this.grpVTicket.TabIndex = 35;
            this.grpVTicket.TabStop = false;
            this.grpVTicket.Text = "Ticket";
            this.grpVTicket.Visible = false;
            // 
            // txtVCloseDate
            // 
            this.txtVCloseDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVCloseDate.Location = new System.Drawing.Point(151, 200);
            this.txtVCloseDate.Name = "txtVCloseDate";
            this.txtVCloseDate.Size = new System.Drawing.Size(184, 23);
            this.txtVCloseDate.TabIndex = 26;
            this.txtVCloseDate.Visible = false;
            // 
            // txtVOpenDate
            // 
            this.txtVOpenDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVOpenDate.Location = new System.Drawing.Point(151, 173);
            this.txtVOpenDate.Name = "txtVOpenDate";
            this.txtVOpenDate.Size = new System.Drawing.Size(184, 23);
            this.txtVOpenDate.TabIndex = 26;
            this.txtVOpenDate.Visible = false;
            // 
            // txtVTicketID
            // 
            this.txtVTicketID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVTicketID.Location = new System.Drawing.Point(151, 26);
            this.txtVTicketID.Name = "txtVTicketID";
            this.txtVTicketID.Size = new System.Drawing.Size(184, 23);
            this.txtVTicketID.TabIndex = 30;
            this.txtVTicketID.Visible = false;
            // 
            // lblVTicketID
            // 
            this.lblVTicketID.AutoSize = true;
            this.lblVTicketID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVTicketID.Location = new System.Drawing.Point(28, 29);
            this.lblVTicketID.Name = "lblVTicketID";
            this.lblVTicketID.Size = new System.Drawing.Size(68, 16);
            this.lblVTicketID.TabIndex = 25;
            this.lblVTicketID.Text = "Ticket ID";
            this.lblVTicketID.Visible = false;
            // 
            // lblVTicketNumber
            // 
            this.lblVTicketNumber.AutoSize = true;
            this.lblVTicketNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVTicketNumber.Location = new System.Drawing.Point(28, 58);
            this.lblVTicketNumber.Name = "lblVTicketNumber";
            this.lblVTicketNumber.Size = new System.Drawing.Size(98, 16);
            this.lblVTicketNumber.TabIndex = 24;
            this.lblVTicketNumber.Text = "Ticket Number";
            this.lblVTicketNumber.Visible = false;
            // 
            // txtVBarCode
            // 
            this.txtVBarCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVBarCode.Location = new System.Drawing.Point(152, 142);
            this.txtVBarCode.Name = "txtVBarCode";
            this.txtVBarCode.Size = new System.Drawing.Size(184, 23);
            this.txtVBarCode.TabIndex = 26;
            this.txtVBarCode.Visible = false;
            // 
            // txtVTicketNumber
            // 
            this.txtVTicketNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVTicketNumber.Location = new System.Drawing.Point(152, 55);
            this.txtVTicketNumber.Name = "txtVTicketNumber";
            this.txtVTicketNumber.Size = new System.Drawing.Size(184, 23);
            this.txtVTicketNumber.TabIndex = 29;
            this.txtVTicketNumber.Visible = false;
            // 
            // lblVDateClose
            // 
            this.lblVDateClose.AutoSize = true;
            this.lblVDateClose.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVDateClose.Location = new System.Drawing.Point(27, 205);
            this.lblVDateClose.Name = "lblVDateClose";
            this.lblVDateClose.Size = new System.Drawing.Size(119, 16);
            this.lblVDateClose.TabIndex = 31;
            this.lblVDateClose.Text = "Ticket Close Date";
            this.lblVDateClose.Visible = false;
            // 
            // lblVIssueNumber
            // 
            this.lblVIssueNumber.AutoSize = true;
            this.lblVIssueNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVIssueNumber.Location = new System.Drawing.Point(28, 87);
            this.lblVIssueNumber.Name = "lblVIssueNumber";
            this.lblVIssueNumber.Size = new System.Drawing.Size(94, 16);
            this.lblVIssueNumber.TabIndex = 23;
            this.lblVIssueNumber.Text = "Issue Number";
            this.lblVIssueNumber.Visible = false;
            // 
            // lblVDateOpen
            // 
            this.lblVDateOpen.AutoSize = true;
            this.lblVDateOpen.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVDateOpen.Location = new System.Drawing.Point(28, 176);
            this.lblVDateOpen.Name = "lblVDateOpen";
            this.lblVDateOpen.Size = new System.Drawing.Size(118, 16);
            this.lblVDateOpen.TabIndex = 32;
            this.lblVDateOpen.Text = "Ticket Open Date";
            this.lblVDateOpen.Visible = false;
            // 
            // txtVIssueNumber
            // 
            this.txtVIssueNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVIssueNumber.Location = new System.Drawing.Point(152, 84);
            this.txtVIssueNumber.Name = "txtVIssueNumber";
            this.txtVIssueNumber.Size = new System.Drawing.Size(184, 23);
            this.txtVIssueNumber.TabIndex = 28;
            this.txtVIssueNumber.Visible = false;
            // 
            // lblVBarCode
            // 
            this.lblVBarCode.AutoSize = true;
            this.lblVBarCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVBarCode.Location = new System.Drawing.Point(28, 145);
            this.lblVBarCode.Name = "lblVBarCode";
            this.lblVBarCode.Size = new System.Drawing.Size(68, 16);
            this.lblVBarCode.TabIndex = 21;
            this.lblVBarCode.Text = "Bar Code";
            this.lblVBarCode.Visible = false;
            // 
            // lblVPinNumber
            // 
            this.lblVPinNumber.AutoSize = true;
            this.lblVPinNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVPinNumber.Location = new System.Drawing.Point(28, 116);
            this.lblVPinNumber.Name = "lblVPinNumber";
            this.lblVPinNumber.Size = new System.Drawing.Size(81, 16);
            this.lblVPinNumber.TabIndex = 22;
            this.lblVPinNumber.Text = "Pin Number";
            this.lblVPinNumber.Visible = false;
            // 
            // txtVPinNumber
            // 
            this.txtVPinNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVPinNumber.Location = new System.Drawing.Point(152, 113);
            this.txtVPinNumber.Name = "txtVPinNumber";
            this.txtVPinNumber.Size = new System.Drawing.Size(184, 23);
            this.txtVPinNumber.TabIndex = 27;
            this.txtVPinNumber.Visible = false;
            // 
            // tabAttachments
            // 
            this.tabAttachments.BackColor = System.Drawing.Color.PaleGreen;
            this.tabAttachments.Controls.Add(this.axAcroPDF1);
            this.tabAttachments.Controls.Add(this.btnDeletePicture);
            this.tabAttachments.Controls.Add(this.btnSaveAs);
            this.tabAttachments.Controls.Add(this.btnAddPicture);
            this.tabAttachments.Controls.Add(this.pictureBox1);
            this.tabAttachments.Controls.Add(this.lstPictures);
            this.tabAttachments.ForeColor = System.Drawing.Color.DarkGreen;
            this.tabAttachments.Location = new System.Drawing.Point(4, 26);
            this.tabAttachments.Name = "tabAttachments";
            this.tabAttachments.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttachments.Size = new System.Drawing.Size(1049, 564);
            this.tabAttachments.TabIndex = 5;
            this.tabAttachments.Text = "Attachments";
            // 
            // axAcroPDF1
            // 
            this.axAcroPDF1.Enabled = true;
            this.axAcroPDF1.Location = new System.Drawing.Point(445, 25);
            this.axAcroPDF1.Name = "axAcroPDF1";
            this.axAcroPDF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF1.OcxState")));
            this.axAcroPDF1.Size = new System.Drawing.Size(589, 531);
            this.axAcroPDF1.TabIndex = 3;
            this.axAcroPDF1.Visible = false;
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletePicture.Location = new System.Drawing.Point(135, 476);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(96, 37);
            this.btnDeletePicture.TabIndex = 2;
            this.btnDeletePicture.Text = "Delete";
            this.btnDeletePicture.UseVisualStyleBackColor = true;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveAs.Location = new System.Drawing.Point(237, 476);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(99, 37);
            this.btnSaveAs.TabIndex = 2;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnAddPicture
            // 
            this.btnAddPicture.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPicture.Location = new System.Drawing.Point(30, 476);
            this.btnAddPicture.Name = "btnAddPicture";
            this.btnAddPicture.Size = new System.Drawing.Size(99, 37);
            this.btnAddPicture.TabIndex = 2;
            this.btnAddPicture.Text = "Add";
            this.btnAddPicture.UseVisualStyleBackColor = true;
            this.btnAddPicture.Click += new System.EventHandler(this.btnAddPicture_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(377, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(657, 531);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lstPictures
            // 
            this.lstPictures.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstPictures.FormattingEnabled = true;
            this.lstPictures.ItemHeight = 16;
            this.lstPictures.Location = new System.Drawing.Point(28, 25);
            this.lstPictures.Name = "lstPictures";
            this.lstPictures.Size = new System.Drawing.Size(301, 388);
            this.lstPictures.TabIndex = 0;
            this.lstPictures.SelectedIndexChanged += new System.EventHandler(this.lstPictures_SelectedIndexChanged);
            // 
            // tabDocuments
            // 
            this.tabDocuments.BackColor = System.Drawing.Color.PaleGreen;
            this.tabDocuments.Controls.Add(this.btnChequeRequisition);
            this.tabDocuments.Controls.Add(this.btnFinalReleaseRepairFrench);
            this.tabDocuments.Controls.Add(this.btnFinalReleaseVoucherFrench);
            this.tabDocuments.Controls.Add(this.btnFinalReleaseFrench);
            this.tabDocuments.Controls.Add(this.btnCustomerCopyFrench);
            this.tabDocuments.Controls.Add(this.reportViewer1);
            this.tabDocuments.Controls.Add(this.btnOccurrencesOverdue);
            this.tabDocuments.Controls.Add(this.btnFinalReleaseRepair);
            this.tabDocuments.Controls.Add(this.btnFinalReleaseVoucher);
            this.tabDocuments.Controls.Add(this.btnFinalRelease);
            this.tabDocuments.Controls.Add(this.btnInternalCopy);
            this.tabDocuments.Controls.Add(this.btnCustomerCopy);
            this.tabDocuments.ForeColor = System.Drawing.Color.DarkGreen;
            this.tabDocuments.Location = new System.Drawing.Point(4, 26);
            this.tabDocuments.Name = "tabDocuments";
            this.tabDocuments.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocuments.Size = new System.Drawing.Size(1049, 564);
            this.tabDocuments.TabIndex = 4;
            this.tabDocuments.Text = "Documents";
            // 
            // btnFinalReleaseRepairFrench
            // 
            this.btnFinalReleaseRepairFrench.Location = new System.Drawing.Point(12, 403);
            this.btnFinalReleaseRepairFrench.Name = "btnFinalReleaseRepairFrench";
            this.btnFinalReleaseRepairFrench.Size = new System.Drawing.Size(221, 40);
            this.btnFinalReleaseRepairFrench.TabIndex = 5;
            this.btnFinalReleaseRepairFrench.Text = "Final Release Repair French";
            this.btnFinalReleaseRepairFrench.UseVisualStyleBackColor = true;
            this.btnFinalReleaseRepairFrench.Click += new System.EventHandler(this.btnFinalReleaseRepairFrench_Click);
            // 
            // btnFinalReleaseVoucherFrench
            // 
            this.btnFinalReleaseVoucherFrench.Location = new System.Drawing.Point(12, 311);
            this.btnFinalReleaseVoucherFrench.Name = "btnFinalReleaseVoucherFrench";
            this.btnFinalReleaseVoucherFrench.Size = new System.Drawing.Size(221, 40);
            this.btnFinalReleaseVoucherFrench.TabIndex = 4;
            this.btnFinalReleaseVoucherFrench.Text = "Final Release Voucher French";
            this.btnFinalReleaseVoucherFrench.UseVisualStyleBackColor = true;
            this.btnFinalReleaseVoucherFrench.Click += new System.EventHandler(this.btnFinalReleaseVoucherFrench_Click);
            // 
            // btnFinalReleaseFrench
            // 
            this.btnFinalReleaseFrench.Location = new System.Drawing.Point(12, 219);
            this.btnFinalReleaseFrench.Name = "btnFinalReleaseFrench";
            this.btnFinalReleaseFrench.Size = new System.Drawing.Size(221, 40);
            this.btnFinalReleaseFrench.TabIndex = 3;
            this.btnFinalReleaseFrench.Text = "Final Release Cheque French";
            this.btnFinalReleaseFrench.UseVisualStyleBackColor = true;
            this.btnFinalReleaseFrench.Click += new System.EventHandler(this.btnFinalReleaseFrench_Click);
            // 
            // btnCustomerCopyFrench
            // 
            this.btnCustomerCopyFrench.Location = new System.Drawing.Point(12, 81);
            this.btnCustomerCopyFrench.Name = "btnCustomerCopyFrench";
            this.btnCustomerCopyFrench.Size = new System.Drawing.Size(221, 40);
            this.btnCustomerCopyFrench.TabIndex = 2;
            this.btnCustomerCopyFrench.Text = "Customer Copy French";
            this.btnCustomerCopyFrench.UseVisualStyleBackColor = true;
            this.btnCustomerCopyFrench.Click += new System.EventHandler(this.btnCustomerCopyFrench_Click);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Location = new System.Drawing.Point(249, 35);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(794, 508);
            this.reportViewer1.TabIndex = 1;
            // 
            // btnOccurrencesOverdue
            // 
            this.btnOccurrencesOverdue.Location = new System.Drawing.Point(12, 449);
            this.btnOccurrencesOverdue.Name = "btnOccurrencesOverdue";
            this.btnOccurrencesOverdue.Size = new System.Drawing.Size(221, 40);
            this.btnOccurrencesOverdue.TabIndex = 0;
            this.btnOccurrencesOverdue.Text = "Occurrences Overdue";
            this.btnOccurrencesOverdue.UseVisualStyleBackColor = true;
            this.btnOccurrencesOverdue.Click += new System.EventHandler(this.btnOccurrencesOverdue_Click);
            // 
            // btnFinalReleaseRepair
            // 
            this.btnFinalReleaseRepair.Location = new System.Drawing.Point(12, 357);
            this.btnFinalReleaseRepair.Name = "btnFinalReleaseRepair";
            this.btnFinalReleaseRepair.Size = new System.Drawing.Size(221, 40);
            this.btnFinalReleaseRepair.TabIndex = 0;
            this.btnFinalReleaseRepair.Text = "Final Release Repair";
            this.btnFinalReleaseRepair.UseVisualStyleBackColor = true;
            this.btnFinalReleaseRepair.Click += new System.EventHandler(this.btnFinalReleaseRepair_Click);
            // 
            // btnFinalReleaseVoucher
            // 
            this.btnFinalReleaseVoucher.Location = new System.Drawing.Point(12, 265);
            this.btnFinalReleaseVoucher.Name = "btnFinalReleaseVoucher";
            this.btnFinalReleaseVoucher.Size = new System.Drawing.Size(221, 40);
            this.btnFinalReleaseVoucher.TabIndex = 0;
            this.btnFinalReleaseVoucher.Text = "Final Release Voucher";
            this.btnFinalReleaseVoucher.UseVisualStyleBackColor = true;
            this.btnFinalReleaseVoucher.Click += new System.EventHandler(this.btnFinalReleaseVoucher_Click);
            // 
            // btnFinalRelease
            // 
            this.btnFinalRelease.Location = new System.Drawing.Point(12, 173);
            this.btnFinalRelease.Name = "btnFinalRelease";
            this.btnFinalRelease.Size = new System.Drawing.Size(221, 40);
            this.btnFinalRelease.TabIndex = 0;
            this.btnFinalRelease.Text = "Final Release Cheque";
            this.btnFinalRelease.UseVisualStyleBackColor = true;
            this.btnFinalRelease.Click += new System.EventHandler(this.btnFinalRelease_Click);
            // 
            // btnInternalCopy
            // 
            this.btnInternalCopy.Location = new System.Drawing.Point(12, 127);
            this.btnInternalCopy.Name = "btnInternalCopy";
            this.btnInternalCopy.Size = new System.Drawing.Size(221, 40);
            this.btnInternalCopy.TabIndex = 0;
            this.btnInternalCopy.Text = "Internal Copy";
            this.btnInternalCopy.UseVisualStyleBackColor = true;
            this.btnInternalCopy.Click += new System.EventHandler(this.btnInternalCopy_Click);
            // 
            // btnCustomerCopy
            // 
            this.btnCustomerCopy.Location = new System.Drawing.Point(12, 35);
            this.btnCustomerCopy.Name = "btnCustomerCopy";
            this.btnCustomerCopy.Size = new System.Drawing.Size(221, 40);
            this.btnCustomerCopy.TabIndex = 0;
            this.btnCustomerCopy.Text = "Customer Copy";
            this.btnCustomerCopy.UseVisualStyleBackColor = true;
            this.btnCustomerCopy.Click += new System.EventHandler(this.btnCustomerCopy_Click);
            // 
            // tabRisk
            // 
            this.tabRisk.BackColor = System.Drawing.Color.PaleGreen;
            this.tabRisk.Controls.Add(this.btnRSendC);
            this.tabRisk.Controls.Add(this.btnRSendB);
            this.tabRisk.Controls.Add(this.btnRAddRecord);
            this.tabRisk.Controls.Add(this.picLine10);
            this.tabRisk.Controls.Add(this.picLine9);
            this.tabRisk.Controls.Add(this.picLine8);
            this.tabRisk.Controls.Add(this.picLine7);
            this.tabRisk.Controls.Add(this.picLine6);
            this.tabRisk.Controls.Add(this.picLine5);
            this.tabRisk.Controls.Add(this.picLine4);
            this.tabRisk.Controls.Add(this.picLine3);
            this.tabRisk.Controls.Add(this.picLine1);
            this.tabRisk.Controls.Add(this.picLine2);
            this.tabRisk.Controls.Add(this.dtpRJudgementDate);
            this.tabRisk.Controls.Add(this.dtpRTrialDate);
            this.tabRisk.Controls.Add(this.lblRJudgementDate);
            this.tabRisk.Controls.Add(this.dtpRPreTrialDate);
            this.tabRisk.Controls.Add(this.lblRTrialDate);
            this.tabRisk.Controls.Add(this.dtpRDefenceDate);
            this.tabRisk.Controls.Add(this.lblRPreTrialDate);
            this.tabRisk.Controls.Add(this.dtpRSOCDate);
            this.tabRisk.Controls.Add(this.lblRDefenceDate);
            this.tabRisk.Controls.Add(this.chkRJudgement);
            this.tabRisk.Controls.Add(this.dtpRLetterDate);
            this.tabRisk.Controls.Add(this.chkRTrial);
            this.tabRisk.Controls.Add(this.lblRSOCDate);
            this.tabRisk.Controls.Add(this.chkRPreTrial);
            this.tabRisk.Controls.Add(this.lblRSmallClaim);
            this.tabRisk.Controls.Add(this.chkRDefence);
            this.tabRisk.Controls.Add(this.lblRLetterDate);
            this.tabRisk.Controls.Add(this.chkRSOC);
            this.tabRisk.Controls.Add(this.dtpRResolvedCDate);
            this.tabRisk.Controls.Add(this.chkRLetter);
            this.tabRisk.Controls.Add(this.lblRResolvedCDate);
            this.tabRisk.Controls.Add(this.dtpRResolvedICDate);
            this.tabRisk.Controls.Add(this.chkRResolvedC);
            this.tabRisk.Controls.Add(this.lblRResolvedICDate);
            this.tabRisk.Controls.Add(this.txtRAdjuster);
            this.tabRisk.Controls.Add(this.chkRResolvedIC);
            this.tabRisk.Controls.Add(this.dtpRReportedDate);
            this.tabRisk.Controls.Add(this.txtRClaim);
            this.tabRisk.Controls.Add(this.lblRReportedDate);
            this.tabRisk.Controls.Add(this.lblRAdjuster);
            this.tabRisk.Controls.Add(this.dtpRAssignedDate);
            this.tabRisk.Controls.Add(this.txtRNotes);
            this.tabRisk.Controls.Add(this.lblRClaim);
            this.tabRisk.Controls.Add(this.chkRReported);
            this.tabRisk.Controls.Add(this.lblRAssignedDate);
            this.tabRisk.Controls.Add(this.lblRNotes);
            this.tabRisk.Controls.Add(this.chkRAssigned);
            this.tabRisk.ForeColor = System.Drawing.Color.DarkGreen;
            this.tabRisk.Location = new System.Drawing.Point(4, 26);
            this.tabRisk.Name = "tabRisk";
            this.tabRisk.Padding = new System.Windows.Forms.Padding(3);
            this.tabRisk.Size = new System.Drawing.Size(1049, 564);
            this.tabRisk.TabIndex = 6;
            this.tabRisk.Text = "Risk Assessment";
            // 
            // btnRSendC
            // 
            this.btnRSendC.Location = new System.Drawing.Point(510, 13);
            this.btnRSendC.Name = "btnRSendC";
            this.btnRSendC.Size = new System.Drawing.Size(224, 60);
            this.btnRSendC.TabIndex = 40;
            this.btnRSendC.Text = "Send To Insurance Company";
            this.btnRSendC.UseVisualStyleBackColor = true;
            this.btnRSendC.Click += new System.EventHandler(this.btnRSendC_Click);
            // 
            // btnRSendB
            // 
            this.btnRSendB.Location = new System.Drawing.Point(298, 13);
            this.btnRSendB.Name = "btnRSendB";
            this.btnRSendB.Size = new System.Drawing.Size(195, 60);
            this.btnRSendB.TabIndex = 39;
            this.btnRSendB.Text = "Send To Insurance Broker";
            this.btnRSendB.UseVisualStyleBackColor = true;
            this.btnRSendB.Click += new System.EventHandler(this.btnRSendB_Click);
            // 
            // btnRAddRecord
            // 
            this.btnRAddRecord.Location = new System.Drawing.Point(102, 13);
            this.btnRAddRecord.Name = "btnRAddRecord";
            this.btnRAddRecord.Size = new System.Drawing.Size(177, 60);
            this.btnRAddRecord.TabIndex = 38;
            this.btnRAddRecord.Text = "Add History Record";
            this.btnRAddRecord.UseVisualStyleBackColor = true;
            this.btnRAddRecord.Click += new System.EventHandler(this.btnRAddRecord_Click);
            // 
            // picLine10
            // 
            this.picLine10.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine10.Location = new System.Drawing.Point(270, 542);
            this.picLine10.Name = "picLine10";
            this.picLine10.Size = new System.Drawing.Size(500, 2);
            this.picLine10.TabIndex = 37;
            this.picLine10.TabStop = false;
            this.picLine10.Visible = false;
            // 
            // picLine9
            // 
            this.picLine9.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine9.Location = new System.Drawing.Point(270, 503);
            this.picLine9.Name = "picLine9";
            this.picLine9.Size = new System.Drawing.Size(500, 2);
            this.picLine9.TabIndex = 37;
            this.picLine9.TabStop = false;
            this.picLine9.Visible = false;
            // 
            // picLine8
            // 
            this.picLine8.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine8.Location = new System.Drawing.Point(270, 463);
            this.picLine8.Name = "picLine8";
            this.picLine8.Size = new System.Drawing.Size(500, 2);
            this.picLine8.TabIndex = 37;
            this.picLine8.TabStop = false;
            this.picLine8.Visible = false;
            // 
            // picLine7
            // 
            this.picLine7.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine7.Location = new System.Drawing.Point(270, 424);
            this.picLine7.Name = "picLine7";
            this.picLine7.Size = new System.Drawing.Size(500, 2);
            this.picLine7.TabIndex = 37;
            this.picLine7.TabStop = false;
            this.picLine7.Visible = false;
            // 
            // picLine6
            // 
            this.picLine6.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine6.Location = new System.Drawing.Point(270, 385);
            this.picLine6.Name = "picLine6";
            this.picLine6.Size = new System.Drawing.Size(500, 2);
            this.picLine6.TabIndex = 37;
            this.picLine6.TabStop = false;
            this.picLine6.Visible = false;
            // 
            // picLine5
            // 
            this.picLine5.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine5.Location = new System.Drawing.Point(270, 347);
            this.picLine5.Name = "picLine5";
            this.picLine5.Size = new System.Drawing.Size(500, 2);
            this.picLine5.TabIndex = 37;
            this.picLine5.TabStop = false;
            this.picLine5.Visible = false;
            // 
            // picLine4
            // 
            this.picLine4.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine4.Location = new System.Drawing.Point(29, 306);
            this.picLine4.Name = "picLine4";
            this.picLine4.Size = new System.Drawing.Size(994, 2);
            this.picLine4.TabIndex = 37;
            this.picLine4.TabStop = false;
            this.picLine4.Visible = false;
            // 
            // picLine3
            // 
            this.picLine3.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine3.Location = new System.Drawing.Point(29, 226);
            this.picLine3.Name = "picLine3";
            this.picLine3.Size = new System.Drawing.Size(994, 2);
            this.picLine3.TabIndex = 37;
            this.picLine3.TabStop = false;
            this.picLine3.Visible = false;
            // 
            // picLine1
            // 
            this.picLine1.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine1.Location = new System.Drawing.Point(29, 107);
            this.picLine1.Name = "picLine1";
            this.picLine1.Size = new System.Drawing.Size(994, 2);
            this.picLine1.TabIndex = 37;
            this.picLine1.TabStop = false;
            this.picLine1.Visible = false;
            // 
            // picLine2
            // 
            this.picLine2.BackColor = System.Drawing.Color.DarkGreen;
            this.picLine2.Location = new System.Drawing.Point(29, 146);
            this.picLine2.Name = "picLine2";
            this.picLine2.Size = new System.Drawing.Size(994, 2);
            this.picLine2.TabIndex = 37;
            this.picLine2.TabStop = false;
            this.picLine2.Visible = false;
            // 
            // dtpRJudgementDate
            // 
            this.dtpRJudgementDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRJudgementDate.Location = new System.Drawing.Point(498, 511);
            this.dtpRJudgementDate.Name = "dtpRJudgementDate";
            this.dtpRJudgementDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRJudgementDate.TabIndex = 35;
            this.dtpRJudgementDate.Visible = false;
            // 
            // dtpRTrialDate
            // 
            this.dtpRTrialDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRTrialDate.Location = new System.Drawing.Point(499, 471);
            this.dtpRTrialDate.Name = "dtpRTrialDate";
            this.dtpRTrialDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRTrialDate.TabIndex = 35;
            this.dtpRTrialDate.Visible = false;
            // 
            // lblRJudgementDate
            // 
            this.lblRJudgementDate.AutoSize = true;
            this.lblRJudgementDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRJudgementDate.Location = new System.Drawing.Point(454, 518);
            this.lblRJudgementDate.Name = "lblRJudgementDate";
            this.lblRJudgementDate.Size = new System.Drawing.Size(38, 16);
            this.lblRJudgementDate.TabIndex = 30;
            this.lblRJudgementDate.Text = "Date";
            this.lblRJudgementDate.Visible = false;
            // 
            // dtpRPreTrialDate
            // 
            this.dtpRPreTrialDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRPreTrialDate.Location = new System.Drawing.Point(499, 432);
            this.dtpRPreTrialDate.Name = "dtpRPreTrialDate";
            this.dtpRPreTrialDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRPreTrialDate.TabIndex = 35;
            this.dtpRPreTrialDate.Visible = false;
            // 
            // lblRTrialDate
            // 
            this.lblRTrialDate.AutoSize = true;
            this.lblRTrialDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRTrialDate.Location = new System.Drawing.Point(455, 478);
            this.lblRTrialDate.Name = "lblRTrialDate";
            this.lblRTrialDate.Size = new System.Drawing.Size(38, 16);
            this.lblRTrialDate.TabIndex = 30;
            this.lblRTrialDate.Text = "Date";
            this.lblRTrialDate.Visible = false;
            // 
            // dtpRDefenceDate
            // 
            this.dtpRDefenceDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRDefenceDate.Location = new System.Drawing.Point(498, 393);
            this.dtpRDefenceDate.Name = "dtpRDefenceDate";
            this.dtpRDefenceDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRDefenceDate.TabIndex = 35;
            this.dtpRDefenceDate.Visible = false;
            // 
            // lblRPreTrialDate
            // 
            this.lblRPreTrialDate.AutoSize = true;
            this.lblRPreTrialDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRPreTrialDate.Location = new System.Drawing.Point(455, 439);
            this.lblRPreTrialDate.Name = "lblRPreTrialDate";
            this.lblRPreTrialDate.Size = new System.Drawing.Size(38, 16);
            this.lblRPreTrialDate.TabIndex = 30;
            this.lblRPreTrialDate.Text = "Date";
            this.lblRPreTrialDate.Visible = false;
            // 
            // dtpRSOCDate
            // 
            this.dtpRSOCDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRSOCDate.Location = new System.Drawing.Point(498, 353);
            this.dtpRSOCDate.Name = "dtpRSOCDate";
            this.dtpRSOCDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRSOCDate.TabIndex = 35;
            this.dtpRSOCDate.Visible = false;
            // 
            // lblRDefenceDate
            // 
            this.lblRDefenceDate.AutoSize = true;
            this.lblRDefenceDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRDefenceDate.Location = new System.Drawing.Point(454, 400);
            this.lblRDefenceDate.Name = "lblRDefenceDate";
            this.lblRDefenceDate.Size = new System.Drawing.Size(38, 16);
            this.lblRDefenceDate.TabIndex = 30;
            this.lblRDefenceDate.Text = "Date";
            this.lblRDefenceDate.Visible = false;
            // 
            // chkRJudgement
            // 
            this.chkRJudgement.AutoSize = true;
            this.chkRJudgement.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRJudgement.Location = new System.Drawing.Point(270, 517);
            this.chkRJudgement.Name = "chkRJudgement";
            this.chkRJudgement.Size = new System.Drawing.Size(96, 20);
            this.chkRJudgement.TabIndex = 32;
            this.chkRJudgement.Text = "Judgement";
            this.chkRJudgement.UseVisualStyleBackColor = true;
            this.chkRJudgement.Visible = false;
            this.chkRJudgement.CheckedChanged += new System.EventHandler(this.chkRJudgement_CheckedChanged);
            // 
            // dtpRLetterDate
            // 
            this.dtpRLetterDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRLetterDate.Location = new System.Drawing.Point(498, 274);
            this.dtpRLetterDate.Name = "dtpRLetterDate";
            this.dtpRLetterDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRLetterDate.TabIndex = 35;
            this.dtpRLetterDate.Visible = false;
            // 
            // chkRTrial
            // 
            this.chkRTrial.AutoSize = true;
            this.chkRTrial.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRTrial.Location = new System.Drawing.Point(271, 477);
            this.chkRTrial.Name = "chkRTrial";
            this.chkRTrial.Size = new System.Drawing.Size(54, 20);
            this.chkRTrial.TabIndex = 32;
            this.chkRTrial.Text = "Trial";
            this.chkRTrial.UseVisualStyleBackColor = true;
            this.chkRTrial.Visible = false;
            this.chkRTrial.CheckedChanged += new System.EventHandler(this.chkRTrial_CheckedChanged);
            // 
            // lblRSOCDate
            // 
            this.lblRSOCDate.AutoSize = true;
            this.lblRSOCDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRSOCDate.Location = new System.Drawing.Point(454, 360);
            this.lblRSOCDate.Name = "lblRSOCDate";
            this.lblRSOCDate.Size = new System.Drawing.Size(38, 16);
            this.lblRSOCDate.TabIndex = 30;
            this.lblRSOCDate.Text = "Date";
            this.lblRSOCDate.Visible = false;
            // 
            // chkRPreTrial
            // 
            this.chkRPreTrial.AutoSize = true;
            this.chkRPreTrial.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRPreTrial.Location = new System.Drawing.Point(271, 438);
            this.chkRPreTrial.Name = "chkRPreTrial";
            this.chkRPreTrial.Size = new System.Drawing.Size(80, 20);
            this.chkRPreTrial.TabIndex = 32;
            this.chkRPreTrial.Text = "Pre Trial";
            this.chkRPreTrial.UseVisualStyleBackColor = true;
            this.chkRPreTrial.Visible = false;
            this.chkRPreTrial.CheckedChanged += new System.EventHandler(this.chkRPreTrial_CheckedChanged);
            // 
            // lblRSmallClaim
            // 
            this.lblRSmallClaim.AutoSize = true;
            this.lblRSmallClaim.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRSmallClaim.Location = new System.Drawing.Point(266, 325);
            this.lblRSmallClaim.Name = "lblRSmallClaim";
            this.lblRSmallClaim.Size = new System.Drawing.Size(206, 19);
            this.lblRSmallClaim.TabIndex = 30;
            this.lblRSmallClaim.Text = "Small Claim Court Actions";
            this.lblRSmallClaim.Visible = false;
            // 
            // chkRDefence
            // 
            this.chkRDefence.AutoSize = true;
            this.chkRDefence.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRDefence.Location = new System.Drawing.Point(270, 399);
            this.chkRDefence.Name = "chkRDefence";
            this.chkRDefence.Size = new System.Drawing.Size(115, 20);
            this.chkRDefence.TabIndex = 32;
            this.chkRDefence.Text = "Defence Filed";
            this.chkRDefence.UseVisualStyleBackColor = true;
            this.chkRDefence.Visible = false;
            this.chkRDefence.CheckedChanged += new System.EventHandler(this.chkRDefence_CheckedChanged);
            // 
            // lblRLetterDate
            // 
            this.lblRLetterDate.AutoSize = true;
            this.lblRLetterDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRLetterDate.Location = new System.Drawing.Point(391, 281);
            this.lblRLetterDate.Name = "lblRLetterDate";
            this.lblRLetterDate.Size = new System.Drawing.Size(71, 16);
            this.lblRLetterDate.TabIndex = 30;
            this.lblRLetterDate.Text = "Sent Date";
            this.lblRLetterDate.Visible = false;
            // 
            // chkRSOC
            // 
            this.chkRSOC.AutoSize = true;
            this.chkRSOC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRSOC.Location = new System.Drawing.Point(270, 359);
            this.chkRSOC.Name = "chkRSOC";
            this.chkRSOC.Size = new System.Drawing.Size(115, 20);
            this.chkRSOC.TabIndex = 32;
            this.chkRSOC.Text = "SOC received";
            this.chkRSOC.UseVisualStyleBackColor = true;
            this.chkRSOC.Visible = false;
            this.chkRSOC.CheckedChanged += new System.EventHandler(this.chkRSOC_CheckedChanged);
            // 
            // dtpRResolvedCDate
            // 
            this.dtpRResolvedCDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRResolvedCDate.Location = new System.Drawing.Point(499, 234);
            this.dtpRResolvedCDate.Name = "dtpRResolvedCDate";
            this.dtpRResolvedCDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRResolvedCDate.TabIndex = 35;
            this.dtpRResolvedCDate.Visible = false;
            // 
            // chkRLetter
            // 
            this.chkRLetter.AutoSize = true;
            this.chkRLetter.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRLetter.Location = new System.Drawing.Point(29, 280);
            this.chkRLetter.Name = "chkRLetter";
            this.chkRLetter.Size = new System.Drawing.Size(141, 20);
            this.chkRLetter.TabIndex = 32;
            this.chkRLetter.Text = "Denial Letter Sent";
            this.chkRLetter.UseVisualStyleBackColor = true;
            this.chkRLetter.Visible = false;
            this.chkRLetter.CheckedChanged += new System.EventHandler(this.chkRLetter_CheckedChanged);
            // 
            // lblRResolvedCDate
            // 
            this.lblRResolvedCDate.AutoSize = true;
            this.lblRResolvedCDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRResolvedCDate.Location = new System.Drawing.Point(392, 241);
            this.lblRResolvedCDate.Name = "lblRResolvedCDate";
            this.lblRResolvedCDate.Size = new System.Drawing.Size(101, 16);
            this.lblRResolvedCDate.TabIndex = 30;
            this.lblRResolvedCDate.Text = "Resolved Date";
            this.lblRResolvedCDate.Visible = false;
            // 
            // dtpRResolvedICDate
            // 
            this.dtpRResolvedICDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRResolvedICDate.Location = new System.Drawing.Point(498, 193);
            this.dtpRResolvedICDate.Name = "dtpRResolvedICDate";
            this.dtpRResolvedICDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRResolvedICDate.TabIndex = 35;
            this.dtpRResolvedICDate.Visible = false;
            // 
            // chkRResolvedC
            // 
            this.chkRResolvedC.AutoSize = true;
            this.chkRResolvedC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRResolvedC.Location = new System.Drawing.Point(30, 240);
            this.chkRResolvedC.Name = "chkRResolvedC";
            this.chkRResolvedC.Size = new System.Drawing.Size(218, 20);
            this.chkRResolvedC.TabIndex = 32;
            this.chkRResolvedC.Text = "Resolved Directly to Customer";
            this.chkRResolvedC.UseVisualStyleBackColor = true;
            this.chkRResolvedC.Visible = false;
            this.chkRResolvedC.CheckedChanged += new System.EventHandler(this.chkRResolvedC_CheckedChanged);
            // 
            // lblRResolvedICDate
            // 
            this.lblRResolvedICDate.AutoSize = true;
            this.lblRResolvedICDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRResolvedICDate.Location = new System.Drawing.Point(391, 200);
            this.lblRResolvedICDate.Name = "lblRResolvedICDate";
            this.lblRResolvedICDate.Size = new System.Drawing.Size(101, 16);
            this.lblRResolvedICDate.TabIndex = 30;
            this.lblRResolvedICDate.Text = "Resolved Date";
            this.lblRResolvedICDate.Visible = false;
            // 
            // txtRAdjuster
            // 
            this.txtRAdjuster.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRAdjuster.Location = new System.Drawing.Point(822, 158);
            this.txtRAdjuster.Name = "txtRAdjuster";
            this.txtRAdjuster.Size = new System.Drawing.Size(201, 23);
            this.txtRAdjuster.TabIndex = 36;
            this.txtRAdjuster.Visible = false;
            // 
            // chkRResolvedIC
            // 
            this.chkRResolvedIC.AutoSize = true;
            this.chkRResolvedIC.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRResolvedIC.Location = new System.Drawing.Point(29, 199);
            this.chkRResolvedIC.Name = "chkRResolvedIC";
            this.chkRResolvedIC.Size = new System.Drawing.Size(283, 20);
            this.chkRResolvedIC.TabIndex = 32;
            this.chkRResolvedIC.Text = "Resolved/Closed by Insurance Company";
            this.chkRResolvedIC.UseVisualStyleBackColor = true;
            this.chkRResolvedIC.Visible = false;
            this.chkRResolvedIC.CheckedChanged += new System.EventHandler(this.chkRResolvedIC_CheckedChanged);
            // 
            // dtpRReportedDate
            // 
            this.dtpRReportedDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRReportedDate.Location = new System.Drawing.Point(499, 115);
            this.dtpRReportedDate.Name = "dtpRReportedDate";
            this.dtpRReportedDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRReportedDate.TabIndex = 35;
            this.dtpRReportedDate.Visible = false;
            // 
            // txtRClaim
            // 
            this.txtRClaim.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRClaim.Location = new System.Drawing.Point(285, 158);
            this.txtRClaim.Name = "txtRClaim";
            this.txtRClaim.Size = new System.Drawing.Size(98, 23);
            this.txtRClaim.TabIndex = 36;
            this.txtRClaim.Visible = false;
            // 
            // lblRReportedDate
            // 
            this.lblRReportedDate.AutoSize = true;
            this.lblRReportedDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRReportedDate.Location = new System.Drawing.Point(392, 122);
            this.lblRReportedDate.Name = "lblRReportedDate";
            this.lblRReportedDate.Size = new System.Drawing.Size(101, 16);
            this.lblRReportedDate.TabIndex = 30;
            this.lblRReportedDate.Text = "Reported Date";
            this.lblRReportedDate.Visible = false;
            // 
            // lblRAdjuster
            // 
            this.lblRAdjuster.AutoSize = true;
            this.lblRAdjuster.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRAdjuster.Location = new System.Drawing.Point(756, 161);
            this.lblRAdjuster.Name = "lblRAdjuster";
            this.lblRAdjuster.Size = new System.Drawing.Size(60, 16);
            this.lblRAdjuster.TabIndex = 30;
            this.lblRAdjuster.Text = "Adjuster";
            this.lblRAdjuster.Visible = false;
            // 
            // dtpRAssignedDate
            // 
            this.dtpRAssignedDate.CalendarFont = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRAssignedDate.Location = new System.Drawing.Point(498, 154);
            this.dtpRAssignedDate.Name = "dtpRAssignedDate";
            this.dtpRAssignedDate.Size = new System.Drawing.Size(252, 25);
            this.dtpRAssignedDate.TabIndex = 35;
            this.dtpRAssignedDate.Visible = false;
            // 
            // txtRNotes
            // 
            this.txtRNotes.Location = new System.Drawing.Point(1000, 13);
            this.txtRNotes.Multiline = true;
            this.txtRNotes.Name = "txtRNotes";
            this.txtRNotes.Size = new System.Drawing.Size(23, 85);
            this.txtRNotes.TabIndex = 34;
            this.txtRNotes.Visible = false;
            // 
            // lblRClaim
            // 
            this.lblRClaim.AutoSize = true;
            this.lblRClaim.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRClaim.Location = new System.Drawing.Point(224, 161);
            this.lblRClaim.Name = "lblRClaim";
            this.lblRClaim.Size = new System.Drawing.Size(55, 16);
            this.lblRClaim.TabIndex = 30;
            this.lblRClaim.Text = "Claim #";
            this.lblRClaim.Visible = false;
            // 
            // chkRReported
            // 
            this.chkRReported.AutoSize = true;
            this.chkRReported.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRReported.Location = new System.Drawing.Point(30, 121);
            this.chkRReported.Name = "chkRReported";
            this.chkRReported.Size = new System.Drawing.Size(311, 20);
            this.chkRReported.TabIndex = 32;
            this.chkRReported.Text = "Reported To Insurance Account Administrator";
            this.chkRReported.UseVisualStyleBackColor = true;
            this.chkRReported.Visible = false;
            this.chkRReported.CheckedChanged += new System.EventHandler(this.chkRReported_CheckedChanged);
            // 
            // lblRAssignedDate
            // 
            this.lblRAssignedDate.AutoSize = true;
            this.lblRAssignedDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRAssignedDate.Location = new System.Drawing.Point(392, 161);
            this.lblRAssignedDate.Name = "lblRAssignedDate";
            this.lblRAssignedDate.Size = new System.Drawing.Size(100, 16);
            this.lblRAssignedDate.TabIndex = 30;
            this.lblRAssignedDate.Text = "Assigned Date";
            this.lblRAssignedDate.Visible = false;
            // 
            // lblRNotes
            // 
            this.lblRNotes.AutoSize = true;
            this.lblRNotes.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRNotes.Location = new System.Drawing.Point(33, 13);
            this.lblRNotes.Name = "lblRNotes";
            this.lblRNotes.Size = new System.Drawing.Size(44, 16);
            this.lblRNotes.TabIndex = 33;
            this.lblRNotes.Text = "Notes";
            this.lblRNotes.Visible = false;
            // 
            // chkRAssigned
            // 
            this.chkRAssigned.AutoSize = true;
            this.chkRAssigned.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRAssigned.Location = new System.Drawing.Point(29, 160);
            this.chkRAssigned.Name = "chkRAssigned";
            this.chkRAssigned.Size = new System.Drawing.Size(189, 20);
            this.chkRAssigned.TabIndex = 32;
            this.chkRAssigned.Text = "Insurance Claim Assigned";
            this.chkRAssigned.UseVisualStyleBackColor = true;
            this.chkRAssigned.Visible = false;
            this.chkRAssigned.CheckedChanged += new System.EventHandler(this.chkRAssigned_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnExit.Location = new System.Drawing.Point(937, 65);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(91, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // chkClosed
            // 
            this.chkClosed.AutoSize = true;
            this.chkClosed.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkClosed.ForeColor = System.Drawing.Color.DarkGreen;
            this.chkClosed.Location = new System.Drawing.Point(650, 67);
            this.chkClosed.Name = "chkClosed";
            this.chkClosed.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkClosed.Size = new System.Drawing.Size(90, 26);
            this.chkClosed.TabIndex = 4;
            this.chkClosed.Text = "Closed";
            this.chkClosed.UseVisualStyleBackColor = true;
            this.chkClosed.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnSave.Location = new System.Drawing.Point(763, 64);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 27);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // picLogo
            // 
            this.picLogo.Image = global::OccurrenceManagement.Properties.Resources.ParkNFly1;
            this.picLogo.InitialImage = global::OccurrenceManagement.Properties.Resources.ParkNFly1;
            this.picLogo.Location = new System.Drawing.Point(12, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(103, 73);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnCancel.Location = new System.Drawing.Point(850, 64);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 27);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkAttachments
            // 
            this.chkAttachments.AutoSize = true;
            this.chkAttachments.Checked = true;
            this.chkAttachments.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAttachments.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAttachments.ForeColor = System.Drawing.Color.DarkGreen;
            this.chkAttachments.Location = new System.Drawing.Point(865, 24);
            this.chkAttachments.Name = "chkAttachments";
            this.chkAttachments.Size = new System.Drawing.Size(163, 24);
            this.chkAttachments.TabIndex = 6;
            this.chkAttachments.Text = "Show Attachments";
            this.chkAttachments.UseVisualStyleBackColor = true;
            // 
            // btnChequeRequisition
            // 
            this.btnChequeRequisition.Location = new System.Drawing.Point(12, 495);
            this.btnChequeRequisition.Name = "btnChequeRequisition";
            this.btnChequeRequisition.Size = new System.Drawing.Size(221, 40);
            this.btnChequeRequisition.TabIndex = 6;
            this.btnChequeRequisition.Text = "Cheque Requisition";
            this.btnChequeRequisition.UseVisualStyleBackColor = true;
            this.btnChequeRequisition.Click += new System.EventHandler(this.btnChequeRequisition_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(1077, 702);
            this.Controls.Add(this.chkAttachments);
            this.Controls.Add(this.tbcOccurrence);
            this.Controls.Add(this.txtOccurrenceNumber);
            this.Controls.Add(this.lblOccurrenceNumber);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.chkClosed);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.picLogo);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Occurrence Management";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tbcOccurrence.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGHistory)).EndInit();
            this.grpGPolice.ResumeLayout(false);
            this.grpGPolice.PerformLayout();
            this.grpGCost.ResumeLayout(false);
            this.grpGCost.PerformLayout();
            this.tabVehicle.ResumeLayout(false);
            this.tabVehicle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVCarMovements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVServices)).EndInit();
            this.grpVPNF.ResumeLayout(false);
            this.grpVPNF.PerformLayout();
            this.grpVVehicle.ResumeLayout(false);
            this.grpVVehicle.PerformLayout();
            this.grpVCustomer.ResumeLayout(false);
            this.grpVCustomer.PerformLayout();
            this.grpVTicket.ResumeLayout(false);
            this.grpVTicket.PerformLayout();
            this.tabAttachments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabDocuments.ResumeLayout(false);
            this.tabRisk.ResumeLayout(false);
            this.tabRisk.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLine10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label lblOccurrenceNumber;
        private System.Windows.Forms.TextBox txtOccurrenceNumber;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TabControl tbcOccurrence;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cmbGSubtype;
        private System.Windows.Forms.ComboBox cmbGType;
        private System.Windows.Forms.Label lblGType;
        private System.Windows.Forms.ComboBox cmbGWeather;
        private System.Windows.Forms.ComboBox cmbGLocation;
        private System.Windows.Forms.Label lblGLocation;
        private System.Windows.Forms.DateTimePicker dtpGDateR;
        private System.Windows.Forms.DateTimePicker dtpGDate;
        private System.Windows.Forms.Label lblGDateR;
        private System.Windows.Forms.Label lblGDate;
        private System.Windows.Forms.TextBox txtGDescription;
        private System.Windows.Forms.Label lblGDescription;
        private System.Windows.Forms.Label lblGStatus;
        private System.Windows.Forms.Label lblGClosed;
        private System.Windows.Forms.Label lblGOpened;
        private System.Windows.Forms.TextBox txtGBadge;
        private System.Windows.Forms.CheckBox chkGPolice;
        private System.Windows.Forms.DateTimePicker dtpGDatePolice;
        private System.Windows.Forms.Label lblGBadge;
        private System.Windows.Forms.Label lblGDatePolice;
        private System.Windows.Forms.TabPage tabVehicle;
        private System.Windows.Forms.TabPage tabDocuments;
        private System.Windows.Forms.CheckBox chkClosed;
        private System.Windows.Forms.TextBox txtVVehicleID;
        private System.Windows.Forms.Label lblVVehicleID;
        private System.Windows.Forms.GroupBox grpGPolice;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtVColour;
        private System.Windows.Forms.Label lblVColour;
        private System.Windows.Forms.TextBox txtVModelYear;
        private System.Windows.Forms.Label lblVModelYear;
        private System.Windows.Forms.TextBox txtVModel;
        private System.Windows.Forms.Label lblVModel;
        private System.Windows.Forms.TextBox txtVMake;
        private System.Windows.Forms.Label lblVMake;
        private System.Windows.Forms.TextBox txtVLicensePlate;
        private System.Windows.Forms.Label lblVLicensePlate;
        private System.Windows.Forms.TextBox txtVOdometer;
        private System.Windows.Forms.Label lblVOdometer;
        private System.Windows.Forms.Label lblVPNFVehicleType;
        private System.Windows.Forms.TextBox txtVPNFUnitNumber;
        private System.Windows.Forms.Label lblVPNFUnitNumber;
        private System.Windows.Forms.TextBox txtVNumberOfParks;
        private System.Windows.Forms.Label lblVNumberOfParks;
        private System.Windows.Forms.GroupBox grpVVehicle;
        private System.Windows.Forms.TextBox txtVCustomerID;
        private System.Windows.Forms.Label lblVCustomerID;
        private System.Windows.Forms.TextBox txtVAddress2;
        private System.Windows.Forms.Label lblVAddress2;
        private System.Windows.Forms.TextBox txtVAddress1;
        private System.Windows.Forms.Label lblVAddress1;
        private System.Windows.Forms.TextBox txtVLastName;
        private System.Windows.Forms.Label lblVLastName;
        private System.Windows.Forms.TextBox txtVFirstName;
        private System.Windows.Forms.Label lblVFirstName;
        private System.Windows.Forms.TextBox txtVFax;
        private System.Windows.Forms.Label lblVFax;
        private System.Windows.Forms.TextBox txtVCellPhone;
        private System.Windows.Forms.Label lblVCellPhone;
        private System.Windows.Forms.TextBox txtVWorkPhone;
        private System.Windows.Forms.Label lblVWorkPhone;
        private System.Windows.Forms.TextBox txtVHomePhone;
        private System.Windows.Forms.Label lblVHomePhone;
        private System.Windows.Forms.TextBox txtVEmail;
        private System.Windows.Forms.Label lblVEmail;
        private System.Windows.Forms.TextBox txtVPostalCode;
        private System.Windows.Forms.Label lblVPostalCode;
        private System.Windows.Forms.TextBox txtVProvince;
        private System.Windows.Forms.Label lblVProvince;
        private System.Windows.Forms.TextBox txtVCity;
        private System.Windows.Forms.Label lblVCity;
        private System.Windows.Forms.GroupBox grpVCustomer;
        private System.Windows.Forms.TextBox txtVBarCode;
        private System.Windows.Forms.TextBox txtVTicketID;
        private System.Windows.Forms.Label lblVDateClose;
        private System.Windows.Forms.Label lblVDateOpen;
        private System.Windows.Forms.Label lblVBarCode;
        private System.Windows.Forms.TextBox txtVPinNumber;
        private System.Windows.Forms.Label lblVPinNumber;
        private System.Windows.Forms.TextBox txtVIssueNumber;
        private System.Windows.Forms.Label lblVIssueNumber;
        private System.Windows.Forms.TextBox txtVTicketNumber;
        private System.Windows.Forms.Label lblVTicketNumber;
        private System.Windows.Forms.Label lblVTicketID;
        private System.Windows.Forms.GroupBox grpVTicket;
        private System.Windows.Forms.GroupBox grpVPNF;
        private System.Windows.Forms.ComboBox cmbVAccident;
        private System.Windows.Forms.Label lblVAccident;
        private System.Windows.Forms.ComboBox cmbVFault;
        private System.Windows.Forms.Label lblVFault;
        private System.Windows.Forms.DataGridView dgvVServices;
        private System.Windows.Forms.Label lblVServices;
        private System.Windows.Forms.DataGridView dgvVCarMovements;
        private System.Windows.Forms.Label lblVCarMovements;
        private System.Windows.Forms.DataGridView dgvGHistory;
        private System.Windows.Forms.Label lblGHistory;
        private System.Windows.Forms.Button btnGAddRecord;
        private System.Windows.Forms.Button btnGGetData;
        private System.Windows.Forms.Label lblGSubtype;
        private System.Windows.Forms.Label lblGWeather;
        private System.Windows.Forms.TextBox txtVCloseDate;
        private System.Windows.Forms.TextBox txtVOpenDate;
        private System.Windows.Forms.Label lblGFinalCost;
        private System.Windows.Forms.Label lblGEstimatedCost;
        private System.Windows.Forms.TextBox txtGFinalCost;
        private System.Windows.Forms.TextBox txtGEstimatedCost;
        private System.Windows.Forms.GroupBox grpGCost;
        private System.Windows.Forms.Button btnFinalRelease;
        private System.Windows.Forms.Button btnInternalCopy;
        private System.Windows.Forms.Button btnCustomerCopy;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.TabPage tabAttachments;
        private System.Windows.Forms.RadioButton rdbGOther;
        private System.Windows.Forms.RadioButton rdbGTelephone;
        private System.Windows.Forms.RadioButton rdbGCheckOut;
        private System.Windows.Forms.Label lblGReported;
        private System.Windows.Forms.ComboBox cmbVPNFVehicleType;
        private System.Windows.Forms.Button btnDeletePicture;
        private System.Windows.Forms.Button btnAddPicture;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox lstPictures;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkNoTicket;
        private AxAcroPDFLib.AxAcroPDF axAcroPDF1;
        private System.Windows.Forms.RadioButton rdbGEmail;
        private System.Windows.Forms.CheckBox chkAssignToRM;
        private System.Windows.Forms.TextBox txtGVouchers;
        private System.Windows.Forms.Label lblGVouchers;
        private System.Windows.Forms.Button btnFinalReleaseRepair;
        private System.Windows.Forms.Button btnFinalReleaseVoucher;
        private System.Windows.Forms.TabPage tabRisk;
        private System.Windows.Forms.TextBox txtRNotes;
        private System.Windows.Forms.Label lblRNotes;
        private System.Windows.Forms.TextBox txtRAdjuster;
        private System.Windows.Forms.TextBox txtRClaim;
        private System.Windows.Forms.Label lblRAdjuster;
        private System.Windows.Forms.DateTimePicker dtpRAssignedDate;
        private System.Windows.Forms.Label lblRClaim;
        private System.Windows.Forms.Label lblRAssignedDate;
        private System.Windows.Forms.CheckBox chkRAssigned;
        private System.Windows.Forms.DateTimePicker dtpRResolvedICDate;
        private System.Windows.Forms.Label lblRResolvedICDate;
        private System.Windows.Forms.CheckBox chkRResolvedIC;
        private System.Windows.Forms.DateTimePicker dtpRResolvedCDate;
        private System.Windows.Forms.Label lblRResolvedCDate;
        private System.Windows.Forms.CheckBox chkRResolvedC;
        private System.Windows.Forms.DateTimePicker dtpRLetterDate;
        private System.Windows.Forms.Label lblRLetterDate;
        private System.Windows.Forms.CheckBox chkRLetter;
        private System.Windows.Forms.PictureBox picLine4;
        private System.Windows.Forms.PictureBox picLine3;
        private System.Windows.Forms.PictureBox picLine2;
        private System.Windows.Forms.DateTimePicker dtpRReportedDate;
        private System.Windows.Forms.Label lblRReportedDate;
        private System.Windows.Forms.CheckBox chkRReported;
        private System.Windows.Forms.PictureBox picLine5;
        private System.Windows.Forms.PictureBox picLine1;
        private System.Windows.Forms.DateTimePicker dtpRSOCDate;
        private System.Windows.Forms.Label lblRSOCDate;
        private System.Windows.Forms.Label lblRSmallClaim;
        private System.Windows.Forms.CheckBox chkRSOC;
        private System.Windows.Forms.PictureBox picLine10;
        private System.Windows.Forms.PictureBox picLine9;
        private System.Windows.Forms.PictureBox picLine8;
        private System.Windows.Forms.PictureBox picLine7;
        private System.Windows.Forms.PictureBox picLine6;
        private System.Windows.Forms.DateTimePicker dtpRJudgementDate;
        private System.Windows.Forms.DateTimePicker dtpRTrialDate;
        private System.Windows.Forms.Label lblRJudgementDate;
        private System.Windows.Forms.DateTimePicker dtpRPreTrialDate;
        private System.Windows.Forms.Label lblRTrialDate;
        private System.Windows.Forms.DateTimePicker dtpRDefenceDate;
        private System.Windows.Forms.Label lblRPreTrialDate;
        private System.Windows.Forms.Label lblRDefenceDate;
        private System.Windows.Forms.CheckBox chkRJudgement;
        private System.Windows.Forms.CheckBox chkRTrial;
        private System.Windows.Forms.CheckBox chkRPreTrial;
        private System.Windows.Forms.CheckBox chkRDefence;
        private System.Windows.Forms.Button btnOccurrencesOverdue;
        private System.Windows.Forms.CheckBox chkAttachments;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.Button btnFinalReleaseRepairFrench;
        private System.Windows.Forms.Button btnFinalReleaseVoucherFrench;
        private System.Windows.Forms.Button btnFinalReleaseFrench;
        private System.Windows.Forms.Button btnCustomerCopyFrench;
        private System.Windows.Forms.TextBox txtGLostDays;
        private System.Windows.Forms.CheckBox chkLTInjury;
        private System.Windows.Forms.Label lblGLostDays;
        private System.Windows.Forms.Button btnRSendC;
        private System.Windows.Forms.Button btnRSendB;
        private System.Windows.Forms.Button btnRAddRecord;
        private System.Windows.Forms.Button btnChequeRequisition;
    }
}

