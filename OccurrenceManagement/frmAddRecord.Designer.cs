﻿namespace OccurrenceManagement
{
    partial class frmAddRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbARActionTypes = new System.Windows.Forms.ComboBox();
            this.lblARActionType = new System.Windows.Forms.Label();
            this.dtpARDate = new System.Windows.Forms.DateTimePicker();
            this.lblARDate = new System.Windows.Forms.Label();
            this.txtARAction = new System.Windows.Forms.TextBox();
            this.lblARAction = new System.Windows.Forms.Label();
            this.btnARAddRecord = new System.Windows.Forms.Button();
            this.btnARCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbARActionTypes
            // 
            this.cmbARActionTypes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbARActionTypes.FormattingEnabled = true;
            this.cmbARActionTypes.Location = new System.Drawing.Point(114, 42);
            this.cmbARActionTypes.Name = "cmbARActionTypes";
            this.cmbARActionTypes.Size = new System.Drawing.Size(376, 24);
            this.cmbARActionTypes.TabIndex = 0;
            this.cmbARActionTypes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbARActionTypes_KeyPress);
            // 
            // lblARActionType
            // 
            this.lblARActionType.AutoSize = true;
            this.lblARActionType.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblARActionType.Location = new System.Drawing.Point(26, 45);
            this.lblARActionType.Name = "lblARActionType";
            this.lblARActionType.Size = new System.Drawing.Size(82, 16);
            this.lblARActionType.TabIndex = 1;
            this.lblARActionType.Text = "Action Type";
            // 
            // dtpARDate
            // 
            this.dtpARDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpARDate.Location = new System.Drawing.Point(642, 40);
            this.dtpARDate.Name = "dtpARDate";
            this.dtpARDate.Size = new System.Drawing.Size(245, 22);
            this.dtpARDate.TabIndex = 2;
            // 
            // lblARDate
            // 
            this.lblARDate.AutoSize = true;
            this.lblARDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblARDate.Location = new System.Drawing.Point(567, 45);
            this.lblARDate.Name = "lblARDate";
            this.lblARDate.Size = new System.Drawing.Size(69, 16);
            this.lblARDate.TabIndex = 1;
            this.lblARDate.Text = "Follow Up";
            // 
            // txtARAction
            // 
            this.txtARAction.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtARAction.Location = new System.Drawing.Point(114, 97);
            this.txtARAction.Multiline = true;
            this.txtARAction.Name = "txtARAction";
            this.txtARAction.Size = new System.Drawing.Size(773, 97);
            this.txtARAction.TabIndex = 3;
            // 
            // lblARAction
            // 
            this.lblARAction.AutoSize = true;
            this.lblARAction.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblARAction.Location = new System.Drawing.Point(26, 98);
            this.lblARAction.Name = "lblARAction";
            this.lblARAction.Size = new System.Drawing.Size(47, 16);
            this.lblARAction.TabIndex = 1;
            this.lblARAction.Text = "Action";
            // 
            // btnARAddRecord
            // 
            this.btnARAddRecord.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnARAddRecord.Location = new System.Drawing.Point(611, 209);
            this.btnARAddRecord.Name = "btnARAddRecord";
            this.btnARAddRecord.Size = new System.Drawing.Size(123, 38);
            this.btnARAddRecord.TabIndex = 4;
            this.btnARAddRecord.Text = "Add Record";
            this.btnARAddRecord.UseVisualStyleBackColor = true;
            this.btnARAddRecord.Click += new System.EventHandler(this.btnARAddRecord_Click);
            // 
            // btnARCancel
            // 
            this.btnARCancel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnARCancel.Location = new System.Drawing.Point(764, 209);
            this.btnARCancel.Name = "btnARCancel";
            this.btnARCancel.Size = new System.Drawing.Size(123, 38);
            this.btnARCancel.TabIndex = 4;
            this.btnARCancel.Text = "Cancel";
            this.btnARCancel.UseVisualStyleBackColor = true;
            this.btnARCancel.Click += new System.EventHandler(this.btnARCancel_Click);
            // 
            // frmAddRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(925, 269);
            this.Controls.Add(this.btnARCancel);
            this.Controls.Add(this.btnARAddRecord);
            this.Controls.Add(this.txtARAction);
            this.Controls.Add(this.dtpARDate);
            this.Controls.Add(this.lblARDate);
            this.Controls.Add(this.lblARAction);
            this.Controls.Add(this.lblARActionType);
            this.Controls.Add(this.cmbARActionTypes);
            this.ForeColor = System.Drawing.Color.DarkGreen;
            this.Name = "frmAddRecord";
            this.Text = "Add Record";
            this.Load += new System.EventHandler(this.frmAddRecord_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbARActionTypes;
        private System.Windows.Forms.Label lblARActionType;
        private System.Windows.Forms.DateTimePicker dtpARDate;
        private System.Windows.Forms.Label lblARDate;
        private System.Windows.Forms.TextBox txtARAction;
        private System.Windows.Forms.Label lblARAction;
        private System.Windows.Forms.Button btnARAddRecord;
        private System.Windows.Forms.Button btnARCancel;
    }
}