﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

namespace OccurrenceManagement
{
    public partial class frmCopyPicture : Form
    {
        private string strPicturePath = "";
        private DirectoryInfo directoryInfo;
        private string strOccurrenceNumber_ = "0";

        public frmCopyPicture(string strOccurrenceNumber)
        {
            strOccurrenceNumber_ = strOccurrenceNumber;

            InitializeComponent();

           // DirectoryInfo directoryInfo = new DirectoryInfo(@"C:\CarPictures");
            //string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ////DirectoryInfo directoryInfo = new DirectoryInfo(path + "\\CAA");
            directoryInfo = new DirectoryInfo(@"C:\OccurrenceManagement\Pics\");



            if (directoryInfo.Exists)
            {
                treeView1.AfterSelect += treeView1_AfterSelect;
                BuildTree(directoryInfo, treeView1.Nodes);

            }
        }

        private void BuildTree(DirectoryInfo directoryInfo, TreeNodeCollection addInMe)
        {
            try
            {
                TreeNode curNode = addInMe.Add(directoryInfo.Name);

                foreach (FileInfo file in directoryInfo.GetFiles())
                { 
                        curNode.Nodes.Add(file.FullName, file.Name); 
                }
                foreach (DirectoryInfo subdir in directoryInfo.GetDirectories())
                {
                        BuildTree(subdir, curNode.Nodes); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //if (e.Node.Name.EndsWith("txt"))
            //{
            //    this.richTextBox1.Clear();
            //    StreamReader reader = new StreamReader(e.Node.Name);
            //    this.richTextBox1.Text = reader.ReadToEnd();
            //    reader.Close();
            //}

            strPicturePath = e.Node.Name.ToString();
        }

        private void frmCopyPicture_Load(object sender, EventArgs e)
        {

        }

        private void btnAddPicture_Click(object sender, EventArgs e)
        {
            string fileName = Path.GetFileName(strPicturePath);
            if (fileName.Length == 0)
            {
                MessageBox.Show("Please select a file");
            }
            else
            {
                try
                {
                    string strNewPicLocation = ConfigurationManager.AppSettings["AttachmentsPath"] + "\\" + strOccurrenceNumber_ + "\\" + fileName;
                    File.Move(strPicturePath, strNewPicLocation);
                    lblMessage.Text = fileName + " is successfully added";
                }
                catch (Exception ex)
                {
                    lblMessage.Text = fileName + " is NOT added because " + ex.Message;
                }
            }

            treeView1.Nodes.Clear();

            if (directoryInfo.Exists)
            {
                treeView1.AfterSelect += treeView1_AfterSelect;
                BuildTree(directoryInfo, treeView1.Nodes);
            }

            this.DialogResult = DialogResult.OK;

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
