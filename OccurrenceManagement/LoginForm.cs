﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace OccurrenceManagement
{
    public partial class frmLogin : Form
    {
        string strUser = "";
        string strPassword = "";

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            strUser = txtUser.Text;
            strPassword = txtPassword.Text;

            dsLoginUsers.dtLoginUsersDataTable dtUsers = new dsLoginUsers.dtLoginUsersDataTable();
            dsLoginUsersTableAdapters.dtLoginUsersTableAdapter daUsers = new dsLoginUsersTableAdapters.dtLoginUsersTableAdapter();

            dsSecurity.dtSecurityDataTable dtSecurity = new dsSecurity.dtSecurityDataTable();
            dsSecurityTableAdapters.dtSecurityTableAdapter daSecurity = new dsSecurityTableAdapters.dtSecurityTableAdapter();

            if (strUser.Length > 2)
            {                
                daUsers.Fill(dtUsers, strUser, strPassword);
                daSecurity.Fill(dtSecurity, strUser);

                if (dtUsers.Rows.Count > 0)
                {
                    if (dtSecurity.Rows.Count > 0)
                    {
                        GlobalData.UserID = dtUsers.Rows[0].ItemArray[0].ToString();
                        GlobalData.UserName = dtUsers.Rows[0].ItemArray[1].ToString() + dtUsers.Rows[0].ItemArray[2].ToString();
                        GlobalData.UserLogin = strUser;
                        GlobalData.Locations = dtSecurity.Rows[0].ItemArray[4].ToString();
                        GlobalData.Level = Convert.ToInt32(dtSecurity.Rows[0].ItemArray[5].ToString());
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //DialogResult = DialogResult.OK; 
            }

        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Do something
                //DialogResult = DialogResult.OK; 
                e.Handled = true;

                strUser = txtUser.Text;
                strPassword = txtPassword.Text;

                dsLoginUsers.dtLoginUsersDataTable dtUsers = new dsLoginUsers.dtLoginUsersDataTable();
                dsLoginUsersTableAdapters.dtLoginUsersTableAdapter daUsers = new dsLoginUsersTableAdapters.dtLoginUsersTableAdapter();

                dsSecurity.dtSecurityDataTable dtSecurity = new dsSecurity.dtSecurityDataTable();
                dsSecurityTableAdapters.dtSecurityTableAdapter daSecurity = new dsSecurityTableAdapters.dtSecurityTableAdapter();

                if (strUser.Length > 2)
                {
                    daUsers.Fill(dtUsers, strUser, strPassword);
                    daSecurity.Fill(dtSecurity, strUser);

                    if (dtUsers.Rows.Count > 0)
                    {
                        if (dtSecurity.Rows.Count > 0)
                        {
                            GlobalData.UserID = dtUsers.Rows[0].ItemArray[0].ToString();
                            GlobalData.UserName = dtUsers.Rows[0].ItemArray[1].ToString() + dtUsers.Rows[0].ItemArray[2].ToString();
                            GlobalData.UserLogin = strUser;
                            GlobalData.Locations = dtSecurity.Rows[0].ItemArray[4].ToString();
                            GlobalData.Level = Convert.ToInt32(dtSecurity.Rows[0].ItemArray[5].ToString());
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show("Sorry, you do not have rights for this application", "Occurrence Management", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //DialogResult = DialogResult.OK; 
                }

            }

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {


        }



    }
}
