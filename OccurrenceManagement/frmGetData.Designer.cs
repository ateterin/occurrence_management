﻿namespace OccurrenceManagement
{
    partial class frmGetData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGDBarCode = new System.Windows.Forms.Label();
            this.txtGDBarCode = new System.Windows.Forms.TextBox();
            this.lblGDIssueNumber = new System.Windows.Forms.Label();
            this.txtGDIssueNumber = new System.Windows.Forms.TextBox();
            this.lblGDPinCode = new System.Windows.Forms.Label();
            this.txtGDPinCode = new System.Windows.Forms.TextBox();
            this.grpGDTicket = new System.Windows.Forms.GroupBox();
            this.dtpGDOpenDate = new System.Windows.Forms.DateTimePicker();
            this.txtGDTicketNumber = new System.Windows.Forms.TextBox();
            this.lblGDTicketNumber = new System.Windows.Forms.Label();
            this.lblGDOpenDate = new System.Windows.Forms.Label();
            this.grpGDVehicle = new System.Windows.Forms.GroupBox();
            this.txtGDLicensePlate = new System.Windows.Forms.TextBox();
            this.lblGDLicensePlate = new System.Windows.Forms.Label();
            this.grpGDCustomer = new System.Windows.Forms.GroupBox();
            this.txtGDLastName = new System.Windows.Forms.TextBox();
            this.lblGDLastName = new System.Windows.Forms.Label();
            this.dgvGDTickets = new System.Windows.Forms.DataGridView();
            this.btnGDFind = new System.Windows.Forms.Button();
            this.btnGDCancel = new System.Windows.Forms.Button();
            this.grpGDTicket.SuspendLayout();
            this.grpGDVehicle.SuspendLayout();
            this.grpGDCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGDTickets)).BeginInit();
            this.SuspendLayout();
            // 
            // lblGDBarCode
            // 
            this.lblGDBarCode.AutoSize = true;
            this.lblGDBarCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDBarCode.Location = new System.Drawing.Point(20, 28);
            this.lblGDBarCode.Name = "lblGDBarCode";
            this.lblGDBarCode.Size = new System.Drawing.Size(68, 16);
            this.lblGDBarCode.TabIndex = 0;
            this.lblGDBarCode.Text = "Bar Code";
            // 
            // txtGDBarCode
            // 
            this.txtGDBarCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDBarCode.Location = new System.Drawing.Point(129, 25);
            this.txtGDBarCode.Name = "txtGDBarCode";
            this.txtGDBarCode.Size = new System.Drawing.Size(138, 23);
            this.txtGDBarCode.TabIndex = 1;
            this.txtGDBarCode.TextChanged += new System.EventHandler(this.txtGDBarCode_TextChanged);
            // 
            // lblGDIssueNumber
            // 
            this.lblGDIssueNumber.AutoSize = true;
            this.lblGDIssueNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDIssueNumber.Location = new System.Drawing.Point(20, 86);
            this.lblGDIssueNumber.Name = "lblGDIssueNumber";
            this.lblGDIssueNumber.Size = new System.Drawing.Size(94, 16);
            this.lblGDIssueNumber.TabIndex = 0;
            this.lblGDIssueNumber.Text = "Issue Number";
            // 
            // txtGDIssueNumber
            // 
            this.txtGDIssueNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDIssueNumber.Location = new System.Drawing.Point(129, 83);
            this.txtGDIssueNumber.Name = "txtGDIssueNumber";
            this.txtGDIssueNumber.Size = new System.Drawing.Size(138, 23);
            this.txtGDIssueNumber.TabIndex = 2;
            this.txtGDIssueNumber.TextChanged += new System.EventHandler(this.txtGDIssueNumber_TextChanged);
            // 
            // lblGDPinCode
            // 
            this.lblGDPinCode.AutoSize = true;
            this.lblGDPinCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDPinCode.Location = new System.Drawing.Point(20, 115);
            this.lblGDPinCode.Name = "lblGDPinCode";
            this.lblGDPinCode.Size = new System.Drawing.Size(66, 16);
            this.lblGDPinCode.TabIndex = 0;
            this.lblGDPinCode.Text = "Pin Code";
            // 
            // txtGDPinCode
            // 
            this.txtGDPinCode.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDPinCode.Location = new System.Drawing.Point(129, 112);
            this.txtGDPinCode.Name = "txtGDPinCode";
            this.txtGDPinCode.Size = new System.Drawing.Size(138, 23);
            this.txtGDPinCode.TabIndex = 3;
            this.txtGDPinCode.TextChanged += new System.EventHandler(this.txtGDPinCode_TextChanged);
            // 
            // grpGDTicket
            // 
            this.grpGDTicket.Controls.Add(this.dtpGDOpenDate);
            this.grpGDTicket.Controls.Add(this.txtGDTicketNumber);
            this.grpGDTicket.Controls.Add(this.txtGDBarCode);
            this.grpGDTicket.Controls.Add(this.lblGDTicketNumber);
            this.grpGDTicket.Controls.Add(this.txtGDPinCode);
            this.grpGDTicket.Controls.Add(this.lblGDBarCode);
            this.grpGDTicket.Controls.Add(this.lblGDOpenDate);
            this.grpGDTicket.Controls.Add(this.lblGDPinCode);
            this.grpGDTicket.Controls.Add(this.txtGDIssueNumber);
            this.grpGDTicket.Controls.Add(this.lblGDIssueNumber);
            this.grpGDTicket.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGDTicket.Location = new System.Drawing.Point(26, 23);
            this.grpGDTicket.Name = "grpGDTicket";
            this.grpGDTicket.Size = new System.Drawing.Size(386, 179);
            this.grpGDTicket.TabIndex = 2;
            this.grpGDTicket.TabStop = false;
            this.grpGDTicket.Text = "Ticket";
            // 
            // dtpGDOpenDate
            // 
            this.dtpGDOpenDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpGDOpenDate.Location = new System.Drawing.Point(129, 142);
            this.dtpGDOpenDate.Name = "dtpGDOpenDate";
            this.dtpGDOpenDate.Size = new System.Drawing.Size(237, 23);
            this.dtpGDOpenDate.TabIndex = 4;
            this.dtpGDOpenDate.ValueChanged += new System.EventHandler(this.dtpGDOpenDate_ValueChanged);
            // 
            // txtGDTicketNumber
            // 
            this.txtGDTicketNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDTicketNumber.Location = new System.Drawing.Point(129, 54);
            this.txtGDTicketNumber.Name = "txtGDTicketNumber";
            this.txtGDTicketNumber.Size = new System.Drawing.Size(138, 23);
            this.txtGDTicketNumber.TabIndex = 1;
            this.txtGDTicketNumber.TextChanged += new System.EventHandler(this.txtGDTicketNumber_TextChanged);
            // 
            // lblGDTicketNumber
            // 
            this.lblGDTicketNumber.AutoSize = true;
            this.lblGDTicketNumber.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDTicketNumber.Location = new System.Drawing.Point(20, 57);
            this.lblGDTicketNumber.Name = "lblGDTicketNumber";
            this.lblGDTicketNumber.Size = new System.Drawing.Size(98, 16);
            this.lblGDTicketNumber.TabIndex = 0;
            this.lblGDTicketNumber.Text = "Ticket Number";
            // 
            // lblGDOpenDate
            // 
            this.lblGDOpenDate.AutoSize = true;
            this.lblGDOpenDate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDOpenDate.Location = new System.Drawing.Point(20, 147);
            this.lblGDOpenDate.Name = "lblGDOpenDate";
            this.lblGDOpenDate.Size = new System.Drawing.Size(77, 16);
            this.lblGDOpenDate.TabIndex = 0;
            this.lblGDOpenDate.Text = "Open Date";
            // 
            // grpGDVehicle
            // 
            this.grpGDVehicle.Controls.Add(this.txtGDLicensePlate);
            this.grpGDVehicle.Controls.Add(this.lblGDLicensePlate);
            this.grpGDVehicle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGDVehicle.Location = new System.Drawing.Point(418, 23);
            this.grpGDVehicle.Name = "grpGDVehicle";
            this.grpGDVehicle.Size = new System.Drawing.Size(291, 56);
            this.grpGDVehicle.TabIndex = 2;
            this.grpGDVehicle.TabStop = false;
            this.grpGDVehicle.Text = "Vehicle";
            // 
            // txtGDLicensePlate
            // 
            this.txtGDLicensePlate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDLicensePlate.Location = new System.Drawing.Point(126, 25);
            this.txtGDLicensePlate.Name = "txtGDLicensePlate";
            this.txtGDLicensePlate.Size = new System.Drawing.Size(138, 23);
            this.txtGDLicensePlate.TabIndex = 4;
            this.txtGDLicensePlate.TextChanged += new System.EventHandler(this.txtGDLicensePlate_TextChanged);
            // 
            // lblGDLicensePlate
            // 
            this.lblGDLicensePlate.AutoSize = true;
            this.lblGDLicensePlate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDLicensePlate.Location = new System.Drawing.Point(17, 28);
            this.lblGDLicensePlate.Name = "lblGDLicensePlate";
            this.lblGDLicensePlate.Size = new System.Drawing.Size(93, 16);
            this.lblGDLicensePlate.TabIndex = 0;
            this.lblGDLicensePlate.Text = "License Plate";
            // 
            // grpGDCustomer
            // 
            this.grpGDCustomer.Controls.Add(this.txtGDLastName);
            this.grpGDCustomer.Controls.Add(this.lblGDLastName);
            this.grpGDCustomer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpGDCustomer.Location = new System.Drawing.Point(418, 85);
            this.grpGDCustomer.Name = "grpGDCustomer";
            this.grpGDCustomer.Size = new System.Drawing.Size(291, 57);
            this.grpGDCustomer.TabIndex = 2;
            this.grpGDCustomer.TabStop = false;
            this.grpGDCustomer.Text = "Customer";
            // 
            // txtGDLastName
            // 
            this.txtGDLastName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGDLastName.Location = new System.Drawing.Point(126, 25);
            this.txtGDLastName.Name = "txtGDLastName";
            this.txtGDLastName.Size = new System.Drawing.Size(138, 23);
            this.txtGDLastName.TabIndex = 5;
            this.txtGDLastName.TextChanged += new System.EventHandler(this.txtGDLastName_TextChanged);
            // 
            // lblGDLastName
            // 
            this.lblGDLastName.AutoSize = true;
            this.lblGDLastName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGDLastName.Location = new System.Drawing.Point(17, 28);
            this.lblGDLastName.Name = "lblGDLastName";
            this.lblGDLastName.Size = new System.Drawing.Size(75, 16);
            this.lblGDLastName.TabIndex = 0;
            this.lblGDLastName.Text = "Last Name";
            // 
            // dgvGDTickets
            // 
            this.dgvGDTickets.AllowUserToAddRows = false;
            this.dgvGDTickets.AllowUserToDeleteRows = false;
            this.dgvGDTickets.AllowUserToResizeColumns = false;
            this.dgvGDTickets.AllowUserToResizeRows = false;
            this.dgvGDTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGDTickets.Location = new System.Drawing.Point(26, 208);
            this.dgvGDTickets.MultiSelect = false;
            this.dgvGDTickets.Name = "dgvGDTickets";
            this.dgvGDTickets.RowHeadersVisible = false;
            this.dgvGDTickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGDTickets.Size = new System.Drawing.Size(951, 308);
            this.dgvGDTickets.TabIndex = 3;
            // 
            // btnGDFind
            // 
            this.btnGDFind.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGDFind.Location = new System.Drawing.Point(806, 31);
            this.btnGDFind.Name = "btnGDFind";
            this.btnGDFind.Size = new System.Drawing.Size(105, 36);
            this.btnGDFind.TabIndex = 4;
            this.btnGDFind.Text = "Find";
            this.btnGDFind.UseVisualStyleBackColor = true;
            this.btnGDFind.Click += new System.EventHandler(this.btnGDFind_Click);
            // 
            // btnGDCancel
            // 
            this.btnGDCancel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGDCancel.Location = new System.Drawing.Point(806, 80);
            this.btnGDCancel.Name = "btnGDCancel";
            this.btnGDCancel.Size = new System.Drawing.Size(105, 36);
            this.btnGDCancel.TabIndex = 4;
            this.btnGDCancel.Text = "Cancel";
            this.btnGDCancel.UseVisualStyleBackColor = true;
            this.btnGDCancel.Click += new System.EventHandler(this.btnGDCancel_Click);
            // 
            // frmGetData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(989, 528);
            this.Controls.Add(this.btnGDCancel);
            this.Controls.Add(this.btnGDFind);
            this.Controls.Add(this.dgvGDTickets);
            this.Controls.Add(this.grpGDVehicle);
            this.Controls.Add(this.grpGDCustomer);
            this.Controls.Add(this.grpGDTicket);
            this.ForeColor = System.Drawing.Color.DarkGreen;
            this.Name = "frmGetData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Get Data";
            this.Load += new System.EventHandler(this.frmGetData_Load);
            this.grpGDTicket.ResumeLayout(false);
            this.grpGDTicket.PerformLayout();
            this.grpGDVehicle.ResumeLayout(false);
            this.grpGDVehicle.PerformLayout();
            this.grpGDCustomer.ResumeLayout(false);
            this.grpGDCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGDTickets)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGDBarCode;
        private System.Windows.Forms.TextBox txtGDBarCode;
        private System.Windows.Forms.Label lblGDIssueNumber;
        private System.Windows.Forms.TextBox txtGDIssueNumber;
        private System.Windows.Forms.Label lblGDPinCode;
        private System.Windows.Forms.TextBox txtGDPinCode;
        private System.Windows.Forms.GroupBox grpGDTicket;
        private System.Windows.Forms.GroupBox grpGDVehicle;
        private System.Windows.Forms.TextBox txtGDLicensePlate;
        private System.Windows.Forms.Label lblGDLicensePlate;
        private System.Windows.Forms.GroupBox grpGDCustomer;
        private System.Windows.Forms.TextBox txtGDLastName;
        private System.Windows.Forms.Label lblGDLastName;
        private System.Windows.Forms.DataGridView dgvGDTickets;
        private System.Windows.Forms.Button btnGDFind;
        private System.Windows.Forms.Button btnGDCancel;
        private System.Windows.Forms.TextBox txtGDTicketNumber;
        private System.Windows.Forms.Label lblGDTicketNumber;
        private System.Windows.Forms.DateTimePicker dtpGDOpenDate;
        private System.Windows.Forms.Label lblGDOpenDate;
    }
}