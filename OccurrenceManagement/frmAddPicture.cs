﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OccurrenceManagement
{
    public partial class frmAddPicture : Form
    {
        public frmAddPicture()
        {
            InitializeComponent();
        }

        private void frmAddtPicture_Load(object sender, EventArgs e)
        {
            foreach (DriveInfo di in DriveInfo.GetDrives())
            {
                lstDrives.Items.Add(di);
            }
        }

        private void lstDrives_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstFolders.Items.Clear();

            try
            {
                DriveInfo drive = (DriveInfo)lstDrives.SelectedItem;

                foreach (DirectoryInfo dirInfo in drive.RootDirectory.GetDirectories())
                {
                    lstFolders.Items.Add(dirInfo);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstFiles.Items.Clear();

            DirectoryInfo dir = (DirectoryInfo)lstFolders.SelectedItem;

            foreach (FileInfo fi in dir.GetFiles())
            {
                lstFiles.Items.Add(fi);
            }
        }
    }
}
