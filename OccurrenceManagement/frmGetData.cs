﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OccurrenceManagement
{
    public partial class frmGetData : Form
    {
        private StringBuilder strTicketId_;
        private string strLocationId_ = "0";
        int statusData = 0;

        dsFindTicket.sp_FindTicketDataTable dtFTicket = new dsFindTicket.sp_FindTicketDataTable();
        dsFindTicketTableAdapters.sp_FindTicketTableAdapter daFTicket = new dsFindTicketTableAdapters.sp_FindTicketTableAdapter();

        dsSearchTickets.dtSearchTicketsDataTable dtSTickets = new dsSearchTickets.dtSearchTicketsDataTable();
        dsSearchTicketsTableAdapters.dtSearchTicketsTableAdapter daSTickets = new dsSearchTicketsTableAdapters.dtSearchTicketsTableAdapter();

        public frmGetData(ref StringBuilder strTicketId, string strLocationId)
        {
            strTicketId_ = strTicketId;
            strLocationId_ = strLocationId;
            InitializeComponent();
        }

        private void frmGetData_Load(object sender, EventArgs e)
        {
            //this.Font = new Font(this.Font.FontFamily, this.Font.SizeInPoints * 125 / 96);
            //base.OnLoad(e);

            this.ActiveControl = txtGDBarCode;
            statusData = 0;
            btnGDFind.Text = "Find";
           
        }

        private void btnGDCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGDFind_Click(object sender, EventArgs e)
        {
            try
            {
                if (statusData == 0)
                {
                    if (txtGDBarCode.Text.Length > 4)
                    {
                        statusData = statusData + 1;
                        btnGDFind.Text = "Select";
                        daFTicket.Fill(dtFTicket, txtGDBarCode.Text, Convert.ToInt32(strLocationId_));
                        dgvGDTickets.DataSource = dtFTicket;
                        dgvGDTickets.Refresh();
                    }
                    else
                    {
                        statusData = statusData + 1;
                        btnGDFind.Text = "Select";

                        DateTime dtTicketOpenDate = this.dtpGDOpenDate.Value;
                        if ((txtGDLastName.Text != null && txtGDLastName.Text.Length > 0) || (txtGDLicensePlate.Text != null && txtGDLicensePlate.Text.Length > 0))
                        {
                            dtTicketOpenDate = DateTime.MinValue;
                        }

                        if (dtTicketOpenDate != DateTime.MinValue)
                        {
                            // fill in the data
                            daSTickets.Fill(dtSTickets, Convert.ToInt32(strLocationId_), dtTicketOpenDate, txtGDLicensePlate.Text, txtGDLastName.Text, "", txtGDIssueNumber.Text, txtGDPinCode.Text);
                        }
                        else
                        {
                            // fill in the data
                            daSTickets.Fill(dtSTickets, Convert.ToInt32(strLocationId_), null, txtGDLicensePlate.Text, txtGDLastName.Text, "", txtGDIssueNumber.Text, txtGDPinCode.Text);
                        }

                        dgvGDTickets.DataSource = dtSTickets;
                        dgvGDTickets.Refresh();
                    }

                    dgvGDTickets.Columns[0].Width = 130;
                    dgvGDTickets.Columns[3].Width = 75;
                    dgvGDTickets.Columns[4].Width = 110;
                    dgvGDTickets.Columns[5].Width = 110;
                    dgvGDTickets.Columns[6].Width = 50;
                    dgvGDTickets.Columns[7].Width = 80;
                    dgvGDTickets.Columns[8].Width = 75;

                }
                else
                {
                    //strTicketId_.Append(txtGDIssueNumber.Text);
                    if (dgvGDTickets.Rows.Count == 0)
                    {
                        statusData = 0;
                        btnGDFind.Text = "Find";
                        MessageBox.Show("No data is selected");
                    }
                    else
                    {
                        if (dgvGDTickets.SelectedCells[0].Value == null)
                        {
                            statusData = 0;
                            btnGDFind.Text = "Find";
                            MessageBox.Show("No data is selected");
                        }
                        else
                        {
                            strTicketId_.Append(dgvGDTickets.SelectedCells[0].Value);
                            this.DialogResult = DialogResult.OK;
                        }
                    }

                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR:" + ex.Message.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void txtGDBarCode_TextChanged(object sender, EventArgs e)
        {
                            statusData = 0;
                            btnGDFind.Text = "Find";
                            dgvGDTickets.DataSource = null;
                            dgvGDTickets.Refresh();
        }

        private void txtGDTicketNumber_TextChanged(object sender, EventArgs e)
        {
                            statusData = 0;
                            btnGDFind.Text = "Find";
                            dgvGDTickets.DataSource = null;
                            dgvGDTickets.Refresh();
        }

        private void txtGDIssueNumber_TextChanged(object sender, EventArgs e)
        {
            statusData = 0;
            btnGDFind.Text = "Find";
            dgvGDTickets.DataSource = null;
            dgvGDTickets.Refresh();
        }

        private void txtGDPinCode_TextChanged(object sender, EventArgs e)
        {
            statusData = 0;
            btnGDFind.Text = "Find";
            dgvGDTickets.DataSource = null;
            dgvGDTickets.Refresh();
        }

        private void dtpGDOpenDate_ValueChanged(object sender, EventArgs e)
        {
            statusData = 0;
            btnGDFind.Text = "Find";
            dgvGDTickets.DataSource = null;
            dgvGDTickets.Refresh();
        }

        private void txtGDLicensePlate_TextChanged(object sender, EventArgs e)
        {
            statusData = 0;
            btnGDFind.Text = "Find";
            dgvGDTickets.DataSource = null;
            dgvGDTickets.Refresh();
        }

        private void txtGDLastName_TextChanged(object sender, EventArgs e)
        {
            statusData = 0;
            btnGDFind.Text = "Find";
            dgvGDTickets.DataSource = null;
            dgvGDTickets.Refresh();
        }

 
    }
}
