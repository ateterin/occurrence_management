﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace OccurrenceManagement
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                DialogResult result;
                using (var loginForm = new frmLogin())
                    result = loginForm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // login was successful
                    Application.Run(new frmMain());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            //Application.Run(new frmMain());
        }
    }
}
